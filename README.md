:JGF

# Мобильное приложение MedTochka

## Предварительные настройки

### [Установка Flutter](https://flutter.dev/docs/get-started/install/linux)

### [Установка Android Studio](https://flutter.dev/docs/development/tools/android-studio)

### SDK (Settings -> Appearance & Behavior -> System Settings -> Android SDK)
 - Platforms:
    - Android 9.0
    - Android 10.0
    - Android 11.0
 - Tools:
    - Android SDK Build-Tools 31
    - Android SDK Command-line Tools
    - CMake
    - Android Emulator
    - Android SDK Platform-Tools
    - Google Play APK Expansion library

### Необходимые плагины
 - Flutter
 - Dart

### [Эмуляция устройств](https://dimlix.com/adv-android-studio/)
Для эмуляции устройства используются Pixel 3a (api 30) и Nexus s (api 22). Данные устройства покрывают большенство тестов связанных с ui.
Во время работы обязательно необходимо проверять вёрстку на них, чтобы она не поломалась.

### Поддерживаемые версии
 - Flutter ^2.5.1
 - Dart ^2.14.2

## Обновление зависимостей и запуск проекта
После клонирования проекта из gitlab необходимо выполнить команду `flutter pub get` - данная команда скачает все зависимости, которые использует проект (аналогично `npm install`)
