import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/bloc/splash/splash_bloc.dart';
import 'package:medtochka/app/bloc/splash/splash_states.dart';
import 'package:medtochka/app/core/router/router_path.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/images.dart';
import 'package:medtochka/resources.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocListener<SplashBloc, SplashState>(
      listener: (context, state) async {
        if (state is SplashShowTour) {
          _navigateToPageTour(context);
        }

        if (state is SplashShowAuth) {
          _navigateToPageAuth(context);
        }

        if (state is SplashShowAuthCode) {
          _navigateToPageAuthCode(context);
        }

        if (state is SplashShowCreateAuthCode) {
          _navigateToPageCreateAuthCode(context);
        }

        if (state is SplashShowHome) {
          _navigateToPageHome(context);
        }
      },
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(ResImages.LOGO_MEDTOCHKA),
            Container(
              margin: const EdgeInsets.all(16),
              child: const Text(
                ResStrings.SPLASH_BODY,
                style: TextStyle(fontSize: 16, color: ResColors.DEEP_GREY),
              ),
            )
          ],
        ),
      ),
    ));
  }

  Future<void> _navigateToPageTour(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.TOUR);
  }

  Future<void> _navigateToPageAuth(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.LOGIN);
  }

  Future<void> _navigateToPageCreateAuthCode(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.CREATE_CODE);
  }

  Future<void> _navigateToPageAuthCode(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.CONFIRM_CODE);
  }

  Future<void> _navigateToPageHome(BuildContext context) async {}
}
