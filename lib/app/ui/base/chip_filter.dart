import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/text_theme.dart';

class ChipFilter extends StatelessWidget {
  final bool isSelected;
  final VoidCallback onSelected;
  final ResChipType type;

  const ChipFilter(
      {Key? key,
      required this.isSelected,
      required this.onSelected,
      required this.type})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      label: Text(type.text,
          style: ResTextTheme.body2.merge(TextStyle(
              color: isSelected ? ResColors.PRIMARY_BLUE : ResColors.BLACK))),
      selected: isSelected,
      backgroundColor: ResColors.WHITE,
      labelPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 3),
      selectedColor: ResColors.LIGHT_BLUE,
      shape: StadiumBorder(
          side: BorderSide(
              color:
                  isSelected ? ResColors.PRIMARY_BLUE : ResColors.MEDIUM_GREY)),
      onSelected: (val) => onSelected(),
    );
  }
}
