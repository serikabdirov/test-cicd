import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/text_theme.dart';

class CustomListTile extends StatelessWidget {
  final Widget? leading;
  final String title;
  final String? subtitleText;
  final TextStyle? subtitleTextStyle;
  final Widget? trailing;
  final VoidCallback? onTap;
  final VoidCallback? onTapTrailing;

  const CustomListTile({
    Key? key,
    this.leading,
    required this.title,
    this.subtitleText,
    this.subtitleTextStyle,
    this.trailing,
    this.onTap,
    this.onTapTrailing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading,
      title: Text(
        title,
        style: ResTextTheme.body1,
      ),
      subtitle: subtitleText != null
          ? Text(
              subtitleText!,
              style: ResTextTheme.body2.merge(
                const TextStyle(
                  color: ResColors.TEXT_SECONDARY,
                ).merge(subtitleTextStyle),
              ),
            )
          : null,
      trailing: Material(
        color: Colors.transparent,
        child: Container(
          width: 40,
          height: 40,
          color: Colors.transparent,
          child: trailing != null
              ? InkWell(
                  customBorder: const CircleBorder(),
                  child: trailing,
                  onTap: onTapTrailing,
                )
              : null,
        ),
      ),
      onTap: onTap,
      tileColor: ResColors.BG_GRAY_0,
      contentPadding: const EdgeInsets.symmetric(horizontal: 8),
    );
  }
}
