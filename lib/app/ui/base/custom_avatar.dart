import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/ui/base/empty_avatar.dart';

class CustomAvatar extends StatelessWidget {
  final String? url;
  final String type;
  final double width;
  final double height;
  final double radius;

  const CustomAvatar(
      {Key? key,
      this.url,
      required this.type,
      this.width = 40,
      this.height = 40,
      this.radius = 8})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(radius),
            border: Border.all(color: ResColors.MEDIUM_GREY)),
        child: url != null
            ? CachedNetworkImage(
                imageUrl: '$url',
                fit: BoxFit.contain,
                placeholder: (context, _) => EmptyAvatar(type: type),
                errorWidget: (context, _, __) => EmptyAvatar(type: type),
              )
            : EmptyAvatar(type: type));
  }
}
