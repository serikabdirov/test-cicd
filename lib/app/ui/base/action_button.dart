import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/button_style.dart';

class ActionButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String text;
  final ResButtonStyle style;
  final bool isLoading;
  final bool block;
  final IconData? icon;

  const ActionButton({
    Key? key,
    required this.text,
    required this.onTap,
    required this.style,
    this.icon,
    this.isLoading = false,
    this.block = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ElevatedButton(
        style:
            ElevatedButton.styleFrom(elevation: 0, primary: style.background),
        child: isLoading
            ? _CircleProgressIndicator(style.text)
            : Row(
                mainAxisSize: block ? MainAxisSize.max : MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (icon != null)
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Icon(
                        icon,
                        color: style.text,
                      ),
                    ),
                  Text(text.toUpperCase(), style: TextStyle(color: style.text))
                ],
              ),
        onPressed: !isLoading ? onTap : null,
      ),
    );
  }
}

class _CircleProgressIndicator extends StatelessWidget {
  final Color _color;
  final double _strokeWidth = 2;

  const _CircleProgressIndicator(this._color);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: CircularProgressIndicator(
        strokeWidth: _strokeWidth,
        valueColor: AlwaysStoppedAnimation<Color>(_color),
      ),
    );
  }
}
