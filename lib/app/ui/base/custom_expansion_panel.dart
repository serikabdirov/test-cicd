import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/text_theme.dart';

ExpansionPanel customExpansionPanel(
    {required Icon icon,
    Color? color,
    required String title,
    required Widget body,
    required bool isOpened}) {
  return ExpansionPanel(
      headerBuilder: (context, isOpen) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: icon,
              ),
              Expanded(
                  child: Text(
                title,
                style: ResTextTheme.body1,
              ))
            ],
          ),
        );
      },
      body: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16.0),
        child: body,
      ),
      backgroundColor: color,
      isExpanded: isOpened,
      canTapOnHeader: true);
}
