import 'package:flutter/material.dart';
import 'package:medtochka/app/bloc/file_manager/model/file_model.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/ui_icon.dart';

class ImagePreview extends StatelessWidget {
  final FileModel file;
  final Widget image;
  final Function(FileModel)? onTapIcon;
  final Function(FileModel)? onTapResend;

  const ImagePreview(
      {Key? key,
      required this.file,
      required this.image,
      this.onTapIcon,
      this.onTapResend})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Stack(children: [
        Align(
            alignment: Alignment.bottomLeft,
            child: ClipRRect(
                borderRadius: BorderRadius.circular(8), child: image)),
        Align(
          alignment: const Alignment(-0.3, 0.3),
          child: PhotoStatus(
              status: file.status,
              onTapResend:
                  onTapResend != null ? () => onTapResend!(file) : null),
        ),
        if (messageStatus.UPLOADING != file.status)
          Align(
              alignment: const Alignment(0.8, -0.8),
              child: SizedBox(
                width: 24,
                height: 24,
                child: RawMaterialButton(
                  onPressed: () => onTapIcon!(file),
                  elevation: 4.0,
                  highlightElevation: 4.0,
                  fillColor: Colors.white,
                  child: const Icon(
                    UiIcon.close_not_a_circle,
                    size: 8,
                  ),
                  padding: const EdgeInsets.all(4.0),
                  shape: const CircleBorder(),
                ),
              ))
      ]),
    );
  }
}

class PhotoStatus extends StatelessWidget {
  final messageStatus status;
  final VoidCallback? onTapResend;

  const PhotoStatus({Key? key, required this.status, this.onTapResend})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (status) {
      case messageStatus.UPLOADING:
        return Container(
          width: 36,
          height: 36,
          decoration: const BoxDecoration(
              shape: BoxShape.circle, color: ResColors.WHITE),
          child: const Center(
            child: SizedBox(
              width: 21,
              height: 21,
              child: CircularProgressIndicator(
                strokeWidth: 2,
              ),
            ),
          ),
        );
      case messageStatus.FAILED:
        return SizedBox(
          width: 36,
          height: 36,
          child: RawMaterialButton(
            elevation: 0,
            highlightElevation: 0,
            fillColor: Colors.white,
            child: const Icon(
              UiIcon.reset,
              size: 22.0,
            ),
            padding: const EdgeInsets.all(4.0),
            shape: const CircleBorder(),
            onPressed: onTapResend,
          ),
        );
      case messageStatus.SENT:
        return const SizedBox();
    }
  }
}
