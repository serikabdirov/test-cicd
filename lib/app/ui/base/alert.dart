import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';

class Alert extends StatelessWidget {
  final String text;

  const Alert({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
          color: ResColors.LIGHT_YELLOW,
          borderRadius: BorderRadius.circular(4)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              width: 6,
              decoration: const BoxDecoration(
                  color: ResColors.DARK_YELLOW,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(4),
                      topLeft: Radius.circular(4)))),
          Container(
              width: 40,
              height: 36,
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: const Icon(
                UiIcon.alarm,
                color: ResColors.DARK_YELLOW,
              )),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              text,
              style: ResTextTheme.body2
                  .merge(const TextStyle(color: ResColors.DARK_YELLOW)),
            ),
          ))
        ],
      ),
    );
  }
}
