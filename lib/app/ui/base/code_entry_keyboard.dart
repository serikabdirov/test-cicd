import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/common.dart';
import 'package:medtochka/app/resources/icons.dart';

class CodeEntryKeyboard extends StatelessWidget {
  final ValueChanged<String> onTapNumber;
  final VoidCallback onTapRemove;
  final VoidCallback? onTapExit;

  final double _keyboardWidth = 234.0;
  final int _buttonCount = 12;
  final int _exitBtnIndex = 9;
  final int _backSpaceBtnIndex = 11;
  final int _keyboardSliceArgument = 11;

  const CodeEntryKeyboard(
      {Key? key,
      this.onTapExit,
      required this.onTapRemove,
      required this.onTapNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _keyboardWidth,
      child: GridView.count(
          shrinkWrap: true,
          primary: true,
          physics: const NeverScrollableScrollPhysics(),
          crossAxisCount: 3,
          children: List<Widget>.generate(_buttonCount, (index) {
            String _keyboardBtnValue =
                ((index + 1) % _keyboardSliceArgument).toString();

            if (index == _exitBtnIndex) {
              return Container(
                child: onTapExit != null
                    ? KeyboardButton(
                        value: _keyboardBtnValue,
                        onTap: (value) => onTapExit!(),
                        child: Center(
                          child: Text(
                            ResCommon.TEXT_EXIT.toUpperCase(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.caption?.merge(
                                const TextStyle(color: ResColors.PRIMARY_BLUE)),
                          ),
                        ))
                    : null,
              );
            }
            if (index == _backSpaceBtnIndex) {
              return KeyboardButton(
                  value: _keyboardBtnValue,
                  onTap: (value) => onTapRemove(),
                  child: SvgPicture.asset(ResIcons.ICON_BACKSPACE));
            }

            return KeyboardButton(
                value: _keyboardBtnValue,
                onTap: (value) => onTapNumber(value),
                child: Text(
                  _keyboardBtnValue,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline5,
                ));
          })),
    );
  }
}

class KeyboardButton extends StatelessWidget {
  final String value;
  final ValueChanged<String> onTap;
  final Widget child;

  const KeyboardButton(
      {Key? key, required this.value, required this.onTap, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: Material(
        child: InkWell(
          onTap: () => onTap(value),
          customBorder: const CircleBorder(),
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
