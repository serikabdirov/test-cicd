import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/info_message.dart';
import 'package:medtochka/app/ui/base/info_widget.dart';

class EmptyMessageWidget extends StatelessWidget {
  final ResInfoMessage infoMessage;
  final VoidCallback? onTap;

  const EmptyMessageWidget({Key? key, required this.infoMessage, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: InfoWidget(
        title: infoMessage.title,
        bodyText: infoMessage.subtitle,
        imageSource: infoMessage.avatarSrc,
        buttonText: infoMessage.buttonText,
        onTap: onTap,
      ),
    );
  }
}
