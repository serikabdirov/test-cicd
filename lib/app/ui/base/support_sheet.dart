import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/core/utils/format_phone_number.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/icons.dart';
import 'package:medtochka/app/resources/support.dart';

void showSupportSheet(BuildContext context, ResSupport support) {
  final formattedSupportPhone = formatPhoneNumber(support.phone);

  final UrlLauncherService _urlLauncherService = UrlLauncherService();

  showModalBottomSheet<String>(
    context: context,
    builder: (BuildContext context) => SafeArea(
        child: Container(
      color: ResColors.WHITE,
      child: Wrap(
        children: [
          ListTile(
            title: Text(formattedSupportPhone),
            leading: SvgPicture.asset(ResIcons.ICON_MOBILE_PHONE),
            onTap: () {
              _urlLauncherService.call(formattedSupportPhone);
            },
          ),
          ListTile(
            title: Text(support.email),
            leading: SvgPicture.asset(ResIcons.ICON_MAIL),
            onTap: () {
              _urlLauncherService.openMailto(support.email);
            },
          ),
          ListTile(
            title: const Text('WhatsApp'),
            leading: SvgPicture.asset(ResIcons.ICON_WHATSAPP),
            onTap: () =>
                _handleOpenWatsApp(_urlLauncherService, support.watsApp),
          ),
        ],
      ),
    )),
  );
}

void _handleOpenWatsApp(UrlLauncherService urlLauncherService, String phone) {
  if (Platform.isIOS) {
    urlLauncherService.openWhatsAppIOS(phone);

    return;
  }

  urlLauncherService.openWhatsAppAndroid(phone);
}
