import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/file_manager/file_manager_bloc.dart';
import 'package:medtochka/app/bloc/file_manager/model/file_model.dart';
import 'package:medtochka/app/core/utils/show_server_error_alert.dart';
import 'package:medtochka/app/resources/alerts.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/file_manager/file_manager_error.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/image_preview.dart';

class FileDownload extends StatelessWidget {
  final Map<String, String> header;

  const FileDownload({Key? key, required this.header}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FileManagerBloc, FileManagerState>(
      listener: (context, state) {
        if (state.error == ResFileManagerError.ERROR_MAX_SIZE) {
          showServerErrorAlert(context, ResAlerts.maxImageSize);
        }

        if (state.error == ResFileManagerError.ERROR_WRONG_FORMAT) {
          showServerErrorAlert(context, ResAlerts.invalidFormat);
        }

        if (state.error == ResFileManagerError.ERROR_LIMIT) {
          showServerErrorAlert(context, ResAlerts.reachedLimit);
        }

        if (state.error == ResFileManagerError.ERROR_SERVER) {
          showServerErrorAlert(context, ResAlerts.serverError);
        }
      },
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 4.0),
          child: SizedBox(
            width: double.infinity,
            child: Wrap(
              children: [
                if (state.files.isNotEmpty)
                  ...state.files
                      .map<Widget>((file) => Padding(
                          padding: const EdgeInsets.only(bottom: 24),
                          child: ImagePreview(
                              file: file,
                              image: file is NetworkFile
                                  ? CachedNetworkImage(
                                      imageUrl: file.imgS,
                                      httpHeaders: header,
                                      width: 80,
                                      height: 80,
                                      fit: BoxFit.cover)
                                  : Image.file(File((file as StoreFile).path),
                                      width: 80, height: 80, fit: BoxFit.cover),
                              onTapIcon: (FileModel file) =>
                                  handleImageIconClick(context, file),
                              onTapResend: (FileModel file) =>
                                  handleImageResend(context, file))))
                      .toList(),
                if (!state.isReachedLimit())
                  SizedBox(
                    child: SizedBox(
                      width: 100,
                      height: 100,
                      child: Align(
                        alignment: Alignment.bottomLeft,
                        child: Material(
                          child: InkWell(
                            borderRadius: BorderRadius.circular(8),
                            child: Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                        color: ResColors.PRIMARY_BLUE),
                                    borderRadius: BorderRadius.circular(8)),
                                child: const Center(
                                    child: Icon(
                                  UiIcon.plus,
                                  color: ResColors.PRIMARY_BLUE,
                                ))),
                            onTap: () => showDownloadShack(
                                context, context.read<FileManagerBloc>()),
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }

  void handleImageIconClick(BuildContext context, FileModel file) {
    context.read<FileManagerBloc>().add(FileManagerDelete(file));
  }

  void handleImageResend(BuildContext context, FileModel file) {
    if (file is StoreFile) {
      context.read<FileManagerBloc>().add(FileManagerResend(file));
    }
  }

  void showDownloadShack(BuildContext context, FileManagerBloc bloc) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
            child: SingleChildScrollView(
          child: Container(
            color: ResColors.WHITE,
            child: Wrap(children: [
              ListTile(
                  title: const Text('Сделать фото'),
                  leading: const Icon(UiIcon.camera),
                  onTap: () {
                    Navigator.pop(context);

                    bloc.add(FileManagerDownloadCamera());
                  }),
              ListTile(
                  title: const Text('Добавить из галереи'),
                  leading: const Icon(UiIcon.image),
                  onTap: () {
                    Navigator.pop(context);

                    bloc.add(FileManagerDownloadGallery());
                  })
            ]),
          ),
        ));
      },
    );
  }
}
