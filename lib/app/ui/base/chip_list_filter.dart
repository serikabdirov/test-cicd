import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/colors.dart';

import 'chip_filter.dart';

class ChipListFilter extends StatelessWidget {
  final Iterable<ResChipType> chips;
  final ResChipType selectedChip;
  final void Function(ResChipType) onTap;

  const ChipListFilter(
      {Key? key, required this.onTap, required this.chips, required this.selectedChip})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      padding: const EdgeInsets.symmetric(horizontal: 4),
      alignment: Alignment.topCenter,
      color: ResColors.WHITE,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: chips
            .map<Widget>((type) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 4),
                  child: ChipFilter(
                    isSelected: selectedChip.key == type.key,
                    onSelected: () => onTap(type),
                    type: type,
                  ),
                ))
            .toList(),
      ),
    );
  }
}
