import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';

class CodeEntryIndicator extends StatelessWidget {
  final String errorMessage;
  final String infoMessage;
  final String code;
  final int codeLength;

  final double _dotSize = 12.0;

  const CodeEntryIndicator(
      {Key? key,
      required this.codeLength,
      this.errorMessage = '',
      this.code = '',
      this.infoMessage = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 8),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: List.generate(codeLength, (index) {
              bool _hasNumber = code.length >= (index + 1);

              return Container(
                margin: const EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _getCorrectDotColor(_hasNumber)),
                width: _dotSize,
                height: _dotSize,
              );
            }),
          ),
        ),
        SizedBox(height: 16, child: _getCorrectMessageWidget(context))
      ],
    );
  }

  bool _hasError() {
    return errorMessage.isNotEmpty;
  }

  bool _hasInfo() {
    return infoMessage.isNotEmpty;
  }

  Color _getCorrectDotColor(bool hasNumber) {
    if (hasNumber) {
      return _hasError() ? ResColors.PRIMARY_RED : ResColors.PRIMARY_BLUE;
    }

    return ResColors.MEDIUM_GREY;
  }

  Widget? _getCorrectMessageWidget(context) {
    if (_hasError()) {
      return Text(
        errorMessage,
        style: Theme.of(context)
            .textTheme
            .caption
            ?.merge(const TextStyle(color: ResColors.PRIMARY_RED)),
      );
    }

    if (_hasInfo()) {
      return Text(infoMessage,
          style: Theme.of(context)
              .textTheme
              .caption
              ?.merge(const TextStyle(color: ResColors.DEEP_GREY)));
    }
  }
}
