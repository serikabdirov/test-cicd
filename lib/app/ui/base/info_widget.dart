import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/text_theme.dart';

class InfoWidget extends StatelessWidget {
  final String title;
  final String imageSource;
  final String? bodyText;
  final String? buttonText;
  final VoidCallback? onTap;
  final double _maxContainerWidth = 350.0;

  static const double _maxImageSize = 200;
  static const double _minImageSize = 150;
  static const int _imageRatio = 2;

  const InfoWidget({
    Key? key,
    required this.title,
    required this.imageSource,
    this.bodyText,
    this.buttonText,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _imageSize = _getImageSize(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 8),
          child: SvgPicture.asset(
            imageSource,
            width: _imageSize,
            height: _imageSize,
          ),
        ),
        Container(
            constraints: BoxConstraints(maxWidth: _maxContainerWidth),
            margin: const EdgeInsets.only(bottom: 16),
            child: Text(title,
                textAlign: TextAlign.center, style: ResTextTheme.headline6)),
        if (bodyText != null)
          Container(
              constraints: BoxConstraints(maxWidth: _maxContainerWidth),
              margin: const EdgeInsets.only(bottom: 16),
              child: Text(
                '$bodyText',
                textAlign: TextAlign.center,
                style: ResTextTheme.body1
                    .merge(const TextStyle(color: ResColors.DEEP_GREY)),
              )),
        if (buttonText != null)
          ElevatedButton(
              onPressed: onTap,
              style: ElevatedButton.styleFrom(
                  elevation: 0, primary: ResButtonStyle.primary.background),
              child: Text(
                buttonText!.toUpperCase(),
                style: ResTextTheme.body1
                    .merge(TextStyle(color: ResButtonStyle.primary.text)),
              ))
      ],
    );
  }

  double _getImageSize(context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    double _imageSize = _screenWidth / _imageRatio;

    if (_imageSize > _maxImageSize) {
      return _maxImageSize;
    }

    if (_imageSize < _minImageSize) {
      return _minImageSize;
    }

    return _imageSize;
  }
}
