import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/resources/images.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';

class EmptyAvatar extends StatelessWidget {
  final String type;

  const EmptyAvatar({Key? key, required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      _getEmptyAvatar(type),
      fit: BoxFit.contain,
    );
  }

  String _getEmptyAvatar(type) {
    if (type == ReviewTypeKey.LPU) {
      return ResImages.EMPTY_AVATAR_LPU;
    }

    if (type == ReviewTypeKey.DRUG) {
      return ResImages.EMPTY_AVATAR_DRUG;
    }

    return ResImages.EMPTY_AVATAR_DOCTOR;
  }
}
