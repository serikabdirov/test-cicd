import 'package:flutter/material.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_text.dart';

class ReviewInfo extends StatelessWidget {
  final ReviewRate rate;
  final ReviewAbout about;

  const ReviewInfo({Key? key, required this.rate, required this.about})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReviewBasePage(
      about: about,
      reviewStatus: rate.status,
      contentWidget: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ReviewText(
          rate: rate,
        ),
      ),
      bottomWidget: ActionButton(
        block: true,
        onTap: () {},
        text: 'Связаться с поддержкой',
        style: ResButtonStyle.secondary,
      ),
    );
  }
}
