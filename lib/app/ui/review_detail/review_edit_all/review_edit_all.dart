import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:medtochka/app/bloc/file_manager/file_manager_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_all/review_edit_all_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_document/review_edit_document_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_text/review_edit_text_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/base/support_sheet.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_document/review_edit_document_widget.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_text/review_edit_text_widget.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';

class ReviewEditAll extends StatelessWidget {
  final ReviewRate rate;

  final ReviewDeclineReason? declineReason;
  final ReviewAbout about;
  final ReviewDetailRepository reviewDetailRepository;
  final TextEditingController commentTextController;
  final TextEditingController posTextController;
  final TextEditingController negTextController;
  final ReviewDetailResponse reviewDetail;
  final GlobalKey<FormState> formKey;
  final List<bool> isReasonsOpen;

  final TokenManager _tokenManager = Get.find<TokenManager>();

  ReviewEditAll({
    Key? key,
    required this.rate,
    this.declineReason,
    required this.isReasonsOpen,
    required this.reviewDetailRepository,
    required this.about,
    required this.commentTextController,
    required this.posTextController,
    required this.negTextController,
    required this.reviewDetail,
    required this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ReviewEditTextBloc>(
          create: (context) => ReviewEditTextBloc(
              reviewDetailRepository, context.read<ReviewDetailBloc>(),
              type: reviewDetail.rateType!, id: reviewDetail.rate!.id)
            ..add(ReviewEditTextFetch()),
        ),
        BlocProvider<FileManagerBloc>(
          create: (context) =>
              FileManagerBloc(reviewDetailRepository, id: rate.id!, type: about.rateType),
        ),
        BlocProvider<ReviewEditDocumentBloc>(
            create: (context) => ReviewEditDocumentBloc(
                reviewDetailRepository, context.read<FileManagerBloc>(),
                id: rate.id!, type: about.rateType, tokenManager: _tokenManager)
              ..add(ReviewEditDocumentFetch())),
        BlocProvider<ReviewEditAllBloc>(
            create: (context) => ReviewEditAllBloc(
                  context.read<ReviewDetailBloc>(),
                )),
      ],
      child: BlocBuilder<ReviewEditAllBloc, ReviewEditAllState>(
        builder: (context, state) {
          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();

              context.read<ReviewEditTextBloc>().add(ReviewEditTextSetShowCountAlert(false));
            },
            child: Stack(
              children: [
                ReviewBasePage(
                  about: reviewDetail.about,
                  reviewStatus: reviewDetail.rate!.status,
                  declineReason: reviewDetail.declineReason?.reasons,
                  isReasonsOpen: isReasonsOpen,
                  backClick: () {
                    if (state.activePage == currentPage.text) {
                      AutoRouter.of(context).pop();

                      return;
                    }

                    context.read<ReviewEditAllBloc>().add(ReviewEditBackToText());
                  },
                  contentWidget: Builder(
                    builder: (context) {
                      if (state.activePage == currentPage.text) {
                        return ReviewEditTextWidget(
                          commentTextController: commentTextController,
                          negTextController: negTextController,
                          posTextController: posTextController,
                          formKey: formKey,
                        );
                      }

                      return ReviewEditDocumentWidget();
                    },
                  ),
                  bottomWidget: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextButton(
                        child: const Text(
                          'Связаться с поддержкой',
                        ),
                        onPressed: () => showSupportSheet(context, ResSupport.changeText),
                      ),
                      BlocBuilder<FileManagerBloc, FileManagerState>(
                        builder: (context, documentState) {
                          return ActionButton(
                            block: true,
                            text: state.activePage == currentPage.text
                                ? 'Сохранить и продолжить'
                                : 'Отправить',
                            style: ResButtonStyle.primary,
                            isLoading: context.read<ReviewDetailBloc>().state.isLoading,
                            onTap: state.activePage == currentPage.text ||
                                    documentState.hasLoadedFile()
                                ? () => sandBtnClick(context, state)
                                : null,
                          );
                        },
                      ),
                    ],
                  ),
                ),
                BlocConsumer<ReviewEditTextBloc, ReviewEditTextState>(
                  listener: (context, state) {
                    if (state.isFixed) {
                      context.read<ReviewEditAllBloc>().add(ReviewEditToDocs());
                    }
                  },
                  builder: (context, state) => Visibility(
                    visible: state.showLettersCount,
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: double.infinity,
                        margin: const EdgeInsets.all(16),
                        height: 68,
                        decoration: BoxDecoration(
                            color: ResColors.TEXT_SECONDARY,
                            borderRadius: BorderRadius.circular(4)),
                        child: Center(
                          child: Text(
                            'Добавьте ещё ${state.lettersLeft()} символа в одно из трёх полей.',
                            style:
                                ResTextTheme.body2.merge(const TextStyle(color: ResColors.WHITE)),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void sandBtnClick(BuildContext context, ReviewEditAllState state) {
    if (state.activePage == currentPage.text) {
      if (formKey.currentState!.validate()) {
        context.read<ReviewEditTextBloc>().add(ReviewEditTextSend());
      }

      return;
    }

    context.read<ReviewDetailBloc>().add(ReviewDetailCompleteFix());
  }
}
