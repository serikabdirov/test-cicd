import 'package:flutter/material.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_discuss.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/ui/base/support_sheet.dart';
import 'package:medtochka/app/ui/review_detail/review_discuss/review_moderator_discuss_widget.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';

class ReviewModeratorDiscuss extends StatelessWidget {
  final ReviewDiscussion reviewDiscuss;
  final List<bool> isReasonsOpen;
  final ReviewAbout reviewAbout;
  final String reviewStatus;

  const ReviewModeratorDiscuss({
    Key? key,
    required this.isReasonsOpen,
    required this.reviewDiscuss,
    required this.reviewStatus,
    required this.reviewAbout,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReviewBasePage(
      about: reviewAbout,
      reviewStatus: reviewStatus,
      declineReason: [
        ReviewDetailReason(title: 'Сообщение от поддержки', text: reviewDiscuss.supportText)
      ],
      isReasonsOpen: isReasonsOpen,
      contentWidget: const ReviewModeratorDiscussWidget(),
      bottomWidget: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TextButton(
            child: const Text(
              'Связаться с поддержкой',
            ),
            onPressed: () => showSupportSheet(context, ResSupport.changeText),
          ),
        ],
      ),
    );
  }
}
