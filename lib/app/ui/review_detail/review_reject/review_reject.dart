import 'package:flutter/material.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_text.dart';

class ReviewReject extends StatelessWidget {
  final ReviewRate rate;
  final ReviewDeclineReason? declineReason;
  final Iterable<bool>? isReasonsOpen;
  final ReviewAbout about;

  const ReviewReject({
    Key? key,
    required this.rate,
    this.declineReason,
    this.isReasonsOpen,
    required this.about,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ReviewBasePage(
      about: about,
      reviewStatus: rate.status,
      declineReason: declineReason?.reasons,
      isReasonsOpen: isReasonsOpen,
      contentWidget: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ReviewText(
          rate: rate,
        ),
      ),
    );
  }
}
