import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:medtochka/app/bloc/review_detail/review_change_score/model/postponed_review.dart';
import 'package:medtochka/app/bloc/review_detail/review_change_score/review_change_score_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/review_detail/review_score_type.dart';
import 'package:medtochka/app/resources/review_detail/score_type.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/base/custom_expansion_panel.dart';
import 'package:medtochka/app/ui/base/support_sheet.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_text.dart';

class ReviewChangeScore extends StatelessWidget {
  final ReviewRate rate;

  final ReviewDeclineReason? declineReason;
  final Iterable<bool>? isReasonsOpen;
  final ReviewAbout about;
  final ReviewAbout aboutFrom;
  final ReviewDetailRepository reviewDetailRepository;

  const ReviewChangeScore({
    Key? key,
    required this.rate,
    this.declineReason,
    this.isReasonsOpen,
    required this.reviewDetailRepository,
    required this.about,
    required this.aboutFrom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: true,
      create: (context) => ReviewChangeScoreBloc(context.read<ReviewDetailBloc>(),
          type: about.rateType, id: rate.id!),
      child: BlocBuilder<ReviewChangeScoreBloc, ReviewChangeScoreState>(
        builder: (context, state) {
          return ReviewBasePage(
            about: about,
            reviewStatus: rate.status,
            declineReason: declineReason?.reasons,
            isReasonsOpen: isReasonsOpen,
            postponedReview: PostponedReview(aboutFrom, about),
            contentWidget: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    ExpansionPanelList(
                      children: [
                        customExpansionPanel(
                            body: ReviewText(
                              rate: rate,
                            ),
                            title: 'Ваш отзыв',
                            icon: const Icon(
                              UiIcon.Reviews,
                              color: ResColors.ICON_SECONDARY,
                            ),
                            isOpened: context.read<ReviewDetailBloc>().state.isReviewOpen)
                      ],
                      expansionCallback: (index, state) {
                        context.read<ReviewDetailBloc>().add(ReviewDetailToggleReview());
                      },
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 16, bottom: 24),
                      decoration: BoxDecoration(
                          border: Border.all(color: ResColors.LIGHT_GREY, width: 2),
                          borderRadius: BorderRadius.circular(4)),
                      child: Column(
                        children: [
                          ...state.currentScoreType.toList().asMap().entries.map<Widget>((entry) {
                            int index = entry.key;
                            ReviewScore score = entry.value;

                            return Column(
                              children: [
                                if (index != 0)
                                  const Divider(
                                    thickness: 1,
                                    color: ResColors.LIGHT_GREY,
                                  ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child:
                                      Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                    Text(score.title),
                                    if (score.detail != null)
                                      GestureDetector(
                                        child: const Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 8.0),
                                          child: Icon(
                                            UiIcon.circle_info,
                                            color: ResColors.ICON_SECONDARY,
                                          ),
                                        ),
                                        onTap: () => _openScoreDetail(context, score.detail!),
                                      )
                                  ]),
                                ),
                                ReviewScoreWidget(
                                  selectedRating: state.selectedRating[index],
                                  index: index,
                                ),
                              ],
                            );
                          })
                        ],
                      ),
                    ),
                  ],
                )),
            bottomWidget: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextButton(
                  child: const Text(
                    'Связаться с поддержкой',
                  ),
                  onPressed: () => showSupportSheet(context, ResSupport.changeScores),
                ),
                ActionButton(
                  block: true,
                  text: 'Отправить',
                  style: ResButtonStyle.primary,
                  isLoading: context.read<ReviewDetailBloc>().state.isLoading,
                  onTap: state.isAllStarSelected()
                      ? () => context.read<ReviewChangeScoreBloc>().add(ReviewChangeScoreSend())
                      : null,
                )
              ],
            ),
          );
        },
      ),
    );
  }

  _openScoreDetail(BuildContext context, String text) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Container(
                color: ResColors.WHITE,
                padding: const EdgeInsets.all(16),
                child: Text(
                  text,
                  style: ResTextTheme.body2,
                )),
          ),
        );
      },
    );
  }
}

class ReviewScoreList extends StatelessWidget {
  const ReviewScoreList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

// ignore: must_be_immutable
class ReviewScoreWidget extends StatelessWidget {
  final int selectedRating;
  final int index;
  late ScoreType currentScore;

  ReviewScoreWidget({Key? key, required this.selectedRating, required this.index})
      : super(key: key) {
    currentScore = ratingScore[selectedRating];
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RatingBar.builder(
          initialRating: 0,
          itemCount: 5,
          itemPadding: const EdgeInsets.symmetric(horizontal: 6),
          glowColor: ResColors.WARNING,
          itemBuilder: (context, index) {
            if (index < selectedRating) {
              return Icon(
                UiIcon.rating_star_yellow,
                color: currentScore.primaryColor,
              );
            }
            return const Icon(
              UiIcon.rating_star_outline,
              color: ResColors.ICON_SECONDARY,
            );
          },
          onRatingUpdate: (rating) {
            context
                .read<ReviewChangeScoreBloc>()
                .add(ReviewChangeScoreSelect(rating.toInt(), index));
          },
        ),
        Container(
            padding: const EdgeInsets.all(8),
            margin: const EdgeInsets.all(16),
            decoration: BoxDecoration(
                color: currentScore.secondaryColor, borderRadius: BorderRadius.circular(4)),
            child: Text(
              currentScore.text,
              style: TextStyle(color: currentScore.primaryColor),
            ))
      ],
    );
  }
}
