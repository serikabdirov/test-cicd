import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_document/review_edit_document_bloc.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:medtochka/app/ui/base/file_download.dart';

class ReviewEditDocumentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReviewEditDocumentBloc, ReviewEditDocumentState>(
      builder: (context, state) {
        if (state.status == pageStatus.LOADING) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: state.token != null
              ? FileDownload(header: state.getAuthHeader())
              : const SizedBox(),
        );
      },
    );
  }
}
