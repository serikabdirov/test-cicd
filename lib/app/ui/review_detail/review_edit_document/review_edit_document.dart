import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:medtochka/app/bloc/file_manager/file_manager_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_document/review_edit_document_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/base/support_sheet.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_document/review_edit_document_widget.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';

class ReviewEditDocument extends StatelessWidget {
  final ReviewRate rate;
  final ReviewDeclineReason? declineReason;
  final Iterable<bool>? isReasonsOpen;
  final ReviewAbout about;
  final ReviewDetailRepository reviewDetailRepository;
  final TokenManager _tokenManager = Get.find<TokenManager>();

  ReviewEditDocument({
    Key? key,
    required this.rate,
    this.declineReason,
    this.isReasonsOpen,
    required this.about,
    required this.reviewDetailRepository,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          lazy: true,
          create: (context) =>
              FileManagerBloc(reviewDetailRepository, id: rate.id!, type: about.rateType),
        ),
        BlocProvider(
            lazy: true,
            create: (context) => ReviewEditDocumentBloc(
                reviewDetailRepository, context.read<FileManagerBloc>(),
                id: rate.id!, type: about.rateType, tokenManager: _tokenManager)
              ..add(ReviewEditDocumentFetch())),
      ],
      child: ReviewBasePage(
        about: about,
        reviewStatus: rate.status,
        declineReason: declineReason?.reasons,
        isReasonsOpen: isReasonsOpen,
        contentWidget: ReviewEditDocumentWidget(),
        bottomWidget: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextButton(
              child: const Text(
                'Связаться с поддержкой',
              ),
              onPressed: () => showSupportSheet(context, ResSupport.changeText),
            ),
            BlocBuilder<FileManagerBloc, FileManagerState>(
              builder: (context, state) {
                var detailBloc = context.read<ReviewDetailBloc>();

                return ActionButton(
                  block: true,
                  text: 'Отправить',
                  style: ResButtonStyle.primary,
                  isLoading: detailBloc.state.isLoading,
                  onTap: state.hasLoadedFile()
                      ? () => detailBloc.add(ReviewDetailCompleteFix())
                      : null,
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
