import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_text/review_edit_text_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/base/support_sheet.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_text/review_edit_text_widget.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_base_page.dart';

class ReviewEditText extends StatelessWidget {
  final TextEditingController commentTextController;
  final TextEditingController posTextController;
  final TextEditingController negTextController;
  final ReviewDetailResponse reviewDetail;
  final GlobalKey<FormState> formKey;
  final List<bool> isReasonsOpen;
  final ReviewDetailRepository reviewDetailRepository;

  const ReviewEditText(
      {Key? key,
      required this.commentTextController,
      required this.posTextController,
      required this.negTextController,
      required this.formKey,
      required this.reviewDetail,
      required this.reviewDetailRepository,
      required this.isReasonsOpen})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      lazy: true,
      create: (context) => ReviewEditTextBloc(
          reviewDetailRepository, context.read<ReviewDetailBloc>(),
          type: reviewDetail.rateType!, id: reviewDetail.rate!.id)
        ..add(ReviewEditTextFetch()),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();

          context.read<ReviewEditTextBloc>().add(ReviewEditTextSetShowCountAlert(false));
        },
        child: Stack(
          children: [
            ReviewBasePage(
              about: reviewDetail.about,
              reviewStatus: reviewDetail.rate!.status,
              declineReason: reviewDetail.declineReason?.reasons,
              isReasonsOpen: isReasonsOpen,
              contentWidget: ReviewEditTextWidget(
                commentTextController: commentTextController,
                negTextController: negTextController,
                posTextController: posTextController,
                formKey: formKey,
              ),
              bottomWidget: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  TextButton(
                    child: const Text(
                      'Связаться с поддержкой',
                    ),
                    onPressed: () => showSupportSheet(context, ResSupport.changeText),
                  ),
                  BlocBuilder<ReviewDetailBloc, ReviewDetailState>(
                    builder: (context, pageState) {
                      return BlocBuilder<ReviewEditTextBloc, ReviewEditTextState>(
                        builder: (context, textState) {
                          return ActionButton(
                            block: true,
                            text: 'Отправить',
                            style: ResButtonStyle.primary,
                            isLoading: pageState.isLoading,
                            onTap: !textState.isBtnDisabled()
                                ? () {
                                    if (formKey.currentState!.validate()) {
                                      FocusScope.of(context).unfocus();

                                      context.read<ReviewEditTextBloc>().add(ReviewEditTextSend());
                                    }
                                  }
                                : null,
                          );
                        },
                      );
                    },
                  )
                ],
              ),
            ),
            BlocConsumer<ReviewEditTextBloc, ReviewEditTextState>(
              listener: (context, state) {
                if (state.isFixed) {
                  context.read<ReviewDetailBloc>().add(ReviewDetailCompleteFix());
                }
              },
              builder: (context, state) => Visibility(
                visible: state.showLettersCount,
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    width: double.infinity,
                    margin: const EdgeInsets.all(16),
                    height: 68,
                    decoration: BoxDecoration(
                        color: ResColors.TEXT_SECONDARY, borderRadius: BorderRadius.circular(4)),
                    child: Center(
                      child: Text(
                        'Добавьте ещё ${state.lettersLeft()} символа в одно из трёх полей.',
                        style: ResTextTheme.body2.merge(const TextStyle(color: ResColors.WHITE)),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
