import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_edit_text/review_edit_text_bloc.dart';
import 'package:medtochka/app/core/utils/show_server_error_alert.dart';
import 'package:medtochka/app/resources/alerts.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:medtochka/app/resources/review_detail/review_edit_alerts.dart';

class ReviewEditTextWidget extends StatelessWidget {
  final TextEditingController commentTextController;
  final TextEditingController posTextController;
  final TextEditingController negTextController;
  final GlobalKey<FormState> formKey;

  const ReviewEditTextWidget({
    Key? key,
    required this.commentTextController,
    required this.posTextController,
    required this.negTextController,
    required this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ReviewEditTextBloc, ReviewEditTextState>(
        listener: (context, state) {
      if (state.isDataLoaded && !state.firstLoad) {
        commentTextController.text = state.comment;
        posTextController.text = state.positive;
        negTextController.text = state.negative;

        context.read<ReviewEditTextBloc>().add(ReviewEditTextSet());
      }

      if (state.serverError) {
        var dialogData = ResAlerts.serverError;

        showServerErrorAlert(context, dialogData);
      }

      if (state.hasBadWords) {
        var dialogData = ResReviewEditAlerts.badWords;

        _showBadWordsAlert(
            context, dialogData, context.read<ReviewEditTextBloc>());
      }
    }, builder: (context, state) {
      var editTextBloc = context.read<ReviewEditTextBloc>();

      if (state.status == pageStatus.LOADING) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }

      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: TextFormField(
                    controller: commentTextController,
                    minLines: 5,
                    maxLines: 8,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Ваша история',
                        alignLabelWithHint: true),
                    validator: state.validateField,
                    onChanged: (value) {
                      onChangeText(ReviewEditTextComment(value), editTextBloc);
                    }),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: TextFormField(
                  controller: posTextController,
                  minLines: 5,
                  maxLines: 8,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Понравилось',
                      alignLabelWithHint: true),
                  validator: state.validateField,
                  onChanged: (value) {
                    onChangeText(
                        ReviewEditTextPositiveComment(value), editTextBloc);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: TextFormField(
                  controller: negTextController,
                  minLines: 5,
                  maxLines: 8,
                  decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Не понравилось',
                      alignLabelWithHint: true),
                  validator: state.validateField,
                  onChanged: (value) {
                    onChangeText(
                        ReviewEditTextNegativeComment(value), editTextBloc);
                  },
                ),
              ),
            ],
          ),
        ),
      );
    });
  }

  void _showBadWordsAlert(BuildContext context, ResReviewEditAlerts dialogData,
      ReviewEditTextBloc bloc) {
    showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        title: Text(dialogData.title),
        content: Text(dialogData.description),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              dialogData.cancelText.toUpperCase(),
            ),
          ),
          TextButton(
            onPressed: () {
              bloc.add(ReviewEditTextSend(ignoreBadWords: true));

              Navigator.pop(context);
            },
            child: Text(
              dialogData.confirmText!.toUpperCase(),
              style: const TextStyle(color: ResColors.SECONDARY),
            ),
          ),
        ],
      ),
    );
  }

  void onChangeText(ReviewEditTextEvent event, ReviewEditTextBloc bloc) {
    bloc.add(event);
    bloc.add(ReviewEditTextSaveDebounce());
    bloc.add(ReviewEditTextSetShowCountAlert(true));

    formKey.currentState!.validate();
  }
}
