import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/bloc/review_detail/review_change_score/model/postponed_review.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_reason.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';
import 'package:medtochka/app/ui/base/custom_list_tile.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_reasons.dart';

class ReviewBasePage extends StatelessWidget {
  final ReviewAbout about;
  final Iterable<ReviewDetailReason>? declineReason;
  final Iterable<bool>? isReasonsOpen;
  final String reviewStatus;
  final PostponedReview? postponedReview;
  final Widget? bottomWidget;
  final Widget contentWidget;
  final VoidCallback? backClick;

  const ReviewBasePage({
    Key? key,
    this.declineReason,
    this.isReasonsOpen,
    this.bottomWidget,
    this.postponedReview,
    this.backClick,
    required this.about,
    required this.reviewStatus,
    required this.contentWidget,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: CustomScrollView(slivers: [
            SliverAppBar(
              floating: true,
              title: Text(
                _getReviewStatus(reviewStatus),
                style: ResTextTheme.headline6,
              ),
              leading: IconButton(
                onPressed: backClick != null ? backClick! : () => AutoRouter.of(context).pop(),
                splashRadius: 24,
                icon: const Icon(UiIcon.arrow_back),
              ),
              iconTheme: const IconThemeData(color: ResColors.BLACK),
              elevation: 0,
              backgroundColor: ResColors.WHITE,
            ),
            SliverToBoxAdapter(
              child: CustomListTile(
                leading: CustomAvatar(
                  type: about.rateType,
                ),
                title: about.name,
                subtitleText: about.info,
              ),
            ),
            if (declineReason != null)
              SliverToBoxAdapter(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ReviewReasons(
                      reasons: declineReason!.toList(),
                      isReasonsOpen: isReasonsOpen!,
                      postponedReview: postponedReview,
                    )),
              ),
            SliverFillRemaining(hasScrollBody: false, child: contentWidget)
          ]),
        ),
        if (bottomWidget != null)
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            decoration: const BoxDecoration(boxShadow: [
              BoxShadow(
                  color: Colors.black, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(0.0, 5.0))
            ], color: Colors.white),
            child: bottomWidget,
          ),
      ],
    );
  }

  String _getReviewStatus(String? status) {
    if (ReviewStatusKey.ACCEPT == status) {
      return 'Опубликован';
    }

    if (ReviewStatusKey.CORRECT == status) {
      return 'Ждёт исправления';
    }

    if (ReviewStatusKey.REJECT == status) {
      return 'Снят с публикации';
    }

    if (ReviewStatusKey.ON_MOD == status) {
      return 'На модерации';
    }

    return 'Отзыв';
  }
}
