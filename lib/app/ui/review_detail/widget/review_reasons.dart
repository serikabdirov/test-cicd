import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_change_score/model/postponed_review.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_reason.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';
import 'package:medtochka/app/ui/base/custom_expansion_panel.dart';
import 'package:medtochka/app/ui/base/custom_list_tile.dart';

class ReviewReasons extends StatelessWidget {
  final List<ReviewDetailReason> reasons;
  final Iterable<bool> isReasonsOpen;
  final PostponedReview? postponedReview;

  const ReviewReasons(
      {Key? key, required this.reasons, required this.isReasonsOpen, this.postponedReview})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
      elevation: 0,
      children: reasons.asMap().entries.map<ExpansionPanel>((entry) {
        int index = entry.key;
        ReviewDetailReason reason = entry.value;

        return customExpansionPanel(
            icon: const Icon(
              UiIcon.error_message,
              color: ResColors.WARNING,
            ),
            title: reason.title,
            body: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 8,
                  ),
                  Text(reason.text, style: ResTextTheme.body2),
                  if (postponedReview != null)
                    GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text('Подробности переноса',
                            style: ResTextTheme.body2.merge(const TextStyle(
                                decoration: TextDecoration.underline,
                                color: ResColors.PRIMARY_BLUE))),
                      ),
                      onTap: () => showAboutPostponed(context, postponedReview!),
                    ),
                ],
              ),
            ),
            isOpened: isReasonsOpen.elementAt(index),
            color: ResColors.BG_WARNING);
      }).toList(),
      expandedHeaderPadding: const EdgeInsets.all(0),
      expansionCallback: (index, isOpened) => toggleReason(context, index),
    );
  }

  void toggleReason(BuildContext context, int index) {
    context.read<ReviewDetailBloc>().add(ReviewDetailToggleReason(index));
  }

  void showAboutPostponed(BuildContext context, PostponedReview postponedReview) {
    showModalBottomSheet(
      context: context,
      backgroundColor: ResColors.WHITE,
      builder: (BuildContext context) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
                    child: Text('Отзыв перенесён со страницы',
                        style:
                            ResTextTheme.body1.merge(const TextStyle(color: ResColors.TEXT_INFO))),
                  ),
                  CustomListTile(
                    leading: CustomAvatar(
                      type: postponedReview.from.rateType,
                    ),
                    title: postponedReview.from.name,
                    subtitleText: postponedReview.from.info,
                  ),
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      const Divider(
                        thickness: 1,
                        color: ResColors.BG_GRAY_60,
                      ),
                      Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            border: Border.all(color: ResColors.BG_GRAY_60),
                            color: ResColors.WHITE,
                            shape: BoxShape.circle),
                        child: const Icon(UiIcon.arrow_down_line),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
                    child: Text(
                      'на страницу',
                      style: ResTextTheme.body1.merge(const TextStyle(color: ResColors.TEXT_INFO)),
                    ),
                  ),
                  CustomListTile(
                    leading: CustomAvatar(
                      type: postponedReview.to.rateType,
                    ),
                    title: postponedReview.to.name,
                    subtitleText: postponedReview.to.info,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
