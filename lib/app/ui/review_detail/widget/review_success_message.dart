import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/images.dart';
import 'package:medtochka/app/resources/review_detail/success_message.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/base/info_widget.dart';

class ReviewSuccessMessage extends StatelessWidget {
  final SuccessMessage message;

  const ReviewSuccessMessage({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Center(
          child: InfoWidget(
            title: message.title,
            imageSource: ResImages.MASCOT_ARMCHAIR,
            bodyText: message.bodyText,
          ),
        )),
        ActionButton(
          block: true,
          text: 'к моим отзывам',
          onTap: () => AutoRouter.of(context).pop(),
          style: ResButtonStyle.primary,
        )
      ],
    );
  }
}
