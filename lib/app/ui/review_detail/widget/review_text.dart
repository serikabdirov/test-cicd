import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_score.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_score_detail.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/review_detail/score_type.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';

class ReviewText extends StatelessWidget {
  final ReviewRate rate;

  const ReviewText({Key? key, required this.rate}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _getFormattedDate(rate.dtCreated, 'd MMMM y'),
                    style: ResTextTheme.body2,
                  ),
                  Text(_getFormattedDate(rate.dtCreated, 'HH:mm'),
                      style: ResTextTheme.body2
                          .merge(const TextStyle(color: ResColors.TEXT_INFO)))
                ],
              ),
              _ReviewScore(
                score: rate.score,
              )
            ],
          ),
        ),
        ...rate.text
            .map<Widget>((text) => _ReviewText(
                  title: text.title,
                  text: text.comment,
                ))
            .toList(),
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: Row(
            children: [
              if (rate.byCall != null)
                Expanded(
                    child: _ReviewSection(
                  title: 'Отзыв через',
                  text: rate.byCall!
                      ? Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: const [
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Icon(
                                  UiIcon.phone_call,
                                  color: ResColors.ICON_SECONDARY,
                                ),
                              ),
                              Text(
                                'Колл-центр',
                                style: ResTextTheme.body2,
                              )
                            ])
                      : Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: const [
                              Padding(
                                padding: EdgeInsets.only(right: 8.0),
                                child: Icon(
                                  UiIcon.site,
                                  color: ResColors.ICON_SECONDARY,
                                ),
                              ),
                              Text(
                                'Сайт',
                                style: ResTextTheme.body2,
                              )
                            ]),
                )),
              if (rate.dtVisit.isNotEmpty)
                Expanded(
                    child: _ReviewSection(
                  title: 'Дата посещения',
                  text: Text(
                    _getFormattedDate(rate.dtVisit, 'd MMMM'),
                    style: ResTextTheme.body2,
                  ),
                ))
            ],
          ),
        ),
        Visibility(
          visible: rate.address.isNotEmpty,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: _ReviewSection(
                title: 'Место посещения',
                text: Text(
                  rate.address,
                  style: ResTextTheme.body2,
                )),
          ),
        )
      ],
    );
  }

  String _getFormattedDate(String date, String format) {
    DateTime dateTime = DateTime.parse(date);

    return DateFormat(format).format(dateTime);
  }
}

class _ReviewText extends StatelessWidget {
  final String title;
  final String text;

  const _ReviewText({Key? key, required this.title, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title.isNotEmpty)
          Container(
            margin: const EdgeInsets.only(bottom: 8),
            child: Text(
              title,
              style: ResTextTheme.subtitle2,
            ),
          ),
        Container(
          margin: const EdgeInsets.only(bottom: 16),
          child: Text(text, style: ResTextTheme.body1),
        ),
      ],
    );
  }
}

class _ReviewSection extends StatelessWidget {
  final String title;
  final Widget text;

  const _ReviewSection({Key? key, required this.title, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            title,
            style: ResTextTheme.body2
                .merge(const TextStyle(color: ResColors.TEXT_INFO)),
          ),
        ),
        text,
      ],
    );
  }
}

class _ReviewScore extends StatelessWidget {
  final ReviewScore score;

  const _ReviewScore({Key? key, required this.score}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return score.type != null && score.value != null
        ? GestureDetector(
            child: Row(
              children: [
                Container(
                  width: 44,
                  height: 34,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: _getCurrentType(score.type!).primaryColor,
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(4),
                          bottomLeft: Radius.circular(4))),
                  child: Text(
                    _getFormattedValue(score.value!),
                    style: const TextStyle(color: ResColors.WHITE),
                  ),
                ),
                Container(
                    width: 80,
                    height: 34,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: _getCurrentType(score.type!).secondaryColor,
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(4),
                            bottomRight: Radius.circular(4))),
                    child: Text(
                      _getCurrentType(score.type!).text,
                      style: TextStyle(
                          color: _getCurrentType(score.type!).primaryColor),
                    )),
              ],
            ),
            onTap: () => _openScoreDetail(context, score.detail),
          )
        : const SizedBox();
  }

  _openScoreDetail(BuildContext context, Iterable<ReviewScoreDetail> detail) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: SingleChildScrollView(
            child: Container(
              color: ResColors.WHITE,
              padding: const EdgeInsets.all(16),
              child: Wrap(
                children: detail
                    .where((element) =>
                        element.text != null && element.type != null)
                    .map((score) {
                  var type = _getCurrentType(score.type!);

                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          flex: 3,
                          child: Text(score.text!, style: ResTextTheme.body2),
                        ),
                        Expanded(
                          flex: 1,
                          child: Text(
                            type.text,
                            style: ResTextTheme.body2
                                .merge(TextStyle(color: type.primaryColor)),
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        );
      },
    );
  }

  String _getFormattedValue(double value) {
    if (value > 0) {
      return '+$value';
    }

    return '$value';
  }
}

ScoreType _getCurrentType(String type) {
  if (ScoreType.awful.type == type) {
    return ScoreType.awful;
  }

  if (ScoreType.bad.type == type) {
    return ScoreType.bad;
  }

  if (ScoreType.normal.type == type) {
    return ScoreType.normal;
  }

  if (ScoreType.good.type == type) {
    return ScoreType.good;
  }

  if (ScoreType.excellent.type == type) {
    return ScoreType.excellent;
  }

  return ScoreType.empty;
}
