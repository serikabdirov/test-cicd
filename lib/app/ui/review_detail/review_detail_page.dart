import 'package:auto_route/auto_route.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/bloc/reviews/reviews_bloc.dart';
import 'package:medtochka/app/core/utils/show_server_error_alert.dart';
import 'package:medtochka/app/injector.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository_impl.dart';
import 'package:medtochka/app/resources/alerts.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:medtochka/app/resources/review_detail/review_edit_type.dart';
import 'package:medtochka/app/resources/reviews/review_action_type.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';
import 'package:medtochka/app/ui/review_detail/review_discuss/review_moderator_discuss.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_all/review_edit_all.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_document/review_edit_document.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_score/review_change_score.dart';
import 'package:medtochka/app/ui/review_detail/review_edit_text/review_edit_text.dart';
import 'package:medtochka/app/ui/review_detail/review_info/review_info.dart';
import 'package:medtochka/app/ui/review_detail/review_reject/review_reject.dart';
import 'package:medtochka/app/ui/review_detail/widget/review_success_message.dart';

class ReviewDetailPage extends StatefulWidget {
  final int id;
  final String type;
  final ReviewDetailRepository _reviewDetailRepository =
      ReviewDetailRepositoryImpl(Get.find<Dio>(tag: DependenciesInitializer.MAIN_CLIENT_TAG));

  ReviewDetailPage({
    Key? key,
    @PathParam('id') required this.id,
    @PathParam('type') required this.type,
  }) : super(key: key);

  @override
  _ReviewDetailPageState createState() => _ReviewDetailPageState();
}

class _ReviewDetailPageState extends State<ReviewDetailPage> {
  final _commentTextController = TextEditingController();
  final _posTextController = TextEditingController();
  final _negTextController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    context.read<ReviewDetailBloc>().add(ReviewDetailFetch(widget.id, widget.type));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ReviewDetailBloc, ReviewDetailState>(
      buildWhen: (prevState, newState) => !newState.serverError,
      listener: (context, state) {
        if (state.serverError) {
          var dialogData = ResAlerts.serverError;

          showServerErrorAlert(context, dialogData);
        }
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: ResColors.WHITE,
          body: SafeArea(
            child: Builder(
              builder: (context) {
                if (state.status == pageStatus.FIXED) {
                  context.read<ReviewsBloc>().add(ReviewsFetch());

                  return ReviewSuccessMessage(
                    message: state.successMessage,
                  );
                }

                if (isRateLoaded(state.status, state.reviewDetail)) {
                  var reviewDetail = state.reviewDetail!;

                  if (reviewDetail.actionType == ResReviewsActionType.DISCUSSION) {
                    return ReviewModeratorDiscuss(
                      isReasonsOpen: state.isReasonsOpen,
                      reviewDiscuss: reviewDetail.discussion!,
                      reviewStatus: state.reviewDetail!.status!,
                      reviewAbout: reviewDetail.about,
                    );
                  }

                  if (isCurrentEditType(
                      ReviewEditType.TEXT, reviewDetail.declineReason?.editType)) {
                    return ReviewEditText(
                      commentTextController: _commentTextController,
                      posTextController: _posTextController,
                      negTextController: _negTextController,
                      formKey: _formKey,
                      reviewDetail: state.reviewDetail!,
                      reviewDetailRepository: widget._reviewDetailRepository,
                      isReasonsOpen: state.isReasonsOpen,
                    );
                  }

                  if (isCurrentEditType(
                      ReviewEditType.CHANGE_SCORE, reviewDetail.declineReason?.editType)) {
                    return ReviewChangeScore(
                      rate: reviewDetail.rate!,
                      about: reviewDetail.about,
                      aboutFrom: reviewDetail.aboutFrom!,
                      declineReason: reviewDetail.declineReason,
                      isReasonsOpen: state.isReasonsOpen,
                      reviewDetailRepository: widget._reviewDetailRepository,
                    );
                  }

                  if (isCurrentEditType(
                      ReviewEditType.DOCUMENT, reviewDetail.declineReason?.editType)) {
                    return ReviewEditDocument(
                        rate: reviewDetail.rate!,
                        about: reviewDetail.about,
                        declineReason: reviewDetail.declineReason,
                        isReasonsOpen: state.isReasonsOpen,
                        reviewDetailRepository: widget._reviewDetailRepository);
                  }

                  if (isCurrentEditType(
                      ReviewEditType.RATE, reviewDetail.declineReason?.editType)) {
                    return ReviewEditAll(
                      commentTextController: _commentTextController,
                      posTextController: _posTextController,
                      negTextController: _negTextController,
                      formKey: _formKey,
                      reviewDetail: state.reviewDetail!,
                      reviewDetailRepository: widget._reviewDetailRepository,
                      isReasonsOpen: state.isReasonsOpen,
                      about: state.reviewDetail!.about,
                      rate: state.reviewDetail!.rate!,
                    );
                  }

                  if (isRateRejected(reviewDetail.rate!.status, reviewDetail.declineReason)) {
                    return ReviewReject(
                      rate: reviewDetail.rate!,
                      about: reviewDetail.about,
                      declineReason: reviewDetail.declineReason,
                      isReasonsOpen: state.isReasonsOpen,
                    );
                  }

                  return ReviewInfo(
                    rate: reviewDetail.rate!,
                    about: reviewDetail.about,
                  );
                }
                if (state.status == pageStatus.LOADING) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                return Container();
              },
            ),
          ),
        );
      },
    );
  }

  bool isRateLoaded(status, ReviewDetailResponse? rate) {
    return status == pageStatus.SUCCESS && rate != null;
  }

  bool isCurrentEditType(String status, String? editStatus) {
    return status == editStatus;
  }

  bool isRateRejected(String status, ReviewDeclineReason? declineReason) {
    return status == ReviewStatusKey.REJECT && declineReason != null;
  }
}
