import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/appointments/appointments_bloc.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/resources/appointments/appointments.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/info_message.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/ui/base/chip_list_filter.dart';
import 'package:medtochka/app/ui/base/empty_message_widget.dart';

import 'appointment_item.dart';

class AppointmentsPage extends StatelessWidget {
  final int _isLoadedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ResColors.LIGHTEST_BLUE,
      body: SafeArea(
        child: BlocBuilder<AppointmentsBloc, AppointmentsState>(
          builder: (context, state) {
            return CustomScrollView(slivers: [
              const SliverAppBar(
                floating: true,
                title: Text(
                  ResAppointments.APP_BAR_TITLE,
                  style: ResTextTheme.headline6,
                ),
                elevation: 0,
                backgroundColor: ResColors.WHITE,
              ),
              SliverToBoxAdapter(
                  child: Container(
                height: 48,
                color: ResColors.WHITE,
                margin: const EdgeInsets.only(bottom: 24),
                child: ChipListFilter(
                  selectedChip: state.selectedType,
                  chips: state.chipOptions,
                  onTap: (type) => _fetchData(context: context, type: type),
                ),
              )),
              if (_isLoading(state))
                const SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              if (_isError(state))
                SliverToBoxAdapter(
                  child: EmptyMessageWidget(
                    infoMessage: ResInfoMessage.errorInternal,
                    onTap: () => _fetchData(context: context),
                  ),
                ),
              if (_isLoadedSuccess(state))
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      if (state.appointments.isEmpty) {
                        return EmptyMessageWidget(infoMessage: state.selectedType.emptyMessage);
                      }

                      if (state.selectedType == ResChipType.appointmentUpcoming) {
                        if (index == 0) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 8, bottom: 8),
                                child: Text(
                                  ResAppointments.APPOINTMENT_NEAREST.toUpperCase(),
                                  style: ResTextTheme.caption,
                                ),
                              ),
                              AppointmentItem(
                                appointment: state.appointments.elementAt(index),
                                onTap: (int id) {
                                  _openDetail(context, id);
                                },
                              )
                            ],
                          );
                        }

                        if (index == 1) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(left: 8, top: 24, bottom: 8),
                                child: Text(
                                  ResAppointments.APPOINTMENT_REST.toUpperCase(),
                                  style: ResTextTheme.caption,
                                ),
                              ),
                              AppointmentItem(
                                appointment: state.appointments.elementAt(index),
                                onTap: (int id) {
                                  _openDetail(context, id);
                                },
                              )
                            ],
                          );
                        }
                      }

                      return AppointmentItem(
                        appointment: state.appointments.elementAt(index),
                        onTap: (int id) {
                          _openDetail(context, id);
                        },
                      );
                    },
                    childCount: getCorrectItemsCount(state),
                  ),
                ),
            ]);
          },
        ),
      ),
    );
  }

  void _fetchData({
    required BuildContext context,
    ResChipType? type,
  }) {
    context.read<AppointmentsBloc>().add(AppointmentsFetch(type: type));
  }

  void _openDetail(BuildContext context, int id) {
    AutoRouter.of(context).navigate(AppointmentDetailRoute(id: id));
  }

  bool _isLoadedSuccess(AppointmentsState state) {
    return (state.status == listStatus.SUCCESS);
  }

  bool _isLoading(AppointmentsState state) {
    return (state.status == listStatus.LOADING);
  }

  bool _isError(AppointmentsState state) {
    return (state.status == listStatus.ERROR);
  }

  int getCorrectItemsCount(AppointmentsState state) {
    if (_isLoadedSuccess(state) && state.appointments.isNotEmpty) {
      return state.appointments.length;
    }

    return _isLoadedIndex;
  }
}
