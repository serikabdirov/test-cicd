import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medtochka/app/repositories/appointments/model/appointments_response.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';

class AppointmentItem extends StatelessWidget {
  final AppointmentsResponse appointment;
  final Function(int) onTap;

  const AppointmentItem({
    Key? key,
    required this.appointment,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.zero,
      child: InkWell(
        onTap: () => onTap(appointment.id),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8),
          child: SizedBox(
            height: 118,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                            _getFormattedDate(
                                appointment.dtVisit, appointment.timeStart),
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                color: ResColors.BLACK, fontSize: 16)),
                        const Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: Icon(
                            UiIcon.arrow_right,
                            size: 24,
                            color: ResColors.MEDIUM_GREY,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 8),
                        child: CustomAvatar(
                          url: appointment.avatarPath,
                          type: ReviewTypeKey.DOCTOR,
                        ),
                      ),
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              child: Text(appointment.doctorName,
                                  overflow: TextOverflow.ellipsis,
                                  style: ResTextTheme.body1)),
                          Expanded(
                            child: Text(appointment.specialityName,
                                overflow: TextOverflow.ellipsis,
                                style: ResTextTheme.body2.merge(const TextStyle(
                                    color: ResColors.DEEP_GREY))),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                const Padding(
                                  padding: EdgeInsets.only(right: 4),
                                  child: Icon(
                                    UiIcon.map_pin,
                                    size: 16,
                                    color: ResColors.DEEP_GREY,
                                  ),
                                ),
                                Expanded(
                                  child: Text(appointment.lpuName,
                                      overflow: TextOverflow.ellipsis,
                                      style: ResTextTheme.body2.merge(
                                          const TextStyle(
                                              color: ResColors.DEEP_GREY))),
                                ),
                              ],
                            ),
                          )
                        ],
                      ))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  String _getFormattedDate(String date, String time) {
    DateTime dateTime = DateTime.parse('$date $time');

    return DateFormat('d MMMM, y, E, HH:mm').format(dateTime);
  }
}
