import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medtochka/app/bloc/code_entry/code_entry_bloc.dart';
import 'package:medtochka/app/bloc/create_auth_code/create_auth_code_bloc.dart';
import 'package:medtochka/app/core/router/router_path.dart';
import 'package:medtochka/app/resources/create_auth_code/create_auth_code.dart';
import 'package:medtochka/app/resources/icons.dart';
import 'package:medtochka/app/ui/base/code_entry_indicator.dart';
import 'package:medtochka/app/ui/base/code_entry_keyboard.dart';

class CreateAuthCodePage extends StatelessWidget {
  const CreateAuthCodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CreateAuthCodeBloc, CreateAuthCodeState>(
      listener: (context, state) {
        if (state is CreateAuthCodeConfirm && state.isConfirmed) {
          _openHomePage(context);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: (state is CreateAuthCodeConfirm && !state.isLoading)
                ? IconButton(
                    icon: SvgPicture.asset(ResIcons.ICON_ARROW_BACK),
                    onPressed: () {
                      _onTapBack(context);
                    },
                  )
                : null,
          ),
          body: SafeArea(
            child: Center(
              child: Column(
                children: [
                  Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                          Container(
                            margin: const EdgeInsets.only(bottom: 40),
                            child: (state is CreateAuthCodeInitial)
                                ? const Text(ResCreateAuthCode.CREATE_CODE_TITLE)
                                : const Text(ResCreateAuthCode.CONFIRM_CODE_TITLE),
                          ),
                          Container(
                            height: 40,
                            alignment: Alignment.center,
                            child: (state is CreateAuthCodeConfirm && state.isLoading)
                                ? const SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                    ),
                                  )
                                : BlocBuilder<CreateCodeEntryBloc, CodeEntryState>(
                                    builder: (context, state) {
                                      return CodeEntryIndicator(
                                        code: state.code,
                                        codeLength: state.codeLength,
                                        errorMessage: state.errorMessage,
                                        infoMessage: state.infoMessage,
                                      );
                                    },
                                  ),
                          )
                        ]),
                      )),
                  Expanded(
                      flex: 6,
                      child: Center(
                        child: CodeEntryKeyboard(onTapNumber: (String value) {
                          _onTapNumber(context, value);
                        }, onTapRemove: () {
                          _onTapRemove(context);
                        }),
                      )),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _onTapNumber(BuildContext context, String value) {
    context.read<CreateCodeEntryBloc>().add(CodeEntryTapNumber(value));
  }

  void _onTapRemove(BuildContext context) {
    context.read<CreateCodeEntryBloc>().add(CodeEntryRemoveNumber());
  }

  void _onTapBack(BuildContext context) {
    context.read<CreateAuthCodeBloc>().add(CreateAuthCodeSetInitial());
  }

  void _openHomePage(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.MAIN_PAGE);
  }
}
