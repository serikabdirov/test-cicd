import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medtochka/app/bloc/reviews/model/reviews.dart';
import 'package:medtochka/app/resources/base_url.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/common.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';

class ReviewItemWidget extends StatelessWidget {
  final ReviewItem reviewItem;
  final Function(ReviewItem)? onTap;

  const ReviewItemWidget({Key? key, required this.reviewItem, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.zero,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 16),
                child: CustomAvatar(
                  url: _getAvatarUrl(reviewItem.avatar, reviewItem.status),
                  type: reviewItem.rateType,
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(reviewItem.name,
                        overflow: TextOverflow.ellipsis, style: ResTextTheme.body1),
                    reviewItem.id != null
                        ? Row(
                            children: [
                              Expanded(
                                child: Text(
                                  reviewItem.dtCreated!,
                                  overflow: TextOverflow.ellipsis,
                                  style: ResTextTheme.body2
                                      .merge(const TextStyle(color: ResColors.TEXT_SECONDARY)),
                                ),
                              ),
                              if (reviewItem.messageCount != null)
                                Expanded(child: Text(_getChatText(reviewItem.messageCount!)))
                            ],
                          )
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (reviewItem.info != null)
                                Text(
                                  reviewItem.info!,
                                  overflow: TextOverflow.ellipsis,
                                  style: ResTextTheme.body2
                                      .merge(const TextStyle(color: ResColors.TEXT_SECONDARY)),
                                ),
                              if (reviewItem.dtVisit != null)
                                Text(
                                  'Дата приёма ${_formatVisitDate(reviewItem.dtVisit!)}',
                                  overflow: TextOverflow.ellipsis,
                                  style: ResTextTheme.body2
                                      .merge(const TextStyle(color: ResColors.TEXT_SECONDARY)),
                                ),
                            ],
                          ),
                  ],
                ),
              ),
              const Icon(
                UiIcon.arrow_right,
                color: ResColors.ICON_SECONDARY,
              )
            ],
          ),
        ),
      ),
      onTap: () => onTap?.call(reviewItem),
    );
  }

  String _getChatText(int messageCount) {
    return messageCount > 0 ? '${ResCommon.TEXT_CHAT} ($messageCount)' : ResCommon.TEXT_CHAT;
  }

  String? _getAvatarUrl(String? url, String status) {
    if (url == null) {
      return null;
    }

    if (status == ReviewStatusKey.WAITING) {
      return '${ResBaseUrl.INFO}/$url';
    }

    return '${ResBaseUrl.MAIN}/$url';
  }

  String _formatVisitDate(String date) {
    DateTime dateTime = DateTime.parse(date);

    return DateFormat('dd.MM.yyyy').format(dateTime);
  }
}
