import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/reviews/review_status.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';

class CategoryTitle extends StatelessWidget {
  final ResReviewStatus category;

  const CategoryTitle({Key? key, required this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, top: 24, bottom: 8),
      child: Row(
        children: [
          if (category.status == ReviewStatusKey.CORRECT)
            const Padding(
              padding: EdgeInsets.only(right: 8.0),
              child: Icon(
                UiIcon.error_message,
                color: ResColors.WARNING,
                size: 20,
              ),
            ),
          Text(
            category.name.toUpperCase(),
            style: ResTextTheme.overline,
          ),
        ],
      ),
    );
  }
}
