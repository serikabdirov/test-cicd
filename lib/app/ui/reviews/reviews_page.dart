import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:medtochka/app/bloc/reviews/model/reviews.dart';
import 'package:medtochka/app/bloc/reviews/reviews_bloc.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/core/utils/get_rate_link.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/info_message.dart';
import 'package:medtochka/app/resources/reviews/reviews.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/ui/base/chip_list_filter.dart';
import 'package:medtochka/app/ui/base/empty_message_widget.dart';
import 'package:medtochka/app/ui/reviews/widgets/category_title.dart';
import 'package:medtochka/app/ui/reviews/widgets/review_item_widget.dart';

class ReviewsPage extends StatelessWidget {
  final int _isLoadedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ResColors.LIGHTEST_BLUE,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: BlocBuilder<ReviewsBloc, ReviewsState>(
                builder: (context, state) {
                  return CustomScrollView(slivers: [
                    const SliverAppBar(
                      floating: true,
                      title: Text(
                        ResReviews.APP_BAR_TITLE,
                        style: ResTextTheme.headline6,
                      ),
                      elevation: 0,
                      backgroundColor: ResColors.WHITE,
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                      height: 48,
                      color: ResColors.WHITE,
                      child: ChipListFilter(
                        chips: state.chipOptions,
                        selectedChip: state.selectedType,
                        onTap: (type) => _fetchData(context: context, type: type),
                      ),
                    )),
                    if (_isLoading(state))
                      const SliverFillRemaining(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    if (_isError(state))
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 24.0),
                          child: EmptyMessageWidget(
                            infoMessage: ResInfoMessage.errorInternal,
                            onTap: () => _fetchData(context: context),
                          ),
                        ),
                      ),
                    if (_isLoadedSuccess(state))
                      SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (BuildContext context, int index) {
                            if (state.reviews.isEmpty) {
                              return Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: EmptyMessageWidget(
                                  infoMessage: state.selectedType.emptyMessage,
                                  onTap: () => context.read<ReviewsBloc>().add(ReviewsGoToPd()),
                                ),
                              );
                            }

                            return _ReviewWidget(
                              review: state.reviews.elementAt(index),
                            );
                          },
                          childCount: getCorrectItemsCount(state),
                        ),
                      )
                  ]);
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void _fetchData({required BuildContext context, ResChipType? type}) {
    context.read<ReviewsBloc>().add(ReviewsFetch(type: type));
  }

  bool _isLoadedSuccess(ReviewsState state) {
    return (state.status == listStatus.SUCCESS);
  }

  bool _isLoading(ReviewsState state) {
    return (state.status == listStatus.LOADING);
  }

  bool _isError(ReviewsState state) {
    return (state.status == listStatus.ERROR);
  }

  int getCorrectItemsCount(ReviewsState state) {
    if (_isLoadedSuccess(state) && state.reviews.isNotEmpty) {
      return state.reviews.length;
    }

    return _isLoadedIndex;
  }
}

class _ReviewWidget extends StatelessWidget {
  final Reviews review;

  const _ReviewWidget({Key? key, required this.review}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (review is ReviewTitle) {
      return CategoryTitle(category: (review as ReviewTitle).title);
    }

    return ReviewItemWidget(
        reviewItem: review as ReviewItem,
        onTap: (reviewItem) => handleReviewClick(context: context, reviewItem: reviewItem));
  }

  void handleReviewClick({required BuildContext context, required ReviewItem reviewItem}) {
    var _urlLauncherService = Get.find<UrlLauncherService>();

    if (reviewItem.id != null) {
      _openReviewDetail(context, reviewItem.id!, reviewItem.rateType);

      return;
    }

    if (reviewItem.doctorId != null && reviewItem.dtVisit != null) {
      _urlLauncherService.openWeb(
        getRateLink(
          type: reviewItem.rateType,
          lpuId: reviewItem.lpuId,
          doctorId: reviewItem.doctorId!,
          dateVisit: reviewItem.dtVisit!,
        ),
      );
    }
  }

  void _openReviewDetail(BuildContext context, int id, String type) async {
    await AutoRouter.of(context).navigate(ReviewDetailRoute(id: id, type: type));
  }
}
