import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/slider/slider_cubit.dart';
import 'package:medtochka/app/bloc/tour/tour_bloc.dart';
import 'package:medtochka/app/core/router/router_path.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/common.dart';
import 'package:medtochka/app/ui/base/action_button.dart';
import 'package:medtochka/app/ui/tour/tour_dot.dart';

class TourPage extends StatelessWidget {
  final PageController _pageController = PageController(initialPage: 0);
  final int _durationInMillisecond = 1;
  final Curve _curveMode = Curves.easeIn;

  TourPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocListener<TourBloc, TourState>(
          listener: (context, state) {
            if (state is TourShowAuth) {
              _openAuthPage(context);
            }
          },
          child: BlocBuilder<SliderCubit, SliderState>(
            builder: (context, state) {
              return Column(
                children: [
                  Expanded(
                    child: Center(
                      child: PageView.builder(
                          itemCount: state.totalSlides,
                          controller: _pageController,
                          onPageChanged: (page) => context.read<SliderCubit>().onSlideChanged(page),
                          itemBuilder: (context, int index) {
                            return Container(
                              margin: const EdgeInsets.all(8),
                              child: state.tourSlides[index],
                            );
                          }),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 14),
                    child: TourDotWidget(
                        itemCount: state.totalSlides,
                        activeItem: state.page,
                        onPageSelected: (int page) {
                          _goToPage(page, context);
                        }),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 8),
                    child: ActionButton(
                      block: true,
                      text: _getButtonText(state),
                      style: ResButtonStyle.primary,
                      onTap: () {
                        _onNextTap(state, context);
                      },
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  String _getButtonText(SliderState state) {
    return state.isLastPage ? ResCommon.TEXT_ENTER : ResCommon.TEXT_CONTINUE;
  }

  void _onNextTap(SliderState state, BuildContext context) {
    if (!state.isLastPage) {
      _goToPage(state.page + 1, context);
      return;
    }

    context.read<TourBloc>().add(TourCompleted());
  }

  void _goToPage(int page, BuildContext context) {
    _pageController.animateToPage(
      page,
      duration: Duration(milliseconds: _durationInMillisecond),
      curve: _curveMode,
    );
  }

  void _openAuthPage(BuildContext context) async {
    AutoRouter.of(context).replaceNamed(RouterPath.LOGIN);
  }
}
