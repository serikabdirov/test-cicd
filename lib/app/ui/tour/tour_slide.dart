import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:medtochka/app/resources/colors.dart';

class TourSlide extends StatelessWidget {
  final String title;
  final String bodyText;
  final String imageSource;
  final double _maxContainerWidth = 350.0;

  static const double _maxImageSize = 200;
  static const double _minImageSize = 150;
  static const int _imageRatio = 2;

  const TourSlide(
      {Key? key,
      required this.title,
      required this.bodyText,
      required this.imageSource})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _imageSize = _getImageSize(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 8),
          child: SvgPicture.asset(
            imageSource,
            width: _imageSize,
            height: _imageSize,
          ),
        ),
        Container(
            constraints: BoxConstraints(maxWidth: _maxContainerWidth),
            margin: const EdgeInsets.only(bottom: 16),
            child: Text(title,
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 20, color: ResColors.BLACK))),
        Container(
            constraints: BoxConstraints(maxWidth: _maxContainerWidth),
            child: Text(
              bodyText,
              textAlign: TextAlign.center,
              style: const TextStyle(fontSize: 16, color: ResColors.DEEP_GREY),
            ))
      ],
    );
  }

  double _getImageSize(context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    double _imageSize = _screenWidth / _imageRatio;

    if (_imageSize > _maxImageSize) {
      return _maxImageSize;
    }

    if (_imageSize < _minImageSize) {
      return _minImageSize;
    }

    return _imageSize;
  }
}
