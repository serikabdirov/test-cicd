import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';

class TourDotWidget extends StatelessWidget {
  const TourDotWidget(
      {required this.itemCount,
      required this.activeItem,
      required this.onPageSelected});

  final int itemCount;
  final int activeItem;
  final ValueChanged<int> onPageSelected;

  final double _dotSize = 8.0;

  Widget _buildDot(int index) {
    return Container(
      margin: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: index == activeItem
            ? ResColors.PRIMARY_BLUE
            : ResColors.MEDIUM_GREY,
      ),
      width: _dotSize,
      height: _dotSize,
      child: GestureDetector(
        onTap: () => onPageSelected(index),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(itemCount, _buildDot),
    );
  }
}
