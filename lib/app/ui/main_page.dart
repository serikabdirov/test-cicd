import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/main/res_main_page.dart';
import 'package:medtochka/app/resources/ui_icon.dart';

class MainPage extends StatelessWidget {
  static const _selectedColor = ResColors.PRIMARY_BLUE;
  static const _unselectedColor = ResColors.DEEP_GREY;
  static const _textSize = 12.0;

  final _tabs = [
    const BottomNavigationBarItem(
        icon: Icon(UiIcon.home_MedLock, color: _unselectedColor),
        activeIcon: Icon(UiIcon.home_MedLock, color: _selectedColor),
        label: ResMainPage.LABEL_HOME),
    const BottomNavigationBarItem(
        icon: Icon(UiIcon.calendar_date_and_clock, color: _unselectedColor),
        activeIcon: Icon(UiIcon.calendar_date_and_clock, color: _selectedColor),
        label: ResMainPage.LABEL_APPOINTMENTS),
    const BottomNavigationBarItem(
        icon: Icon(UiIcon.Reviews, color: _unselectedColor),
        activeIcon: Icon(UiIcon.Reviews, color: _selectedColor),
        label: ResMainPage.LABEL_REVIEWS),
    const BottomNavigationBarItem(
        icon: Icon(UiIcon.heart, color: _unselectedColor),
        activeIcon: Icon(UiIcon.heart, color: _selectedColor),
        label: ResMainPage.LABEL_FAVORITES),
  ];

  @override
  Widget build(BuildContext context) {
    return AutoTabsScaffold(
      routes: const [
        HomeRoute(),
        Appointments(),
        ReviewsRoute(),
        FavouritesRoute(),
      ],
      lazyLoad: false,
      animationDuration: Duration.zero,
      bottomNavigationBuilder: (_, tabsRouter) => BottomNavigationBar(
        items: _tabs,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        backgroundColor: ResColors.WHITE,
        unselectedItemColor: _unselectedColor,
        selectedItemColor: _selectedColor,
        selectedFontSize: _textSize,
        currentIndex: tabsRouter.activeIndex,
        onTap: tabsRouter.setActiveIndex,
      ),
    );
  }
}
