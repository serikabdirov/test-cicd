import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:medtochka/app/bloc/auth/auth_bloc.dart';
import 'package:medtochka/app/bloc/auth/auth_event.dart';
import 'package:medtochka/app/bloc/auth/auth_state.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/core/utils/create_sanck_bar.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/resources/auth/res_auth.dart';
import 'package:medtochka/app/resources/base_url.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/document_link.dart';
import 'package:medtochka/app/resources/errors.dart';
import 'package:medtochka/app/resources/images.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/resources/text_theme.dart';

class AuthPage extends StatefulWidget {
  @override
  State createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  static const String _PHONE_MASK = '+7(###)###-##-##';

  final _phoneTextController = TextEditingController();
  final _codeTextController = TextEditingController();
  final _phoneMaskFormatter = MaskTextInputFormatter(mask: _PHONE_MASK);

  ///вынести в di
  final _urlLauncherService = UrlLauncherService();

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
        listener: (context, state) async {
          if (state.status == Status.VALIDATION_CODE_SUCCESS) {
            _navigateToPageCreateAuthCode(context);
          }

          if (state.status == Status.ERROR_WHEN_SENDING_SMS ||
              state.status == Status.ERROR_WHEN_VALIDATION_CODE ||
              state.status == Status.ERROR_WHEN_RESENDING_SMS) {
            _showAlertDialogError(context, state);
          }
        },
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: Scaffold(
              backgroundColor: ResColors.WHITE,
              appBar: AppBar(
                centerTitle: true,
                automaticallyImplyLeading: false,
                elevation: 0,
                backgroundColor: ResColors.WHITE,
                actions: [
                  IconButton(
                      onPressed: () {
                        _urlLauncherService.call(ResSupport.app.phone);
                      },
                      icon: SvgPicture.asset(ResAuth.ICON_SUPPORT))
                ],
                title: SizedBox(
                  width: 133,
                  height: 16,
                  child: SvgPicture.asset(ResImages.LOGO_MEDTOCHKA),
                ),
              ),
              body: SafeArea(
                child: Column(
                  children: [
                    _PhoneInput(_phoneTextController, _phoneMaskFormatter),
                    _ConfirmCodeInput(_codeTextController),
                    _IndicatorValidationCode(),
                    _IndicatorResendSms(),
                    _CountDownTimer(),
                    _ResendSmsBtn(_phoneTextController, _codeTextController),
                    _ChangePhoneBtn(_phoneTextController),
                    Expanded(
                      child: Align(
                          alignment: FractionalOffset.bottomCenter,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              _PersonalAndPolicy(
                                urlLauncherService: _urlLauncherService,
                              ),
                              _SendSmsBtn(_phoneTextController)
                            ],
                          )),
                    )
                  ],
                ),
              )),
        ));
  }

  Future<void> _navigateToPageCreateAuthCode(BuildContext context) async {
    AutoRouter.of(context).replace(const CreateAuthCodeRoute());
  }

  Future<void> _showAlertDialogError(
    BuildContext context,
    AuthState state,
  ) async {
    ScaffoldMessenger.of(context)
        .showSnackBar(createSnackBar(context, ResErrors.NO_INTERNET_CONNECTION));
    return showDialog<void>(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Пропал интернет'),
            content: const Text('Подождите, пока появится сеть. Или попробуйте ещё раз.'),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('закрыть'.toUpperCase())),
              TextButton(
                  onPressed: () {
                    if (state.status == Status.ERROR_WHEN_SENDING_SMS) {
                      context.read<AuthBloc>().add(AuthSmsSent(_phoneTextController.text));
                    } else if (state.status == Status.ERROR_WHEN_VALIDATION_CODE) {
                      context.read<AuthBloc>().add(AuthCodeEntered(_codeTextController.text));
                    } else if (state.status == Status.ERROR_WHEN_RESENDING_SMS) {
                      context.read<AuthBloc>().add(AuthSmsResent(_phoneTextController.text));
                    }
                    Navigator.pop(context);
                  },
                  child: Text('повторить'.toUpperCase()))
            ],
          );
        });
  }
}

class _PhoneInput extends StatelessWidget {
  final TextEditingController _controller;
  final MaskTextInputFormatter _formatter;

  const _PhoneInput(this._controller, this._formatter);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Container(
        margin: const EdgeInsets.only(left: 8, top: 12, right: 8),
        child: TextField(
          enabled: state.isEnabledInputPhoneField,
          controller: _controller,
          inputFormatters: [_formatter],
          keyboardType: TextInputType.phone,
          onChanged: (text) {
            context.read<AuthBloc>().add(AuthPhoneEntered(text));
          },
          decoration:
              const InputDecoration(labelText: ResAuth.HINT_PHONE, border: OutlineInputBorder()),
        ),
      );
    });
  }
}

class _ConfirmCodeInput extends StatelessWidget {
  final TextEditingController _controller;

  const _ConfirmCodeInput(this._controller);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isVisibleInputCodeField,
          child: Container(
            margin: const EdgeInsets.only(left: 8, top: 32, right: 8),
            child: TextField(
              controller: _controller,
              enabled: state.isEnabledInputCodeField,
              keyboardType: TextInputType.number,
              onChanged: (text) {
                context.read<AuthBloc>().add(AuthCodeEntered(text));
              },
              decoration: InputDecoration(
                  labelText: ResAuth.HINT_CODE,
                  errorText: state.code.error,
                  border: const OutlineInputBorder()),
            ),
          ));
    });
  }
}

class _CountDownTimer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isVisibleCountDownTimer,
          child: StreamBuilder<int>(
            initialData: context.read<AuthBloc>().initCounter,
            stream: context.read<AuthBloc>().countDownTimerStream,
            builder: (context, snapshot) {
              if (snapshot.data == 0) {
                context.read<AuthBloc>().add(AuthTimerFinished());
              }
              return Container(
                margin: const EdgeInsets.only(left: 8, top: 36, right: 8),
                child: Text(
                  '${ResAuth.TEXT_COUNT_DOWN_TIMER} ${snapshot.data}',
                  style: const TextStyle(fontSize: 14, color: ResColors.BLACK),
                ),
              );
            },
          ));
    });
  }
}

class _ResendSmsBtn extends StatelessWidget {
  final TextEditingController _phoneController;
  final TextEditingController _codeController;

  const _ResendSmsBtn(this._phoneController, this._codeController);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isVisibleBtnResendSms,
          child: Container(
            margin: const EdgeInsets.only(top: 36),
            child: TextButton(
              onPressed: state.isEnabledBtnResendSms
                  ? () {
                      context.read<AuthBloc>().add(AuthSmsResent(_phoneController.text));
                      _codeController.clear();
                    }
                  : null,
              child: Text(
                ResAuth.RESEND_SMS,
                style: TextStyle(
                    fontSize: 14,
                    color:
                        state.isEnabledBtnResendSms ? ResColors.PRIMARY_BLUE : ResColors.DEEP_GREY),
              ),
            ),
          ));
    });
  }
}

class _ChangePhoneBtn extends StatelessWidget {
  final TextEditingController _controller;

  const _ChangePhoneBtn(this._controller);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isVisibleBtnChangePhone,
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            child: TextButton(
              onPressed: state.isEnabledBtnChangePhone
                  ? () {
                      _controller.clear();
                      context.read<AuthBloc>().add(AuthPhoneChanged());
                    }
                  : null,
              child: Text(
                ResAuth.CHANGE_PHONE,
                style: TextStyle(
                    fontSize: 14,
                    color: state.isEnabledBtnChangePhone
                        ? ResColors.PRIMARY_BLUE
                        : ResColors.DEEP_GREY),
              ),
            ),
          ));
    });
  }
}

class _PersonalAndPolicy extends StatelessWidget {
  final UrlLauncherService urlLauncherService;

  const _PersonalAndPolicy({Key? key, required this.urlLauncherService}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: true,
          child: Container(
            margin: const EdgeInsets.only(left: 8, top: 8, right: 8, bottom: 24),
            child: Text.rich(
              TextSpan(
                children: [
                  const TextSpan(
                    text: ResAuth.TEXT_PRIVACY_POLICY,
                  ),
                  TextSpan(
                      text: '\nобработку персональных данных',
                      style: const TextStyle(
                          color: ResColors.PRIMARY_BLUE, decoration: TextDecoration.underline),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          urlLauncherService
                              .openWeb(DocumentLink.privacyPolicy.url(ResBaseUrl.LANDING));
                        }),
                  const TextSpan(
                    text: ' и ',
                  ),
                  TextSpan(
                    text: '\nпринимаете пользовательское соглашение',
                    style: const TextStyle(
                        color: ResColors.PRIMARY_BLUE, decoration: TextDecoration.underline),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        urlLauncherService.openWeb(DocumentLink.termsOfUse.url(ResBaseUrl.LANDING));
                      },
                  )
                ],
              ),
              textAlign: TextAlign.center,
              style: ResTextTheme.body2,
            ),
          ));
    });
  }
}

class _SendSmsBtn extends StatelessWidget {
  final TextEditingController _controller;

  const _SendSmsBtn(this._controller);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isVisibleBtnSendSms,
          child: Container(
            margin: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
            height: 40,
            child: ElevatedButton(
              onPressed: state.isEnabledBtnSendSms
                  ? () {
                      if (state.status != Status.SENDING_SMS) {
                        context.read<AuthBloc>().add(AuthSmsSent(_controller.text));
                      }
                    }
                  : null,
              style: ElevatedButton.styleFrom(primary: ResColors.PRIMARY_BLUE),
              child: state.isSendingSms
                  ? const _CircleProgressIndicator(ResColors.WHITE, 2)
                  : Text(ResAuth.SEND_SMS.toUpperCase()),
            ),
          ));
    });
  }
}

class _IndicatorResendSms extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isResendingSms,
          child: Container(
            margin: const EdgeInsets.only(top: 36),
            child: const _CircleProgressIndicator(ResColors.PRIMARY_BLUE, 2.5),
          ));
    });
  }
}

class _CircleProgressIndicator extends StatelessWidget {
  final Color _color;
  final double _strokeWidth;

  const _CircleProgressIndicator(this._color, this._strokeWidth);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 20,
      height: 20,
      child: CircularProgressIndicator(
        strokeWidth: _strokeWidth,
        valueColor: AlwaysStoppedAnimation<Color>(_color),
      ),
    );
  }
}

class _IndicatorValidationCode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
      return Visibility(
          visible: state.isValidationCode,
          child: Container(
            margin: const EdgeInsets.only(left: 8, right: 8),
            child: const LinearProgressIndicator(
              minHeight: 2,
            ),
          ));
    });
  }
}
