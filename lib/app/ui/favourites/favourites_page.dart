import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/favourites/favourites_bloc.dart';
import 'package:medtochka/app/bloc/favourites/model/favourite.dart';
import 'package:medtochka/app/core/utils/create_sanck_bar.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/common.dart';
import 'package:medtochka/app/resources/favourites/favourites.dart';
import 'package:medtochka/app/resources/info_message.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/chip_list_filter.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';
import 'package:medtochka/app/ui/base/custom_list_tile.dart';
import 'package:medtochka/app/ui/base/empty_message_widget.dart';

class FavouritesPage extends StatelessWidget {
  final int _isLoadedIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ResColors.LIGHTEST_BLUE,
      body: SafeArea(
        child: BlocConsumer<FavouritesBloc, FavouritesState>(
          listener: (context, state) {
            if (state.snackBarText != null) {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              context.read<FavouritesBloc>().add(FavouritesClearSnackBarText());

              ScaffoldMessenger.of(context)
                  .showSnackBar(createSnackBar(context, state.snackBarText!));

              return;
            }

            if (state.removedFavourite != null) {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();

              final Favourite favourite = state.removedFavourite!;

              ScaffoldMessenger.of(context).showSnackBar(
                _createRemoveSnackBar(context, favourite, state.selectedType),
              );

              return;
            }
          },
          builder: (context, state) {
            return CustomScrollView(slivers: [
              const SliverAppBar(
                floating: true,
                title: Text(
                  ResFavourites.APP_BAR_TITLE,
                  style: ResTextTheme.headline6,
                ),
                elevation: 0,
                backgroundColor: ResColors.WHITE,
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 48,
                  color: ResColors.WHITE,
                  margin: const EdgeInsets.only(bottom: 24),
                  child: ChipListFilter(
                    onTap: (type) => _fetchData(context: context, type: type),
                    chips: state.chipOptions,
                    selectedChip: state.selectedType,
                  ),
                ),
              ),
              if (_isLoading(state))
                const SliverFillRemaining(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              if (_isError(state))
                SliverToBoxAdapter(
                  child: EmptyMessageWidget(
                    infoMessage: ResInfoMessage.errorInternal,
                    onTap: () => _fetchData(context: context),
                  ),
                ),
              if (_isLoadedSuccess(state))
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                      if (state.favourites.isEmpty) {
                        return EmptyMessageWidget(
                          infoMessage: state.selectedType.emptyMessage,
                        );
                      }

                      return _FavouriteWidget(
                        favourite: state.favourites.elementAt(index),
                      );
                    },
                    childCount: getCorrectItemsCount(state),
                  ),
                ),
            ]);
          },
        ),
      ),
    );
  }

  void _fetchData({required BuildContext context, ResChipType? type}) {
    context.read<FavouritesBloc>().add(FavouritesFetch(type: type));
  }

  SnackBar _createRemoveSnackBar(BuildContext context, Favourite favourite, ResChipType type) {
    return createSnackBar(context, ResFavourites.SNACK_BAR_TEXT_REMOVE,
        snackBarAction: SnackBarAction(
          label: ResCommon.TEXT_CANCEL.toUpperCase(),
          textColor: ResColors.PRIMARY_BLUE,
          onPressed: () {
            context.read<FavouritesBloc>().add(FavouritesAdd(favourite, type));
          },
        ));
  }

  bool _isLoadedSuccess(FavouritesState state) {
    return (state.status == listStatus.SUCCESS);
  }

  bool _isLoading(FavouritesState state) {
    return (state.status == listStatus.LOADING);
  }

  bool _isError(FavouritesState state) {
    return (state.status == listStatus.ERROR);
  }

  int getCorrectItemsCount(FavouritesState state) {
    if (_isLoadedSuccess(state) && state.favourites.isNotEmpty) {
      return state.favourites.length;
    }

    return _isLoadedIndex;
  }
}

class _FavouriteWidget extends StatelessWidget {
  final Favourite favourite;

  const _FavouriteWidget({Key? key, required this.favourite}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomListTile(
      leading: CustomAvatar(
        url: favourite.avatar,
        type: favourite.type.key,
      ),
      title: favourite.name,
      subtitleText: favourite.info,
      subtitleTextStyle: const TextStyle(
        overflow: TextOverflow.ellipsis,
      ),
      trailing: const Icon(
        UiIcon.heart_active,
        color: ResColors.PRIMARY_RED,
      ),
      onTapTrailing: () => _removeFavourite(context, favourite),
    );
  }

  void _removeFavourite(BuildContext context, Favourite favourite) {
    context.read<FavouritesBloc>().add(FavouritesRemove(favourite));
  }
}
