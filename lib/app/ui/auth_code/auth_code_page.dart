import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/auth_code/auth_code_bloc.dart';
import 'package:medtochka/app/bloc/code_entry/code_entry_bloc.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/resources/auth_code/auth_code.dart';
import 'package:medtochka/app/resources/auth_code/auth_code_alerts.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/support.dart';
import 'package:medtochka/app/ui/base/code_entry_indicator.dart';
import 'package:medtochka/app/ui/base/code_entry_keyboard.dart';

class AuthCodePage extends StatelessWidget {
  const AuthCodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCodeBloc, AuthCodeState>(
      listener: (context, state) {
        if (state.status == AuthCodeStatus.ERROR_LIMIT) {
          final ResAuthCodeAlerts dialogData = ResAuthCodeAlerts.errorLimit;

          showDialog<String>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AlertDialog(
              title: Text(dialogData.title),
              content: Text(dialogData.description),
              actions: <Widget>[
                TextButton(
                  onPressed: () => {UrlLauncherService().call(ResSupport.app.phone)},
                  child: Text(
                    dialogData.cancelText.toUpperCase(),
                  ),
                ),
                TextButton(
                  onPressed: () => _onTapShowExitDialog(context),
                  child: Text(
                    dialogData.confirmText.toUpperCase(),
                  ),
                ),
              ],
            ),
          );
        }

        if (state.status == AuthCodeStatus.EXIT_MESSAGE) {
          final ResAuthCodeAlerts dialogData = ResAuthCodeAlerts.exit;

          showDialog<String>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AlertDialog(
              title: Text(dialogData.title),
              content: Text(dialogData.description),
              actions: <Widget>[
                TextButton(
                  onPressed: () => _onTapCloseExitDialog(context),
                  child: Text(
                    dialogData.cancelText.toUpperCase(),
                    style: const TextStyle(color: ResColors.BLACK),
                  ),
                ),
                TextButton(
                  onPressed: () => _onTapLogoutUser(context),
                  child: Text(
                    dialogData.confirmText.toUpperCase(),
                  ),
                ),
              ],
            ),
          );
        }

        if (state.status == AuthCodeStatus.CODE_CONFIRMED) {
          _openHomePage(context);
        }

        if (state.status == AuthCodeStatus.AUTH_PAGE) {
          _openAuthPage(context);
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: SafeArea(
            child: Center(
              child: Column(
                children: [
                  Expanded(
                      flex: 2,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                          Container(
                              margin: const EdgeInsets.only(bottom: 40),
                              child: const Text(ResAuthCode.CODE_TITLE)),
                          Container(
                            height: 40,
                            alignment: Alignment.center,
                            child: _isLoading(state)
                                ? const SizedBox(
                                    height: 20,
                                    width: 20,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                    ),
                                  )
                                : BlocBuilder<ConfirmCodeEntryBloc, CodeEntryState>(
                                    builder: (context, state) {
                                      return CodeEntryIndicator(
                                        code: state.code,
                                        codeLength: state.codeLength,
                                        errorMessage: state.errorMessage,
                                        infoMessage: state.infoMessage,
                                      );
                                    },
                                  ),
                          )
                        ]),
                      )),
                  Expanded(
                      flex: 6,
                      child: Center(
                        child: CodeEntryKeyboard(
                          onTapNumber: (String value) {
                            _onTapNumber(context, value);
                          },
                          onTapRemove: () {
                            _onTapRemove(context);
                          },
                          onTapExit: () {
                            _onTapShowExitDialog(context);
                          },
                        ),
                      )),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  bool _isLoading(AuthCodeState state) {
    return (state.status == AuthCodeStatus.SEND_CODE ||
        state.status == AuthCodeStatus.CODE_CONFIRMED);
  }

  void _onTapNumber(BuildContext context, String value) {
    context.read<ConfirmCodeEntryBloc>().add(CodeEntryTapNumber(value));
  }

  void _onTapRemove(BuildContext context) {
    context.read<ConfirmCodeEntryBloc>().add(CodeEntryRemoveNumber());
  }

  void _onTapShowExitDialog(BuildContext context) {
    context.read<AuthCodeBloc>().add(AuthCodeShowExitDialog());
  }

  void _onTapCloseExitDialog(BuildContext context) {
    context.read<AuthCodeBloc>().add(AuthCodeCloseDialog());

    Navigator.pop(context);
  }

  void _onTapLogoutUser(BuildContext context) {
    Navigator.pop(context);

    context.read<AuthCodeBloc>().add(AuthCodeLogout());
  }

  void _openHomePage(BuildContext context) async {
    AutoRouter.of(context).replace(const MainRoute());
  }

  void _openAuthPage(BuildContext context) async {
    AutoRouter.of(context).replace(const AuthRoute());
  }
}
