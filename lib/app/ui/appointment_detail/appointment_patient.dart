part of 'appointment_detail_page.dart';

class _AppointmentPatient extends StatelessWidget {
  const _AppointmentPatient({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: false,
      child: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Divider(
                thickness: 1,
              ),
            ),
            ItemWithName(
              title: 'пациент',
              content: Text(
                'Марина Афанасьева',
                style: ResTextTheme.body1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
