part of 'appointment_detail_page.dart';

class _AppointmentHeader extends StatelessWidget {
  final String? avatar;
  final String name;
  final String? speciality;
  final int? price;
  const _AppointmentHeader({
    Key? key,
    required this.avatar,
    required this.name,
    this.price,
    this.speciality,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, bottom: 16, top: 24),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 108,
            width: 80,
            margin: const EdgeInsets.only(right: 16),
            decoration: BoxDecoration(
                border: Border.all(color: ResColors.MEDIUM_GREY),
                borderRadius: BorderRadius.circular(8)),
            child: CustomAvatar(
              url: avatar,
              type: ReviewTypeKey.DOCTOR,
              width: 80,
              height: 108,
              radius: 0,
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 4),
                  child: Text(name, style: ResTextTheme.body1),
                ),
                if (speciality != null)
                  Text(
                    speciality!,
                    style: ResTextTheme.body2.merge(const TextStyle(color: ResColors.DEEP_GREY)),
                  ),
                if (price != null)
                  Text('Стоимость от ${formatNumber(price!)}₽',
                      style: ResTextTheme.body2.merge(const TextStyle(color: ResColors.DEEP_GREY))),
              ],
            ),
          )
        ],
      ),
    );
  }
}
