part of 'appointment_detail_page.dart';

class _AppointmentItemList extends StatelessWidget {
  final IconData icon;
  final String title;
  final VoidCallback onTap;

  const _AppointmentItemList(
      {Key? key, required this.icon, required this.title, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ListTile(
        leading: Icon(
          icon,
          color: ResColors.DEEP_GREY,
        ),
        title: Text(title,
            overflow: TextOverflow.ellipsis, style: ResTextTheme.body1),
        trailing: Container(
          width: 40,
          height: 40,
          color: Colors.transparent,
          child: const Icon(UiIcon.arrow_right),
        ),
        contentPadding: EdgeInsets.zero,
        onTap: onTap,
      ),
    );
  }
}
