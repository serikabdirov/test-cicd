part of 'appointment_detail_page.dart';

class _AppointmentBtn extends StatelessWidget {
  final ResButtonStyle style;
  final String text;
  final VoidCallback onTap;
  final IconData? icon;

  const _AppointmentBtn(
      {Key? key,
      required this.style,
      required this.text,
      required this.onTap,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: ElevatedButton(
        style:
            ElevatedButton.styleFrom(elevation: 0, primary: style.background),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (icon != null)
              Padding(
                padding: const EdgeInsets.only(right: 8.0),
                child: Icon(
                  icon,
                  color: style.text,
                ),
              ),
            Text(text.toUpperCase(), style: TextStyle(color: style.text))
          ],
        ),
        onPressed: onTap,
      ),
    );
  }
}
