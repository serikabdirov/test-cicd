part of 'appointment_detail_page.dart';

class _AppointmentMetroItem extends StatelessWidget {
  final AppointmentDetailMetro metro;

  const _AppointmentMetroItem({Key? key, required this.metro})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          decoration:
              BoxDecoration(color: Color(metro.color), shape: BoxShape.circle),
          height: 8,
          width: 8,
          margin: const EdgeInsets.only(right: 8),
        ),
        Text('${metro.name} (${getMetroDist(metro.dist)})'),
      ],
    );
  }
}
