part of 'appointment_detail_page.dart';

class ItemWithName extends StatelessWidget {
  final Widget content;
  final String title;

  const ItemWithName({Key? key, required this.content, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: ResTextTheme.caption
              .merge(const TextStyle(color: ResColors.DEEP_GREY)),
        ),
        content
      ],
    );
  }
}
