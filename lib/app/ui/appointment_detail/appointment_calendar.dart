part of 'appointment_detail_page.dart';

// ignore: must_be_immutable
class AppointmentCalendar extends StatelessWidget {
  final String time;
  final String date;
  final bool isHideCalendar;
  late String? dayDifference;

  AppointmentCalendar(
      {Key? key,
      required this.time,
      required this.date,
      required this.isHideCalendar})
      : super(key: key) {
    dayDifference = getDifferentTimeText(DateTime.parse('$date $time'));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, bottom: 16),
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: isHideCalendar ? ResColors.LIGHT_GREY : null,
            border: !isHideCalendar
                ? Border.all(color: ResColors.MEDIUM_GREY)
                : null),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 4.0),
              child: Text(
                _getFormattedDate(date, time),
                style: !isHideCalendar
                    ? ResTextTheme.headline6
                    : ResTextTheme.headline6
                        .merge(const TextStyle(color: ResColors.DEEP_GREY)),
              ),
            ),
            if (isHideCalendar && dayDifference != null)
              Text(
                dayDifference!,
                style: ResTextTheme.body1
                    .merge(const TextStyle(color: ResColors.DEEP_GREY)),
              )
          ],
        ),
      ),
    );
  }

  String _getFormattedDate(String date, String time) {
    DateTime dateTime = DateTime.parse('$date $time');

    return DateFormat('d MMMM, y, E, HH:mm').format(dateTime);
  }
}
