import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:medtochka/app/bloc/appointment_detail/appointment_detail_bloc.dart';
import 'package:medtochka/app/bloc/appointments/appointments_bloc.dart';
import 'package:medtochka/app/core/utils/convert_map_name.dart';
import 'package:medtochka/app/core/utils/create_sanck_bar.dart';
import 'package:medtochka/app/core/utils/format_number.dart';
import 'package:medtochka/app/core/utils/format_phone_number.dart';
import 'package:medtochka/app/core/utils/get_different_time_text.dart';
import 'package:medtochka/app/core/utils/get_doctor_link.dart';
import 'package:medtochka/app/core/utils/get_metro_dist.dart';
import 'package:medtochka/app/core/utils/get_rate_link.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail.dart';
import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail_metro.dart';
import 'package:medtochka/app/resources/appointment_detail/appointment_status.dart';
import 'package:medtochka/app/resources/button_style.dart';
import 'package:medtochka/app/resources/colors.dart';
import 'package:medtochka/app/resources/info_message.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';
import 'package:medtochka/app/resources/text_theme.dart';
import 'package:medtochka/app/resources/ui_icon.dart';
import 'package:medtochka/app/ui/base/alert.dart';
import 'package:medtochka/app/ui/base/custom_avatar.dart';
import 'package:medtochka/app/ui/base/info_widget.dart';

part 'appointment_address.dart';
part 'appointment_btn.dart';
part 'appointment_calendar.dart';
part 'appointment_cancel_modal.dart';
part 'appointment_header.dart';
part 'appointment_info.dart';
part 'appointment_item_list.dart';
part 'appointment_item_with_category.dart';
part 'appointment_lpu.dart';
part 'appointment_metro_item.dart';
part 'appointment_patient.dart';

//TODO: вынести текст в ресурсы

class AppointmentDetailPage extends StatefulWidget {
  final int id;

  const AppointmentDetailPage({Key? key, @PathParam('id') required this.id}) : super(key: key);

  @override
  _AppointmentDetailPageState createState() => _AppointmentDetailPageState();
}

class _AppointmentDetailPageState extends State<AppointmentDetailPage> {
  final _callService = UrlLauncherService();

  @override
  void initState() {
    context.read<AppointmentDetailBloc>().add(AppointmentDetailFetch(widget.id));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ResColors.WHITE,
      body: SafeArea(
        child: BlocBuilder<AppointmentDetailBloc, AppointmentDetailState>(
          builder: (context, state) {
            if (state.status == AppointmentDetailStatus.LOADING) {
              return const Center(child: CircularProgressIndicator());
            }

            if (state.status == AppointmentDetailStatus.SUCCESS) {
              if (state.appointmentDetail != null) {
                var detail = state.appointmentDetail!;
                var latitude = detail.lpuLatitude;
                var longitude = detail.lpuLongitude;

                return CustomScrollView(
                  slivers: [
                    SliverAppBar(
                      floating: true,
                      title: Text(
                        getCurrentTitle(detail.status),
                        style: ResTextTheme.headline6,
                      ),
                      leading: IconButton(
                        icon: const Icon(
                          UiIcon.arrow_back,
                          color: ResColors.BLACK,
                        ),
                        onPressed: () {
                          AutoRouter.of(context).pop();
                        },
                      ),
                      elevation: 0,
                      backgroundColor: ResColors.WHITE,
                    ),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: _isAbsenceStatus(detail.status),
                      child: const Alert(
                        text: 'Вы не явились на приём',
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: _AppointmentHeader(
                      avatar: detail.avatarPath,
                      name: detail.doctorName,
                      price: detail.price,
                      speciality: detail.specialityName,
                    )),
                    SliverToBoxAdapter(
                        child: AppointmentCalendar(
                      date: detail.dtVisit,
                      time: detail.timeStart,
                      isHideCalendar: !_isUpcomingStatus(detail.status),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: _isUpcomingStatus(detail.status) && detail.lpuPhone != null,
                      child: _AppointmentBtn(
                        text: 'Позвонить',
                        style: ResButtonStyle.primary,
                        icon: UiIcon.phone_call,
                        onTap: () => _callLpu(detail.lpuPhone!),
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible:
                          !_isUpcomingStatus(detail.status) && detail.doctorUrlProdoctorov != null,
                      child: _AppointmentBtn(
                        text: 'Записаться повторно',
                        style: ResButtonStyle.primary,
                        icon: UiIcon.calendar_date_and_clock,
                        onTap: () => _openDoctorPage(
                            doctorLink: detail.doctorUrlProdoctorov!,
                            lpuId: detail.lpuIdProdoctorov),
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible:
                          _isUpcomingStatus(detail.status) && _isHasCoords(latitude, longitude),
                      child: _AppointmentBtn(
                        text: 'Открыть на карте',
                        style: ResButtonStyle.secondary,
                        icon: UiIcon.place,
                        onTap: () =>
                            _openMapsSheet(context, latitude!, longitude!, detail.lpuFullAddress),
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: _isCompletedStatus(detail.status) &&
                          detail.doctorIdProdoctorov != null &&
                          !_isHasRate(detail.rateId, detail.doctorIdProdoctorov),
                      child: _AppointmentBtn(
                        text: 'Оставить отзыв',
                        style: ResButtonStyle.secondary,
                        icon: UiIcon.place,
                        onTap: () => _openDoctorRate(
                          doctorId: detail.doctorIdProdoctorov!,
                          lpuId: detail.lpuIdProdoctorov,
                          dateVisit: detail.dtVisit,
                        ),
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: _AppointmentLpu(
                      lpuName: detail.lpuName,
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      child: _AppointmentAddress(
                        address: detail.lpuFullAddress,
                        metroItems: detail.metroNearby,
                      ),
                    )),
                    const SliverToBoxAdapter(
                      child: Visibility(child: _AppointmentPatient()),
                    ),
                    SliverToBoxAdapter(
                      child: Visibility(
                        visible: _isHasListItem(detail),
                        child: const Divider(
                          thickness: 1,
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: _isUpcomingStatus(detail.status) && detail.cancelable,
                      child: _AppointmentItemList(
                        title: 'Отменить запись',
                        icon: UiIcon.circle_account,
                        onTap: () {
                          _openCancelDialog(context, detail.id);
                        },
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: !_isUpcomingStatus(detail.status) &&
                          _isHasRate(detail.rateId, detail.doctorIdProdoctorov),
                      child: _AppointmentItemList(
                        title: 'Ваш отзыв',
                        icon: UiIcon.circle_account,
                        onTap: () {},
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible: !_isUpcomingStatus(detail.status) && detail.lpuPhone != null,
                      child: _AppointmentItemList(
                        title: 'Позвонить',
                        icon: UiIcon.phone_call,
                        onTap: () => _callLpu(detail.lpuPhone!),
                      ),
                    )),
                    SliverToBoxAdapter(
                        child: Visibility(
                      visible:
                          !_isUpcomingStatus(detail.status) && _isHasCoords(latitude, longitude),
                      child: _AppointmentItemList(
                        title: 'Открыть на карте',
                        icon: UiIcon.place,
                        onTap: () {
                          _openMapsSheet(context, latitude!, longitude!, detail.lpuFullAddress);
                        },
                      ),
                    )),
                    SliverToBoxAdapter(
                      child: Visibility(
                        child: _AppointmentInfo(
                          detail: detail.recommendations,
                        ),
                      ),
                    ),
                  ],
                );
              }
            }

            return Container();
          },
        ),
      ),
    );
  }

  bool _isUpcomingStatus(String status) {
    return status == AppointmentStatus.UPCOMING;
  }

  bool _isCompletedStatus(String status) {
    return status == AppointmentStatus.COMPLETED;
  }

  bool _isCancelledStatus(String status) {
    return status == AppointmentStatus.CANCELLED;
  }

  bool _isAbsenceStatus(String status) {
    return status == AppointmentStatus.ABSENCE;
  }

  bool _isHasCoords(double? lat, double? lon) {
    return lat != null || lon != null;
  }

  bool _isHasRate(int? rateId, int? pdId) {
    return rateId != null && pdId != null;
  }

  bool _isHasListItem(AppointmentDetailResponse detail) {
    return !_isUpcomingStatus(detail.status) ||
        (_isHasCoords(detail.lpuLatitude, detail.lpuLongitude) &&
            !_isUpcomingStatus(detail.status)) ||
        ((_isHasRate(detail.rateId, detail.doctorIdProdoctorov) || detail.cancelable) &&
            _isUpcomingStatus(detail.status));
  }

  String getCurrentTitle(String status) {
    if (_isUpcomingStatus(status)) {
      return 'Запись на приём';
    }

    if (_isCancelledStatus(status)) {
      return 'Отменённая запись';
    }

    return 'Прошедшая запись';
  }

  void _callLpu(String phone) {
    _callService.call(formatPhoneNumber(phone));
  }

  _openCancelDialog(context, id) async {
    showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('Отменить запись?'),
        content: const Text('Вы действительно хотите отменить запись?'),
        actions: <Widget>[
          TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'не отменять'.toUpperCase(),
              style: const TextStyle(color: ResColors.BLACK),
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pop(context);

              context.read<AppointmentDetailBloc>().add(AppointmentDetailCancelAppointment(id));

              AutoRouter.of(context).pushWidget(const AppointmentCancelModal());
            },
            child: Text(
              'отменить'.toUpperCase(),
            ),
          ),
        ],
      ),
    );
  }

  _openMapsSheet(context, double lat, double lon, String title) async {
    final coords = Coords(lat, lon);

    try {
      final availableMaps = await MapLauncher.installedMaps;

      if (availableMaps.isEmpty) {
        _openYandexLink(coords);
      }

      if (availableMaps.length == 1) {
        _openMap(availableMaps.first, coords, title);

        return;
      }

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                color: ResColors.WHITE,
                child: Wrap(
                    children: availableMaps
                        .map<Widget>((map) => ListTile(
                            title: Text(convertMapName(map)),
                            leading: SvgPicture.asset(
                              map.icon,
                              height: 24.0,
                              width: 24.0,
                            ),
                            onTap: () {
                              _openMap(map, coords, title);
                            }))
                        .toList()),
              ),
            ),
          );
        },
      );
    } catch (e) {
      _openYandexLink(coords);
    }
  }

  _openMap(AvailableMap map, Coords coords, String title) {
    if (map.mapType == MapType.yandexMaps) {
      map.showMarker(coords: coords, title: title);

      return;
    }

    map.showDirections(destination: coords);
  }

  _openYandexLink(Coords coords) {
    UrlLauncherService urlLauncherService = UrlLauncherService();

    urlLauncherService.openWeb(
        'https://maps.yandex.ru/?whatshere[point]=${coords.longitude},${coords.latitude}&z=16&l=map');
  }

  _openDoctorPage({required String doctorLink, int? lpuId}) {
    UrlLauncherService urlLauncherService = UrlLauncherService();

    urlLauncherService.openWeb(getDoctorLink(doctorLink: doctorLink, lpuId: lpuId));
  }

  _openDoctorRate({required int doctorId, required String dateVisit, int? lpuId}) {
    UrlLauncherService urlLauncherService = UrlLauncherService();

    urlLauncherService.openWeb(getRateLink(
        type: ReviewTypeKey.DOCTOR, doctorId: doctorId, lpuId: lpuId, dateVisit: dateVisit));
  }
}
