part of 'appointment_detail_page.dart';

class AppointmentCancelModal extends StatelessWidget {
  const AppointmentCancelModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child:
            BlocConsumer<AppointmentDetailBloc, AppointmentDetailState>(listener: (context, state) {
          if (state.cancelStatus == AppointmentCancelStatus.SUCCESS) {
            context.read<AppointmentsBloc>().add(AppointmentsFetch());
          }
        }, builder: (context, state) {
          switch (state.cancelStatus) {
            case AppointmentCancelStatus.LOADING:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case AppointmentCancelStatus.SUCCESS:
              return _InfoState(
                info: ResInfoMessage.appointmentCancelSuccess,
              );
            case AppointmentCancelStatus.ERROR:
              return _InfoState(
                info: ResInfoMessage.appointmentCancelError,
              );
          }
        }),
      ),
    );
  }
}

class _InfoState extends StatelessWidget {
  final ResInfoMessage info;
  final String? phone;

  const _InfoState({Key? key, required this.info, this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Align(
        alignment: Alignment.topLeft,
        child: IconButton(
          icon: const Icon(UiIcon.close_not_a_circle),
          onPressed: () {
            AutoRouter.of(context).pop();
          },
        ),
      ),
      Expanded(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: InfoWidget(
              title: info.title,
              imageSource: info.avatarSrc,
              bodyText: info.subtitle,
            ),
          ),
        ),
      ),
      _AppointmentBtn(
        style: ResButtonStyle.primary,
        text: info.buttonText!,
        onTap: () {
          AutoRouter.of(context).pop();
        },
      )
    ]);
  }
}
