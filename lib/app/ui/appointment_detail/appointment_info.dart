part of 'appointment_detail_page.dart';

class _AppointmentInfo extends StatelessWidget {
  final String? detail;

  const _AppointmentInfo({Key? key, required this.detail}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: detail != null && detail!.isNotEmpty,
      child: Padding(
        padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Divider(
                thickness: 1,
              ),
            ),
            ItemWithName(
              title: 'Информация о приёме',
              content: Text(
                '$detail',
                style: ResTextTheme.body1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
