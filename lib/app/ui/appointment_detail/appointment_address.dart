part of 'appointment_detail_page.dart';

class _AppointmentAddress extends StatelessWidget {
  final String address;
  final Iterable<AppointmentDetailMetro> metroItems;

  const _AppointmentAddress(
      {Key? key, required this.address, required this.metroItems})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: Divider(
              thickness: 1,
            ),
          ),
          ItemWithName(
            title: 'Адрес',
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 4),
                        child: Text(
                          address,
                          style: ResTextTheme.body1,
                        ),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(UiIcon.copy),
                      splashRadius: 25,
                      onPressed: () {
                        ScaffoldMessenger.of(context).hideCurrentSnackBar();

                        Clipboard.setData(ClipboardData(text: address));
                        ScaffoldMessenger.of(context).showSnackBar(
                            createSnackBar(context, 'Адрес скопирован'));
                      },
                    )
                  ],
                ),
                ...metroItems.map<Widget>((metro) => _AppointmentMetroItem(
                      metro: metro,
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
