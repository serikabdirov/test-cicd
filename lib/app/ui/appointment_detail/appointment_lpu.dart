part of 'appointment_detail_page.dart';

class _AppointmentLpu extends StatelessWidget {
  final String lpuName;

  const _AppointmentLpu({Key? key, required this.lpuName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 16),
      child: ItemWithName(
        title: 'Клиника',
        content: Text(
          lpuName,
          style: ResTextTheme.body1,
        ),
      ),
    );
  }
}
