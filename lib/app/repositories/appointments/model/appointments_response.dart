import 'package:medtochka/app/resources/base_url.dart';

class AppointmentsResponse {
  final int id;
  final String? avatarPath;
  final String currentStatus;
  final String doctorName;
  final String dtVisit;
  final String lpuName;
  final String specialityName;
  final String status;
  final String timeStart;

  AppointmentsResponse(
      {this.avatarPath,
      required this.currentStatus,
      required this.doctorName,
      required this.dtVisit,
      required this.id,
      required this.lpuName,
      required this.specialityName,
      required this.status,
      required this.timeStart});

  AppointmentsResponse.fromJson(Map<String, dynamic> json)
      : avatarPath = _formatAvatarPath(json['avatar_path']),
        currentStatus = json['current_status'],
        doctorName = json['doctor_name'],
        dtVisit = json['dt_visit'],
        id = json['id'],
        lpuName = json['lpu_name'],
        specialityName = json['speciality_name'],
        status = json['status'],
        timeStart = json['time_start'];

  Map<String, dynamic> toJson() => {
        'avatar_path': avatarPath,
        'current_status': currentStatus,
        'doctor_name': doctorName,
        'dt_visit': dtVisit,
        'id': id,
        'lpu_name': lpuName,
        'speciality_name': specialityName,
        'status': status,
        'time_start': timeStart,
      };

  static _formatAvatarPath(String? url) {
    return url != null ? '${ResBaseUrl.MAIN}/$url' : url;
  }
}
