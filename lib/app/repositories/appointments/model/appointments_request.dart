class AppointmentsRequest {
  String type;
  int? id;

  AppointmentsRequest(this.type, {this.id});

  Map<String, dynamic> toJson() => {'status': type};
}
