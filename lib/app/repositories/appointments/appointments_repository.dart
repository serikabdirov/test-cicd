import 'package:medtochka/app/repositories/appointments/model/appointments_response.dart';

abstract class AppointmentsRepository {
  Future<Iterable<AppointmentsResponse>> fetchAppointments(String type);
}
