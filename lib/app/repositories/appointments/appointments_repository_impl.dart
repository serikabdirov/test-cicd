import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/appointments/appointments_repository.dart';
import 'package:medtochka/app/repositories/appointments/model/appointments_request.dart';
import 'package:medtochka/app/repositories/appointments/model/appointments_response.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';

class AppointmentsRepositoryImpl implements AppointmentsRepository {
  static const String _PATH = 'api/appointments/';

  final Dio _infoNetworkClient;

  AppointmentsRepositoryImpl(this._infoNetworkClient);

  @override
  Future<Iterable<AppointmentsResponse>> fetchAppointments(String type) async {
    final appointmentsRequest = AppointmentsRequest(type);

    try {
      final response = await _infoNetworkClient.patch(_PATH,
          queryParameters: appointmentsRequest.toJson());

      return response.data.map<AppointmentsResponse>(
          (appointment) => AppointmentsResponse.fromJson(appointment));
    } catch (ex) {
      throw InternalError();
    }
  }
}
