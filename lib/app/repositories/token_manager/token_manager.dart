import 'package:dio/dio.dart';
import 'package:medtochka/app/data/tokens/model/token.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenManager {
  static const String _PATH_INFO_TOKEN =
      "api/tokens/medtochka_info/profile_token/";
  static const String _PATH_PROFILE_TOKEN =
      "api/tokens/medtochka_profile/profile_token/";
  // ignore: unused_field
  static const String _PATH_PD_TOKEN = "api/tokens/pd_access_token/";
  static const String _PATH_MAIN_REFRESH_TOKEN = "api/o/token/";
  static const String _PATH_MAIN_REVOKE_TOKEN = "/api/o/revoke_token/";

  static const String _MAIN_ACCESS_TOKEN = "mainAccessToken";
  static const String _MAIN_REFRESH_TOKEN = "mainRefreshToken";
  static const String _INFO_TOKEN = "infoToken";
  static const String _PROFILE_TOKEN = "profileToken";
  static const String _PD_TOKEN = "pdToken";
  static const String _PARAM_REFRESH_TOKEN = "refresh_token";
  static const String _PARAM_MOBILE_APP = "mobile_app";
  static const String _PARAM_GRANT_TYPE = "grant_type";
  static const String _PARAM_CLIENT_ID = "client_id";
  static const String _PARAM_TOKEN = "token";

  final Dio _mainClient;

  TokenManager(this._mainClient);

  Future<String?> getMainToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_MAIN_ACCESS_TOKEN);
  }

  Future<void> setMainToken(String accessToken, String refreshToken) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_MAIN_ACCESS_TOKEN, accessToken);
    prefs.setString(_MAIN_REFRESH_TOKEN, refreshToken);
  }

  Future<void> refreshMainToken() async {
    final prefs = await SharedPreferences.getInstance();
    final refreshToken = prefs.getString(_MAIN_REFRESH_TOKEN);
    const grantType = _PARAM_REFRESH_TOKEN;
    const clientId = _PARAM_MOBILE_APP;
    var formData = FormData.fromMap({
      _PARAM_GRANT_TYPE: grantType,
      _PARAM_REFRESH_TOKEN: refreshToken,
      _PARAM_CLIENT_ID: clientId
    });

    var response =
        await _mainClient.post(_PATH_MAIN_REFRESH_TOKEN, data: formData);
    if (response.statusCode == 200) {
      final token = Token.fromJson(response.data);
      prefs.setString(_MAIN_ACCESS_TOKEN, token.accessToken ?? "");
      prefs.setString(_MAIN_REFRESH_TOKEN, token.refreshToken ?? "");
    }
  }

  Future<String?> getInfoToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_INFO_TOKEN);
  }

  Future<void> refreshInfoToken() async {
    final prefs = await SharedPreferences.getInstance();

    var response = await _mainClient.post(_PATH_INFO_TOKEN);
    if (response.statusCode == 201) {
      final token = Token.fromJson(response.data);
      prefs.setString(_INFO_TOKEN, token.profileToken ?? "");
    }
  }

  Future<String?> getProfileToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_PROFILE_TOKEN);
  }

  Future<void> refreshProfileToken() async {
    final prefs = await SharedPreferences.getInstance();

    var response = await _mainClient.post(_PATH_PROFILE_TOKEN);
    if (response.statusCode == 201) {
      final token = Token.fromJson(response.data);
      prefs.setString(_PROFILE_TOKEN, token.profileToken ?? "");
    }
  }

  Future<String?> getPdToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_PD_TOKEN);
  }

  Future<void> refreshPdToken() async {
    /* */
  }

  Future<void> revokeMainToken() async {
    final prefs = await SharedPreferences.getInstance();
    final mainToken = prefs.getString(_MAIN_ACCESS_TOKEN);
    var formData = FormData.fromMap(
        {_PARAM_TOKEN: mainToken, _PARAM_CLIENT_ID: _PARAM_MOBILE_APP});

    var response =
        await _mainClient.post(_PATH_MAIN_REVOKE_TOKEN, data: formData);
    if (response.statusCode == 200) {
      prefs.setString(_MAIN_ACCESS_TOKEN, "");
      prefs.setString(_MAIN_REFRESH_TOKEN, "");
      return;
    }
  }
}
