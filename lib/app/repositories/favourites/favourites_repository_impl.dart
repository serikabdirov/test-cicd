import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/repositories/favourites/favourites_repository.dart';
import 'package:medtochka/app/repositories/favourites/model/favourite_request.dart';
import 'package:medtochka/app/repositories/favourites/model/favourite_response.dart';

class FavouritesRepositoryImpl implements FavouritesRepository {
  final Dio _mainNetworkClient;

  static const String _PATH = 'api/favourites';

  FavouritesRepositoryImpl(this._mainNetworkClient);

  @override
  Future<Iterable<FavouriteResponse>> fetchFavourites(String type) async {
    final request = FavouriteRequest(type);

    try {
      var response = await _mainNetworkClient.get('$_PATH/',
          queryParameters: request.toJson());

      final favouriteResponse = response.data.map<FavouriteResponse>(
          (category) => FavouriteResponse.fromJson(category));

      return favouriteResponse as Iterable<FavouriteResponse>;
    } catch (ex) {
      throw InternalError();
    }
  }

  @override
  Future<void> addFavourites(String type, int id) async {
    final addPath = '$_PATH/$type/$id/';

    try {
      await _mainNetworkClient.post(addPath);
    } catch (ex) {
      throw InternalError();
    }
  }

  @override
  Future<void> deleteFavourites(String type, int id) async {
    final deletePath = '$_PATH/$type/$id/';

    try {
      await _mainNetworkClient.delete(deletePath);
    } catch (ex) {
      throw InternalError();
    }
  }
}
