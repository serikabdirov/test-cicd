class FavouriteRequest {
  String type;
  int? id;

  FavouriteRequest(this.type, {this.id});

  Map<String, dynamic> toJson() => {'type': type};
}
