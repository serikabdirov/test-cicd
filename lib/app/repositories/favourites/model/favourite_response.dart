import 'package:medtochka/app/resources/base_url.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/favourites/favourite_type_key.dart';

class FavouriteResponse {
  final String? avatar;
  final int id;
  final String info;
  final String name;
  final ResChipType type;
  final String urlPath;

  FavouriteResponse(
      {required this.avatar,
      required this.type,
      required this.id,
      required this.info,
      required this.name,
      required this.urlPath});

  FavouriteResponse.fromJson(Map<String, dynamic> json)
      : avatar = json['avatar'] != null && json['avatar'].toString().isNotEmpty
            ? '${ResBaseUrl.MAIN}/${json['avatar']}'
            : null,
        id = json['id'],
        info = json['info'],
        name = json['name'],
        urlPath = json['url_path'],
        type = _stringToFavouriteStatus(json['type']);

  Map<String, dynamic> toJson() => {
        'avatar': avatar,
        'id': id,
        'info': info,
        'name': name,
        'type': type,
        'url_path': urlPath,
      };

  static ResChipType _stringToFavouriteStatus(String input) {
    switch (input) {
      case FavouriteTypeKey.LPU:
        return ResChipType.favouriteLpu;
      case FavouriteTypeKey.DOCTORS:
        return ResChipType.favouriteDoctors;
      default:
        return ResChipType.favouriteAll;
    }
  }
}
