import 'package:medtochka/app/repositories/favourites/model/favourite_response.dart';

abstract class FavouritesRepository {
  Future<Iterable<FavouriteResponse>> fetchFavourites(String type);

  Future<void> addFavourites(String type, int id);

  Future<void> deleteFavourites(String type, int id);
}
