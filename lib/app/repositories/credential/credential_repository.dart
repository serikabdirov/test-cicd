abstract class CredentialRepository {
  Future<bool> isAuthUser();

  Future<String> getAccessToken();
}
