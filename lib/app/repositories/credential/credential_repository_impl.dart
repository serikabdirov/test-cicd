import 'package:medtochka/app/data/storage/prefs_provider.dart';

import 'credential_repository.dart';

class CredentialRepositoryImpl implements CredentialRepository {
  final PrefsProvider _prefsProvider = PrefsProvider();

  @override
  Future<String> getAccessToken() async {
    final type = await _prefsProvider.getTokenType();
    final accessToken = await _prefsProvider.getAccessToken();
    return "$type $accessToken";
  }

  @override
  Future<bool> isAuthUser() async {
    final accessToken = await _prefsProvider.getAccessToken();
    return accessToken.isNotEmpty;
  }
}
