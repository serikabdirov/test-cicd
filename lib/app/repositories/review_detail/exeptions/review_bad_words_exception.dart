class ReviewBadWordsException implements Exception {
  late final String _message;

  ReviewBadWordsException(this._message);

  @override
  String toString() => _message;
}
