import 'package:medtochka/app/repositories/review_detail/model/review_change_score_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_document_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_document_response.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_edit_text_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_edit_text_response.dart';

abstract class ReviewDetailRepository {
  Future<ReviewDetailResponse> fetchReview(int id, String type);

  Future<ReviewEditTextResponse> fetchComment(int id, String type);

  Future<void> sendComment(int id, String type, ReviewEditTextRequest payload);

  Future<void> completeReview(int id, String type);

  Future<void> patchScore(
      int id, String type, ReviewChangeScoreRequest payload);

  Future<ReviewDocumentResponse> uploadFile(
      int id, String type, ReviewDocumentRequest payload);

  Future<void> deleteFile(int id, String type, int documentId);

  Future<List<ReviewDocumentResponse>> getFiles(int id, String type);
}
