class ReviewAbout {
  final String? avatar;
  final bool? inUserFavourites;
  final String? info;
  final String name;
  final int rateObjectId;
  final String rateType;

  ReviewAbout(
      {this.avatar,
      this.inUserFavourites,
      this.info,
      required this.name,
      required this.rateObjectId,
      required this.rateType});

  ReviewAbout.fromJson(Map<String, dynamic> json)
      : avatar = json['avatar'],
        inUserFavourites = json['in_user_favourites'],
        info = json['info'],
        name = json['name'],
        rateObjectId = json['rate_object_id'],
        rateType = json['rate_type'];
}
