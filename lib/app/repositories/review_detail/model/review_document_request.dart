class ReviewDocumentRequest {
  final String image;

  ReviewDocumentRequest({required this.image});

  Map<String, dynamic> toJson() => {
        'image': image,
      };
}
