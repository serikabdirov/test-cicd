class ReviewDocumentDeleteRequest {
  final int id;

  ReviewDocumentDeleteRequest({required this.id});

  Map<String, dynamic> toJson() => {
        'id': id,
      };
}
