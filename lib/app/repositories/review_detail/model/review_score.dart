import 'package:medtochka/app/repositories/review_detail/model/review_score_detail.dart';

class ReviewScore {
  final Iterable<ReviewScoreDetail> detail;
  final String? type;
  final double? value;

  ReviewScore({required this.detail, this.type, this.value});

  ReviewScore.fromJson(Map<String, dynamic> json)
      : detail = json['detail'].map<ReviewScoreDetail>(
            (detail) => ReviewScoreDetail.fromJson(detail)),
        value = json['value'],
        type = json['type'];

  Map<String, dynamic> toJson() => {
        'detail': detail,
        'value': value,
        'type': type,
      };
}
