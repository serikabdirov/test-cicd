class ReviewDetailReason {
  final String? moreInfoText;
  final String? category;
  final String text;
  final String title;

  ReviewDetailReason({
    this.moreInfoText,
    this.category,
    required this.text,
    required this.title,
  });

  ReviewDetailReason.fromJson(Map<String, dynamic> json)
      : category = json['category'],
        moreInfoText = null,
        title = json['title'],
        text = json['text'];
}
