import 'package:medtochka/app/repositories/review_detail/model/review_score.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_text.dart';

class ReviewRate {
  final String address;
  final bool? byCall;
  final int? chat;
  final String? chatResolved;
  final String dtCreated;
  final String dtVisit;
  final int? id;
  final int? messagesCount;
  final ReviewScore score;
  final String status;
  final Iterable<ReviewText> text;

  ReviewRate(
      {required this.address,
      this.byCall,
      this.chat,
      this.chatResolved,
      required this.dtCreated,
      required this.dtVisit,
      this.id,
      this.messagesCount,
      required this.score,
      required this.status,
      required this.text});

  ReviewRate.fromJson(Map<String, dynamic> json)
      : address = json['address'],
        byCall = json['by_call'],
        chat = json['chat'],
        chatResolved = json['chat_resolved'],
        dtCreated = json['dt_created'],
        dtVisit = json['dt_visit'],
        id = json['id'],
        messagesCount = json['messages_count'],
        score = ReviewScore.fromJson(json['score']),
        status = json['status'],
        text = _getFilteredReviewText(json['text']);

  static Iterable<ReviewText> _getFilteredReviewText(json) {
    final Iterable<ReviewText> reviewText =
        json.map<ReviewText>((text) => ReviewText.fromJson(text));

    return reviewText.where((text) => text.comment.isNotEmpty);
  }
}
