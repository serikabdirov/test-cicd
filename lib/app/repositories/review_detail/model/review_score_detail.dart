class ReviewScoreDetail {
  final String? key;
  final String? text;
  final String? type;

  ReviewScoreDetail(
      {required this.key, required this.text, required this.type});

  ReviewScoreDetail.fromJson(Map<String, dynamic> json)
      : key = json['key'],
        type = json['type'],
        text = json['text'];

  Map<String, dynamic> toJson() => {
        'key': key,
        'type': type,
        'text': text,
      };
}
