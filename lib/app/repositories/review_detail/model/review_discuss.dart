class ReviewDiscussion {
  final int id;
  final String supportText;
  final String patientText;

  ReviewDiscussion({
    required this.supportText,
    required this.patientText,
    required this.id,
  });

  ReviewDiscussion.fromJson(Map<String, dynamic> json)
      : supportText = json['support_text'],
        patientText = json['patient_text'],
        id = json['id'];
}
