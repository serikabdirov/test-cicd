class ReviewEditTextResponse {
  final String comment;
  final String commentNeg;
  final String commentPos;
  final int id;
  final bool ignoreBadWords;

  ReviewEditTextResponse(
      {required this.comment,
      required this.commentNeg,
      required this.commentPos,
      required this.id,
      required this.ignoreBadWords});

  ReviewEditTextResponse.fromJson(Map<String, dynamic> json)
      : comment = json['comment'],
        commentNeg = json['comment_neg'],
        commentPos = json['comment_pos'],
        id = json['id'],
        ignoreBadWords = json['ignore_bad_words'];
}
