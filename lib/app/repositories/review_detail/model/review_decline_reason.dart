import 'package:medtochka/app/repositories/review_detail/model/review_detail_reason.dart';

class ReviewDeclineReason {
  final String contactType;
  final String? editType;
  final String? mainKey;
  final Iterable<ReviewDetailReason> reasons;

  ReviewDeclineReason({
    required this.contactType,
    this.editType,
    this.mainKey,
    required this.reasons,
  });

  ReviewDeclineReason.fromJson(Map<String, dynamic> json)
      : contactType = json['contact_type'],
        editType = json['edit_type'],
        mainKey = json['main_key'],
        reasons = json['reasons'].map<ReviewDetailReason>(
            (reason) => ReviewDetailReason.fromJson(reason));
}
