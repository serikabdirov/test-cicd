class ReviewEditTextRequest {
  final String comment;
  final String commentNeg;
  final String commentPos;
  final bool ignoreBadWords;

  ReviewEditTextRequest(
      {required this.comment,
      required this.commentNeg,
      required this.commentPos,
      required this.ignoreBadWords});

  Map<String, dynamic> toJson() => {
        'comment': comment,
        'comment_neg': commentNeg,
        'comment_pos': commentPos,
        'ignore_bad_words': ignoreBadWords
      };
}
