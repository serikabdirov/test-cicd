import 'package:medtochka/app/resources/base_url.dart';

class ReviewDocumentResponse {
  final int id;
  final String imgS;
  final String imgL;
  final String? preview;

  ReviewDocumentResponse({
    required this.id,
    required this.imgS,
    required this.imgL,
    this.preview,
  });

  ReviewDocumentResponse.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        imgS = '${ResBaseUrl.MAIN}${json['img_s']}',
        imgL = json['img_l'],
        preview = json['preview'];
}
