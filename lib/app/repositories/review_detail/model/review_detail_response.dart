import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_decline_reason.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_discuss.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_rate.dart';

class ReviewDetailResponse {
  final ReviewAbout about;
  final ReviewAbout? aboutFrom;
  final ReviewDeclineReason? declineReason;
  final ReviewDiscussion? discussion;
  final bool? hospital;
  final ReviewRate? rate;
  final String? rateType;
  final String? status;
  final String actionType;

  ReviewDetailResponse({
    required this.about,
    this.aboutFrom,
    this.declineReason,
    this.discussion,
    this.status,
    required this.actionType,
    required this.hospital,
    required this.rate,
    required this.rateType,
  });

  ReviewDetailResponse.fromJson(Map<String, dynamic> json)
      : about = ReviewAbout.fromJson(json['about']),
        aboutFrom = json['about_from'] != null ? ReviewAbout.fromJson(json['about_from']) : null,
        declineReason = json['decline_reason'] != null
            ? ReviewDeclineReason.fromJson(json['decline_reason'])
            : null,
        hospital = json['hospital'],
        actionType = json['action_type'],
        status = json['status'],
        discussion =
            json['discussion'] != null ? ReviewDiscussion.fromJson(json['discussion']) : null,
        rate = json['rate'] != null ? ReviewRate.fromJson(json['rate']) : null,
        rateType = json['rate_type'];
}
