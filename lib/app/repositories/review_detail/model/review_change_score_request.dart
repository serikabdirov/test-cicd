abstract class ReviewChangeScoreRequest {
  Map<String, dynamic> toJson() => {};
}

class ReviewChangeScoreDoctorRequest implements ReviewChangeScoreRequest {
  final int osmotr;
  final int efficiency;
  final int friendliness;
  final int informativity;
  final int recommend;

  ReviewChangeScoreDoctorRequest(
      {required this.osmotr,
      required this.efficiency,
      required this.friendliness,
      required this.informativity,
      required this.recommend});

  @override
  Map<String, dynamic> toJson() => {
        'osmotr': osmotr,
        'efficiency': efficiency,
        'friendliness': friendliness,
        'informativity': informativity,
        'recommend': recommend
      };
}

class ReviewChangeScoreLpuRequest implements ReviewChangeScoreRequest {
  final int building;
  final int equipment;
  final int medstaff;
  final int leisure;
  final int food;

  ReviewChangeScoreLpuRequest(
      {required this.building,
      required this.equipment,
      required this.medstaff,
      required this.leisure,
      required this.food});

  @override
  Map<String, dynamic> toJson() => {
        'building': building,
        'equipment': equipment,
        'medstaff': medstaff,
        'leisure': leisure,
        'food': food
      };
}
