class ReviewText {
  final String comment;
  final String title;

  ReviewText({required this.comment, required this.title});

  ReviewText.fromJson(Map<String, dynamic> json)
      : comment = json['comment'],
        title = json['title'];

  Map<String, dynamic> toJson() => {
        'comment': comment,
        'title': title,
      };
}
