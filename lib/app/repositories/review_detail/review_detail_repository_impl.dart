import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/review_detail/exeptions/review_bad_words_exception.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_change_score_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_document_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_document_response.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_edit_text_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_edit_text_response.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';

class ReviewDetailRepositoryImpl implements ReviewDetailRepository {
  final Dio _client;

  static const String _PATH = 'api/rates';

  ReviewDetailRepositoryImpl(this._client);

  @override
  Future<ReviewDetailResponse> fetchReview(int id, String type) async {
    var response = await _client.get('$_PATH/$type/$id');

    return ReviewDetailResponse.fromJson(response.data);
  }

  @override
  Future<ReviewEditTextResponse> fetchComment(int id, String type) async {
    var response = await _client.get('$_PATH/$type/$id/comment/');

    return ReviewEditTextResponse.fromJson(response.data);
  }

  @override
  Future<void> sendComment(
      int id, String type, ReviewEditTextRequest payload) async {
    try {
      await _client.post('$_PATH/$type/$id/comment/', data: payload.toJson());
    } on DioError catch (error) {
      if (error.response?.data?['comment']?['code'] == "bad_words") {
        throw ReviewBadWordsException(
            error.response?.data?['comment']?['message']);
      }
    }
  }

  @override
  Future<void> completeReview(int id, String type) async {
    await _client.post('$_PATH/$type/$id/complete/');
  }

  @override
  Future<void> patchScore(
      int id, String type, ReviewChangeScoreRequest payload) async {
    await _client.patch('$_PATH/$type/$id/', data: payload.toJson());
  }

  @override
  Future<ReviewDocumentResponse> uploadFile(
      int id, String type, ReviewDocumentRequest payload) async {
    var response = await _client.post('$_PATH/$type/$id/document/',
        data: payload.toJson());
    return ReviewDocumentResponse.fromJson(response.data);
  }

  @override
  Future<void> deleteFile(int id, String type, int documentId) async {
    await _client.delete('$_PATH/$type/$id/document/$documentId/');
  }

  @override
  Future<List<ReviewDocumentResponse>> getFiles(int id, String type) async {
    var response = await _client.get('$_PATH/$type/$id/document/');

    return response.data['images']
        .map<ReviewDocumentResponse>(
            (json) => ReviewDocumentResponse.fromJson(json))
        .toList();
  }
}
