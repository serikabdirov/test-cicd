import 'package:medtochka/app/data/storage/prefs_provider.dart';

class PrefsRepository {
  final prefsProvider = PrefsProvider();

  Future<bool> wasTourShown() => prefsProvider.wasTourShown();

  Future<void> setWasTourShown(bool was) => prefsProvider.setWasTourShown(was);

  Future<String> getAuthCode() => prefsProvider.getAuthCode();

  Future<void> setAuthCode(String code) => prefsProvider.setAuthCode(code);

  Future<int> getAuthCodeAttempts() => prefsProvider.getAuthCodeAttempts();

  Future<void> setAuthCodeAttempts(int attempts) =>
      prefsProvider.setAuthCodeAttempts(attempts);

  Future<void> setTown(String town) => prefsProvider.setTown(town);

  Future<String> getTown() => prefsProvider.getTown();
}
