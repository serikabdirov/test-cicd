class UserInfoResponse {
  final String phone;
  final String townName;
  final String townKey;
  final bool townConfirmed;
  final bool rulesAgreed;

  UserInfoResponse(
      {required this.phone,
      required this.townName,
      required this.townKey,
      required this.townConfirmed,
      required this.rulesAgreed});

  UserInfoResponse.fromJson(Map<String, dynamic> json)
      : phone = json['phone'],
        townName = json['town_name'],
        townKey = json['town_key'],
        townConfirmed = json['town_confirmed'],
        rulesAgreed = json['rules_agreed'];
}
