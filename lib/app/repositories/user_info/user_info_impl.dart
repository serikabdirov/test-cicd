import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/user_info/model/user_info_response.dart';
import 'package:medtochka/app/repositories/user_info/user_info.dart';

class UserInfoRepositoryImpl implements UserInfoRepository {
  final Dio _mainNetworkClient;
  static const String _PATH = 'api/me/';

  UserInfoRepositoryImpl(this._mainNetworkClient);

  @override
  Future<UserInfoResponse> getInfo() async {
    var response = await _mainNetworkClient.get(_PATH);

    return UserInfoResponse.fromJson(response.data);
  }
}
