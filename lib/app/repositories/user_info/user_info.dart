import 'package:medtochka/app/repositories/user_info/model/user_info_response.dart';

abstract class UserInfoRepository {
  Future<UserInfoResponse> getInfo();
}
