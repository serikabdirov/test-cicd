class ReviewsCategoryRequest {
  String type;

  ReviewsCategoryRequest(this.type);

  Map<String, dynamic> toJson() => {'type': type};
}
