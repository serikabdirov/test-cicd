class ReviewResponse {
  final String? avatar;
  final String? dtCreated;
  final String? dtVisit;
  final int? id;
  final int? lpuIdProdoctorov;
  final bool? inUserFavourites;
  final String? info;
  final int? messagesCount;
  final String name;
  final int? rateObjectId;
  final String rateType;
  final String status;

  ReviewResponse(
      {required this.status,
      required this.avatar,
      this.dtCreated,
      this.dtVisit,
      this.lpuIdProdoctorov,
      this.id,
      this.inUserFavourites,
      this.info,
      this.messagesCount,
      required this.name,
      this.rateObjectId,
      required this.rateType});

  ReviewResponse.fromJson(Map<String, dynamic> json)
      : avatar = json['avatar'],
        dtCreated = json['dt_created'],
        dtVisit = json['dt_visit'],
        id = json['id'],
        inUserFavourites = json['in_user_favourites'],
        info = json['info'],
        messagesCount = json['messages_count'],
        name = json['name'],
        lpuIdProdoctorov = json['lpu_id_prodoctorov'],
        rateObjectId = json['rate_object_id'],
        rateType = json['rate_type'],
        status = json['status'];

  Map<String, dynamic> toJson() => {
        'avatar': avatar,
        'dt_created': dtCreated,
        'id': id,
        'in_user_favourites': inUserFavourites,
        'info': info,
        'messages_count': messagesCount,
        'name': name,
        'rate_object_id': rateObjectId,
        'rate_type': rateType,
        'rates': status,
      };
}
