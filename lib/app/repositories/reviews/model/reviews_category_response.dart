import 'package:medtochka/app/repositories/reviews/model/review_response.dart';
import 'package:medtochka/app/resources/reviews/review_status.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';

class ReviewsCategoryResponse {
  final ResReviewStatus status;
  final Iterable<ReviewResponse> reviews;

  ReviewsCategoryResponse({required this.reviews, required this.status});

  ReviewsCategoryResponse.fromJson(Map<String, dynamic> json)
      : status = _stringToReviewStatus(json['status']),
        reviews = json['rates']
            .map<ReviewResponse>((review) => ReviewResponse.fromJson(review))
            .toList();

  Map<String, dynamic> toJson(json) => {
        'status': status,
        'rates': json,
      };

  static ResReviewStatus _stringToReviewStatus(String input) {
    switch (input) {
      case ReviewStatusKey.ACCEPT:
        return ResReviewStatus.accept;
      case ReviewStatusKey.CORRECT:
        return ResReviewStatus.correct;
      case ReviewStatusKey.REJECT:
        return ResReviewStatus.reject;
      case ReviewStatusKey.ON_MOD:
        return ResReviewStatus.onMod;
      case ReviewStatusKey.WAITING:
        return ResReviewStatus.waiting;
      default:
        return ResReviewStatus.onMod;
    }
  }
}
