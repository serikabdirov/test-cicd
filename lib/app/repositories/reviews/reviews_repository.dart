import 'package:medtochka/app/repositories/reviews/model/reviews_category_response.dart';

abstract class ReviewsRepository {
  Future<Iterable<ReviewsCategoryResponse>> fetchReviews(String type);
}
