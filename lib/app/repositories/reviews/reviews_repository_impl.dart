import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/repositories/reviews/model/reviews_category_request.dart';
import 'package:medtochka/app/repositories/reviews/model/reviews_category_response.dart';
import 'package:medtochka/app/repositories/reviews/reviews_repository.dart';

class ReviewsRepositoryImpl implements ReviewsRepository {
  final Dio _mainNetworkClient;

  static const String _PATH = 'api/rates';

  ReviewsRepositoryImpl(this._mainNetworkClient);

  @override
  Future<Iterable<ReviewsCategoryResponse>> fetchReviews(String type) async {
    final request = ReviewsCategoryRequest(type);

    try {
      final response = await _mainNetworkClient.get(_PATH,
          queryParameters: request.toJson());

      final reviewsCategoryResponse = response.data
          .map<ReviewsCategoryResponse>(
              (category) => ReviewsCategoryResponse.fromJson(category));

      return reviewsCategoryResponse as Iterable<ReviewsCategoryResponse>;
    } catch (ex) {
      throw InternalError();
    }
  }
}
