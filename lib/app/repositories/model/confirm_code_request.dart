class ConfirmCodeRequest {
  String phone;
  String code;
  bool mobileApp = true;

  ConfirmCodeRequest(this.phone, this.code);

  Map<String, dynamic> toJson() =>
      {'phone': phone, 'secret_code': code, 'mobile_app': mobileApp};
}
