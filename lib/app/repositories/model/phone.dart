class Phone {
  String message;
  String code;

  Phone({required this.message, required this.code});

  static Phone? fromJson(Map<String, dynamic>? json) {
    if (json != null) {
      return Phone(message: json['message'], code: json['code']);
    } else {
      return null;
    }
  }
}
