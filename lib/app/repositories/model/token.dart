class Token {
  String tokenType;
  String accessToken;
  String refreshToken;
  int expiresIn;

  Token(
      {required this.tokenType,
      required this.accessToken,
      required this.refreshToken,
      required this.expiresIn});

  static Token? fromJson(Map<String, dynamic>? json) {
    if (json != null) {
      return Token(
          tokenType: json['token_type'],
          accessToken: json['access_token'],
          refreshToken: json['refresh_token'],
          expiresIn: json['expires_in']);
    } else {
      return null;
    }
  }
}
