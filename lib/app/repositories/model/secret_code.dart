class SecretCode {
  String message;
  String code;

  SecretCode({required this.message, required this.code});

  static SecretCode? fromJson(Map<String, dynamic>? json) {
    if (json != null) {
      return SecretCode(message: json['message'], code: json['code']);
    } else {
      return null;
    }
  }
}
