class AuthRequest {
  String phone;
  bool mobileApp = true;

  AuthRequest(this.phone);

  Map<String, dynamic> toJson() => {'phone': phone, 'mobile_app': mobileApp};
}
