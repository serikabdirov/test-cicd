import 'package:medtochka/app/repositories/model/phone.dart';
import 'package:medtochka/app/repositories/model/secret_code.dart';
import 'package:medtochka/app/repositories/model/token.dart';

class AuthResponse {
  Phone? phone;
  SecretCode? secretCode;
  Token? token;

  AuthResponse({this.phone, this.secretCode, this.token});

  static AuthResponse fromJson(Map<String, dynamic> json) {
    return AuthResponse(
        phone: Phone.fromJson(json['phone']),
        secretCode: SecretCode.fromJson(json['secret_code']),
        token: Token.fromJson(json['token']));
  }
}
