import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail_metro.dart';
import 'package:medtochka/app/resources/base_url.dart';

class AppointmentDetailResponse {
  final String? avatarPath;
  final bool cancelable;
  final int? doctorIdProdoctorov;
  final String doctorName;
  final String? doctorUrlProdoctorov;
  final String dtVisit;
  final int id;
  final bool? inUserFavourites;
  final String lpuFullAddress;
  final int? lpuIdProdoctorov;
  final double? lpuLatitude;
  final double? lpuLongitude;
  final String lpuName;
  final String? lpuPhone;
  final Iterable<AppointmentDetailMetro> metroNearby;
  final int? price;
  final String profileUuid;
  final int? rateId;
  final String recommendations;
  final int specialityIdProdoctorov;
  final String specialityName;
  late String status;
  final bool statusCanBeCheckedThroughPd;
  final String? timeEnd;
  final String timeStart;

  AppointmentDetailResponse(
      {required this.avatarPath,
      required this.cancelable,
      this.doctorIdProdoctorov,
      required this.doctorName,
      required this.doctorUrlProdoctorov,
      required this.dtVisit,
      required this.id,
      this.inUserFavourites,
      required this.lpuFullAddress,
      this.lpuIdProdoctorov,
      required this.lpuLatitude,
      required this.lpuLongitude,
      required this.lpuName,
      this.lpuPhone,
      this.price,
      required this.profileUuid,
      this.rateId,
      required this.recommendations,
      required this.specialityIdProdoctorov,
      required this.specialityName,
      required this.status,
      required this.statusCanBeCheckedThroughPd,
      required this.timeEnd,
      required this.metroNearby,
      required this.timeStart});

  AppointmentDetailResponse.fromJson(Map<String, dynamic> json)
      : avatarPath = _formatAvatarPath(json['avatar_path']),
        cancelable = json['cancelable'],
        doctorIdProdoctorov = json['doctor_id_prodoctorov'],
        doctorName = json['doctor_name'],
        doctorUrlProdoctorov = json['doctor_url_prodoctorov'],
        dtVisit = json['dt_visit'],
        id = json['id'],
        inUserFavourites = json['in_user_favourites'],
        lpuFullAddress = json['lpu_full_address'],
        lpuIdProdoctorov = json['lpu_id_prodoctorov'],
        lpuLatitude = json['lpu_latitude'],
        lpuLongitude = json['lpu_longitude'],
        lpuName = json['lpu_name'],
        lpuPhone = json['lpu_phone'],
        metroNearby = json['metro_nearby']
            .map<AppointmentDetailMetro>((metro) => AppointmentDetailMetro.fromJSON(metro)),
        price = json['price'],
        profileUuid = json['profile_uuid'],
        rateId = json['rate_id'],
        recommendations = json['recommendations'],
        specialityIdProdoctorov = json['speciality_id_prodoctorov'],
        specialityName = json['speciality_name'],
        status = json['current_status'],
        statusCanBeCheckedThroughPd = json['status_can_be_checked_through_pd'],
        timeEnd = json['time_end'],
        timeStart = json['time_start'];

  Map<String, dynamic> toJson() => {};

  set updateStatus(String val) => status = val;

  static _formatAvatarPath(String? url) {
    return url != null ? '${ResBaseUrl.MAIN}/$url' : url;
  }
}
