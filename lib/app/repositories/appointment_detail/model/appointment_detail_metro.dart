class AppointmentDetailMetro {
  final int color;
  final String name;
  final double dist;

  AppointmentDetailMetro(
      {required this.color, required this.name, required this.dist});

  AppointmentDetailMetro.fromJSON(Map<String, dynamic> json)
      : color = int.parse(json['color'].replaceAll('#', '0xff')),
        name = json['name'],
        dist = json['dist'];

  Map<String, dynamic> toJson() => {
        'color': color,
        'name': name,
        'dist': dist,
      };
}
