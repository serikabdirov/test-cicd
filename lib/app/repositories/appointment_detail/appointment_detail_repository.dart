import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail.dart';

abstract class AppointmentDetailRepository {
  Future<AppointmentDetailResponse> fetchAppointmentDetail(int id);

  Future? cancelAppointment(int id);
}
