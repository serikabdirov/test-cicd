import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/appointment_detail/appointment_detail_repository.dart';
import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/resources/appointment_detail/appointment_status.dart';

class AppointmentDetailRepositoryImpl implements AppointmentDetailRepository {
  static const String _PATH = 'api/appointments';

  final Dio _client;

  AppointmentDetailRepositoryImpl(this._client);

  @override
  Future<AppointmentDetailResponse> fetchAppointmentDetail(int id) async {
    try {
      var response = await _client.get('$_PATH/$id/');

      return AppointmentDetailResponse.fromJson(response.data);
    } catch (e) {
      throw InternalError();
    }
  }

  @override
  Future? cancelAppointment(int id) {
    return _client
        .patch('$_PATH/$id/', data: {'status': AppointmentStatus.CANCELLED});
  }
}
