import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/auth/auth_repository.dart';
import 'package:medtochka/app/repositories/exceptions/invalid_code_conformation_exception.dart';
import 'package:medtochka/app/repositories/exceptions/invalid_phone_exception.dart';
import 'package:medtochka/app/repositories/model/auth_request.dart';
import 'package:medtochka/app/repositories/model/auth_response.dart';
import 'package:medtochka/app/repositories/model/confirm_code_request.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';

class AuthRepositoryImpl implements AuthRepository {
  static const String _PATH = 'api/profile/phone_login/';

  final TokenManager _tokenManager;
  final Dio _mainNetworkClient;

  AuthRepositoryImpl(this._tokenManager, this._mainNetworkClient);

  @override
  Future<void> authUser(String phone) async {
    final authRequest = AuthRequest(phone);
    try {
      await _mainNetworkClient.post(_PATH, data: authRequest);
      return;
    } on DioError catch (exception) {
      if (exception.response?.statusCode == 400) {
        final response = AuthResponse.fromJson(exception.response?.data);
        throw InvalidPhoneException(response.phone?.message ?? "");
      }
      throw DioError;
    }
  }

  @override
  Future<void> confirmPhone(String phone, String code) async {
    final confirmCodeRequest = ConfirmCodeRequest(phone, code);
    try {
      final response =
          await _mainNetworkClient.post(_PATH, data: confirmCodeRequest);
      if (response.statusCode == 200) {
        final responseConfirm = AuthResponse.fromJson(response.data);
        await _tokenManager.setMainToken(
            responseConfirm.token?.accessToken ?? "",
            responseConfirm.token?.refreshToken ?? "");
        return;
      }
    } on DioError catch (ex) {
      if (ex.response?.statusCode == 400) {
        final response = AuthResponse.fromJson(ex.response?.data);
        throw InvalidCodeConformationException(response.secretCode!.message);
      }
      throw DioError;
    }
  }
}
