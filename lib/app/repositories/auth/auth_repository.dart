abstract class AuthRepository {
  Future<void> authUser(String phone);

  Future<void> confirmPhone(String phone, String code);
}
