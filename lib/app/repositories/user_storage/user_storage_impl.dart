import 'package:medtochka/app/repositories/user_storage/user_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserStorageImpl implements UserStorage {
  static const String _KEY_WAS_TOUR_SHOWN = "key_was_tour_shown";
  static const String _KEY_CODE_AUTH = "key_code_auth";
  static const String _AUTH_CODE_ATTEMPTS = "key_auth_code_attempts";

  static const String _KEY_TOKEN_TYPE = "key_token_type";
  static const String _KEY_ACCESS_TOKEN = "key_access_token";
  static const String _KEY_REFRESH_TOKEN = "key_refresh_token";
  static const String _KEY_EXPIRES_IN = "key_expires_in";

  @override
  Future<String> getAccessToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_KEY_ACCESS_TOKEN) ?? '';
  }

  @override
  Future<String> getAuthCode() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_KEY_CODE_AUTH) ?? '';
  }

  @override
  Future<int> getAuthCodeAttempts() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getInt(_AUTH_CODE_ATTEMPTS) ?? 0;
  }

  @override
  Future<String> getTokenType() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(_KEY_TOKEN_TYPE) ?? '';
  }

  @override
  Future<void> setAuthCode(String code) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_KEY_CODE_AUTH, code);
  }

  @override
  Future<void> setAuthCodeAttempts(int attempts) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(_AUTH_CODE_ATTEMPTS, attempts);
  }

  @override
  Future<void> setCredentials(String tokenType, String accessToken,
      String refreshToken, int expireIn) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_KEY_TOKEN_TYPE, tokenType);
    prefs.setString(_KEY_ACCESS_TOKEN, accessToken);
    prefs.setString(_KEY_REFRESH_TOKEN, refreshToken);
    prefs.setInt(_KEY_EXPIRES_IN, expireIn);
  }

  @override
  Future<void> setWasTourShown(bool was) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(_KEY_WAS_TOUR_SHOWN, was);
  }

  @override
  Future<bool> wasTourShown() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_KEY_WAS_TOUR_SHOWN) ?? false;
  }
}
