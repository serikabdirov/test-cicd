abstract class UserStorage {
  Future<bool> wasTourShown();

  Future<void> setWasTourShown(bool was);

  Future<void> setAuthCode(String code);

  Future<String> getAuthCode();

  Future<void> setAuthCodeAttempts(int attempts);

  Future<int> getAuthCodeAttempts();

  Future<void> setCredentials(
      String tokenType, String accessToken, String refreshToken, int expireIn);

  Future<String> getTokenType();

  Future<String> getAccessToken();
}
