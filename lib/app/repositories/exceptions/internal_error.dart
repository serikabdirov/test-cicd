import 'package:medtochka/app/resources/errors.dart';

class InternalError implements Exception {
  late final String _message = ResErrors.INTERNAL;

  InternalError();

  @override
  String toString() => _message;
}
