class InvalidPhoneException implements Exception {
  late final String _message;

  InvalidPhoneException(this._message);

  @override
  String toString() => _message;
}
