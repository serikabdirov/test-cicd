class InvalidCodeConformationException implements Exception {
  late final String _message;

  InvalidCodeConformationException(this._message);

  @override
  String toString() {
    return _message;
  }
}
