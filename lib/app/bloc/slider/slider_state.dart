part of 'slider_cubit.dart';

class SliderState {
  final Iterable<Widget> _tourSlides = [
    const TourSlide(
      title: ResTourSlider.MED_CARD_TITLE,
      bodyText: ResTourSlider.MED_CARD_DESCRIPTION,
      imageSource: ResTourSlider.MED_CARD_IMAGE,
    ),
    const TourSlide(
      title: ResTourSlider.APPOINTMENT_TITLE,
      bodyText: ResTourSlider.APPOINTMENT_DESCRIPTION,
      imageSource: ResTourSlider.APPOINTMENT_IMAGE,
    )
  ];
  final int page;

  SliderState({this.page = 0});

  SliderState copyWith({int? page}) {
    return SliderState(page: page ?? this.page);
  }

  get totalSlides {
    return _tourSlides.length;
  }

  get tourSlides {
    return _tourSlides;
  }

  get isLastPage {
    return page == totalSlides - 1;
  }
}
