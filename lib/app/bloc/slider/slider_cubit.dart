import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:medtochka/app/resources/tour/slider.dart';
import 'package:medtochka/app/ui/tour/tour_slide.dart';

part 'slider_state.dart';

class SliderCubit extends Cubit<SliderState> {
  SliderCubit() : super(SliderState());

  void onSlideChanged(int page) {
    emit(state.copyWith(page: page));
  }
}
