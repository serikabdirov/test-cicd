part of 'appointment_detail_bloc.dart';

@immutable
abstract class AppointmentDetailEvent {}

class AppointmentDetailFetch extends AppointmentDetailEvent {
  final int id;

  AppointmentDetailFetch(this.id);
}

class AppointmentDetailCancelAppointment extends AppointmentDetailEvent {
  final int id;

  AppointmentDetailCancelAppointment(this.id);
}
