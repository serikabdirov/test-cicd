import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:medtochka/app/repositories/appointment_detail/appointment_detail_repository.dart';
import 'package:medtochka/app/repositories/appointment_detail/model/appointment_detail.dart';
import 'package:medtochka/app/resources/appointment_detail/appointment_status.dart';
import 'package:meta/meta.dart';

part 'appointment_detail_event.dart';
part 'appointment_detail_state.dart';

class AppointmentDetailBloc
    extends Bloc<AppointmentDetailEvent, AppointmentDetailState> {
  final AppointmentDetailRepository _appointmentDetailRepository;

  AppointmentDetailBloc(this._appointmentDetailRepository)
      : super(AppointmentDetailState());

  @override
  Stream<AppointmentDetailState> mapEventToState(
    AppointmentDetailEvent event,
  ) async* {
    if (event is AppointmentDetailFetch) {
      yield* mapAppointmentDetailFetch(event);
    } else if (event is AppointmentDetailCancelAppointment) {
      yield* mapAppointmentCancelAppointment(event);
    }
  }

  Stream<AppointmentDetailState> mapAppointmentDetailFetch(
      AppointmentDetailFetch event) async* {
    yield state.copyWith(status: AppointmentDetailStatus.LOADING);

    try {
      var data =
          await _appointmentDetailRepository.fetchAppointmentDetail(event.id);

      yield state.copyWith(
          status: AppointmentDetailStatus.SUCCESS, appointmentDetail: data);
      // ignore: empty_catches
    } catch (e) {}
  }

  Stream<AppointmentDetailState> mapAppointmentCancelAppointment(
      AppointmentDetailCancelAppointment event) async* {
    yield state.copyWith(cancelStatus: AppointmentCancelStatus.LOADING);

    try {
      await _appointmentDetailRepository.cancelAppointment(event.id);

      state.appointmentDetail?.status = AppointmentStatus.CANCELLED;

      yield state.copyWith(
          cancelStatus: AppointmentCancelStatus.SUCCESS,
          appointmentDetail: state.appointmentDetail);
    } catch (e) {
      yield state.copyWith(cancelStatus: AppointmentCancelStatus.ERROR);
    }
  }
}
