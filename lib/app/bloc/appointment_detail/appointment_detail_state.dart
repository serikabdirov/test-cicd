part of 'appointment_detail_bloc.dart';

enum AppointmentDetailStatus { LOADING, SUCCESS, ERROR }
enum AppointmentCancelStatus { LOADING, SUCCESS, ERROR }

class AppointmentDetailState {
  final AppointmentDetailResponse? appointmentDetail;
  final AppointmentDetailStatus status;
  final AppointmentCancelStatus cancelStatus;

  AppointmentDetailState(
      {this.appointmentDetail,
      this.status = AppointmentDetailStatus.LOADING,
      this.cancelStatus = AppointmentCancelStatus.LOADING});

  AppointmentDetailState copyWith({
    AppointmentDetailStatus? status,
    AppointmentCancelStatus? cancelStatus,
    AppointmentDetailResponse? appointmentDetail,
  }) {
    return AppointmentDetailState(
        status: status ?? this.status,
        cancelStatus: cancelStatus ?? this.cancelStatus,
        appointmentDetail: appointmentDetail ?? this.appointmentDetail);
  }
}
