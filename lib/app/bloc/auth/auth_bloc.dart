import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/auth/auth_event.dart';
import 'package:medtochka/app/bloc/auth/auth_state.dart';
import 'package:medtochka/app/repositories/auth/auth_repository.dart';
import 'package:medtochka/app/repositories/exceptions/invalid_code_conformation_exception.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/user_info/user_info.dart';

import 'model/code.dart';
import 'model/phone.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  static const int _TIMEOUT = 10;
  static const int _LENGTH_PHONE = 16;

  final AuthRepository _authRepository;
  final UserInfoRepository _userRepository;
  final PrefsRepository _prefsRepository;

  final _countDownTimerController = StreamController<int>.broadcast();
  Stream<int> get countDownTimerStream => _countDownTimerController.stream;

  Timer? _timer;

  int _counter = _TIMEOUT;
  int get initCounter => _counter;

  AuthBloc(this._authRepository, this._userRepository, this._prefsRepository)
      : super(const AuthState());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthPhoneEntered) {
      yield _mapPhoneEnteredToState(event, state);
    } else if (event is AuthSmsSent) {
      yield* _mapSmsSentToState(event, state);
    } else if (event is AuthPhoneChanged) {
      _timer?.cancel();
      yield _mapPhoneChangedToState(event, state);
    } else if (event is AuthTimerFinished) {
      yield _mapTimerFinishedToState(event, state);
    } else if (event is AuthSmsResent) {
      yield* _mapSmsResentToState(event, state);
    } else if (event is AuthCodeEntered) {
      yield* _mapCodeEnteredToState(event, state);
    } else if (event is AuthPhoneConfirmed) {
      yield* _mapCodeConfirmedToState(event, state);
    }
  }

  AuthState _mapPhoneEnteredToState(AuthPhoneEntered event, AuthState state) {
    final phone = Phone.dirty(event.phone);
    if (phone.phone?.length == _LENGTH_PHONE) {
      return state.copyWith(status: Status.INPUT_PHONE, phone: phone, isEnabledBtnSendSms: true);
    } else {
      return state.copyWith(status: Status.INPUT_PHONE, phone: phone, isEnabledBtnSendSms: false);
    }
  }

  AuthState _mapTimerFinishedToState(AuthTimerFinished event, AuthState state) {
    return state.copyWith(
        isVisibleCountDownTimer: false, isVisibleBtnResendSms: true, isEnabledBtnResendSms: true);
  }

  Stream<AuthState> _mapSmsSentToState(AuthSmsSent event, AuthState state) async* {
    final exp = RegExp(r"[^0-9]+");
    var phone = Phone.dirty(event.phone.replaceAll(exp, ''));
    yield state.copyWith(
        status: Status.SENDING_SMS,
        phone: phone,
        isSendingSms: true,
        isEnabledInputPhoneField: false,
        isVisibleBtnResendSms: false);
    try {
      await _authRepository.authUser(phone.phone!);
      yield state.copyWith(
          status: Status.INPUT_CODE,
          phone: phone,
          isSendingSms: false,
          isVisibleBtnSendSms: false,
          isEnabledInputPhoneField: false,
          isVisibleInputCodeField: true,
          isEnabledInputCodeField: true,
          isVisibleCountDownTimer: true,
          isVisibleBtnChangePhone: true,
          isEnabledBtnChangePhone: true);
      _startTimer();
    } catch (error) {
      yield state.copyWith(
          status: Status.ERROR_WHEN_SENDING_SMS,
          phone: phone,
          isEnabledBtnSendSms: true,
          isSendingSms: false,
          isEnabledInputPhoneField: true);
    }
  }

  AuthState _mapPhoneChangedToState(AuthPhoneChanged event, AuthState state) {
    const phone = Phone.pure();
    const code = Code.pure();
    return state.copyWith(
        status: Status.INPUT_PHONE,
        phone: phone,
        code: code,
        isEnabledInputPhoneField: true,
        isVisibleBtnSendSms: true,
        isEnabledBtnSendSms: false,
        isVisibleInputCodeField: false,
        isVisibleBtnResendSms: false,
        isVisibleBtnChangePhone: false,
        isVisibleCountDownTimer: false);
  }

  Stream<AuthState> _mapSmsResentToState(AuthSmsResent event, AuthState state) async* {
    final exp = RegExp(r"[^0-9]+");
    var phone = Phone.dirty(event.phone.replaceAll(exp, ''));
    yield state.copyWith(
        status: Status.SENDING_SMS,
        phone: phone,
        code: const Code.dirty(''),
        isResendingSms: true,
        isEnabledBtnResendSms: false,
        isVisibleBtnResendSms: false,
        isEnabledInputCodeField: false,
        isEnabledBtnChangePhone: false);

    try {
      await _authRepository.authUser(phone.phone!);
      phone = Phone.dirty(event.phone);
      yield state.copyWith(
          status: Status.INPUT_CODE,
          phone: phone,
          isVisibleCountDownTimer: true,
          isResendingSms: false,
          isVisibleBtnResendSms: false,
          isEnabledInputCodeField: true,
          isEnabledBtnChangePhone: true);
      _startTimer();
    } catch (exception) {
      yield state.copyWith(
          status: Status.ERROR_WHEN_RESENDING_SMS,
          phone: phone,
          isEnabledBtnSendSms: true,
          isSendingSms: false,
          isEnabledInputPhoneField: true);
    }
  }

  Stream<AuthState> _mapCodeConfirmedToState(AuthPhoneConfirmed event, AuthState state) async* {
    try {
      var response = await _userRepository.getInfo();

      await _prefsRepository.setTown(response.townKey);
      // ignore: empty_catches
    } catch (e) {}

    yield state.copyWith(
        status: Status.VALIDATION_CODE_SUCCESS,
        isEnabledInputCodeField: false,
        isVisibleCountDownTimer: false,
        isVisibleBtnResendSms: true,
        isEnabledBtnResendSms: false,
        isEnabledBtnChangePhone: false);
  }

  Stream<AuthState> _mapCodeEnteredToState(AuthCodeEntered event, AuthState state) async* {
    if (event.code.length == 4) {
      yield state.copyWith(
          status: Status.VALIDATION_CODE,
          isValidationCode: true,
          isEnabledInputCodeField: false,
          isEnabledBtnChangePhone: false,
          isEnabledBtnResendSms: false);

      try {
        await _authRepository.confirmPhone(state.phone.phone!, event.code);

        add(AuthPhoneConfirmed());
      } on InvalidCodeConformationException catch (ex) {
        if (_timer!.isActive) {
          yield state.copyWith(
              status: Status.INPUT_CODE,
              code: Code(event.code, ex.toString()),
              isVisibleCountDownTimer: true,
              isVisibleBtnResendSms: false);
        } else {
          yield state.copyWith(
              status: Status.INPUT_CODE,
              code: Code(event.code, ex.toString()),
              isVisibleCountDownTimer: false,
              isVisibleBtnResendSms: true);
        }
      } catch (exception) {
        if (_timer!.isActive) {
          yield state.copyWith(
              status: Status.ERROR_WHEN_VALIDATION_CODE,
              isVisibleCountDownTimer: true,
              isVisibleBtnResendSms: false);
        } else {
          yield state.copyWith(
              status: Status.ERROR_WHEN_VALIDATION_CODE,
              isVisibleCountDownTimer: false,
              isVisibleBtnResendSms: true);
        }
      }
    } else {
      if (_timer!.isActive) {
        yield state.copyWith(status: Status.INPUT_CODE, code: Code.dirty(event.code));
      } else {
        yield state.copyWith(status: Status.INPUT_CODE, code: Code.dirty(event.code));
      }
    }
  }

  @override
  Future<void> close() async {
    _countDownTimerController.close();
    super.close();
  }

  void _startTimer() {
    _counter = _TIMEOUT;
    _timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (_counter == 0) {
        _timer?.cancel();
      } else {
        _counter--;
        _countDownTimerController.sink.add(_counter);
      }
    });
  }
}
