import 'package:equatable/equatable.dart';
import 'package:medtochka/app/bloc/auth/model/code.dart';
import 'package:medtochka/app/bloc/auth/model/phone.dart';

class AuthState extends Equatable {
  final Status status;
  final Phone phone;
  final Code code;
  final String error;
  final bool isEnabledInputPhoneField;
  final bool isEnabledBtnSendSms;
  final bool isVisibleBtnSendSms;
  final bool isSendingSms;
  final bool isResendingSms;
  final bool isVisibleInputCodeField;
  final bool isEnabledInputCodeField;
  final bool isValidationCode;
  final bool isVisibleCountDownTimer;
  final bool isVisibleBtnResendSms;
  final bool isEnabledBtnResendSms;
  final bool isVisibleBtnChangePhone;
  final bool isEnabledBtnChangePhone;

  const AuthState(
      {this.status = Status.INPUT_PHONE,
      this.phone = const Phone.pure(),
      this.code = const Code.pure(),
      this.error = '',
      this.isEnabledInputPhoneField = true,
      this.isEnabledBtnSendSms = false,
      this.isVisibleBtnSendSms = true,
      this.isSendingSms = false,
      this.isResendingSms = false,
      this.isVisibleInputCodeField = false,
      this.isEnabledInputCodeField = false,
      this.isValidationCode = false,
      this.isVisibleCountDownTimer = false,
      this.isVisibleBtnResendSms = false,
      this.isEnabledBtnResendSms = false,
      this.isVisibleBtnChangePhone = false,
      this.isEnabledBtnChangePhone = false});

  AuthState copyWith(
      {Status? status,
      Phone? phone,
      Code? code,
      String? error,
      bool? isEnabledInputPhoneField,
      bool? isEnabledBtnSendSms,
      bool? isVisibleBtnSendSms,
      bool? isSendingSms,
      bool? isResendingSms,
      bool? isVisibleInputCodeField,
      bool? isEnabledInputCodeField,
      bool? isValidationCode,
      bool? isVisibleCountDownTimer,
      bool? isVisibleBtnResendSms,
      bool? isEnabledBtnResendSms,
      bool? isVisibleBtnChangePhone,
      bool? isEnabledBtnChangePhone}) {
    return AuthState(
        status: status ?? this.status,
        phone: phone ?? this.phone,
        code: code ?? this.code,
        error: error ?? this.error,
        isEnabledInputPhoneField:
            isEnabledInputPhoneField ?? this.isEnabledInputPhoneField,
        isEnabledBtnSendSms: isEnabledBtnSendSms ?? this.isEnabledBtnSendSms,
        isVisibleBtnSendSms: isVisibleBtnSendSms ?? this.isVisibleBtnSendSms,
        isSendingSms: isSendingSms ?? this.isSendingSms,
        isResendingSms: isResendingSms ?? this.isResendingSms,
        isVisibleInputCodeField:
            isVisibleInputCodeField ?? this.isVisibleInputCodeField,
        isEnabledInputCodeField:
            isEnabledInputCodeField ?? this.isEnabledInputCodeField,
        isValidationCode: isValidationCode ?? this.isValidationCode,
        isVisibleCountDownTimer:
            isVisibleCountDownTimer ?? this.isVisibleCountDownTimer,
        isVisibleBtnResendSms:
            isVisibleBtnResendSms ?? this.isVisibleBtnResendSms,
        isEnabledBtnResendSms:
            isEnabledBtnResendSms ?? this.isEnabledBtnResendSms,
        isVisibleBtnChangePhone:
            isVisibleBtnChangePhone ?? this.isVisibleBtnChangePhone,
        isEnabledBtnChangePhone:
            isEnabledBtnChangePhone ?? this.isEnabledBtnChangePhone);
  }

  @override
  List<Object> get props => [
        status,
        phone,
        code,
        error,
        isEnabledInputPhoneField,
        isEnabledBtnSendSms,
        isVisibleBtnSendSms,
        isSendingSms,
        isResendingSms,
        isVisibleInputCodeField,
        isEnabledInputCodeField,
        isValidationCode,
        isVisibleCountDownTimer,
        isVisibleBtnResendSms,
        isEnabledBtnResendSms,
        isVisibleBtnChangePhone,
        isEnabledBtnChangePhone
      ];
}

enum Status {
  INPUT_PHONE,
  SENDING_SMS,
  INPUT_CODE,
  VALIDATION_CODE,
  VALID_CODE,
  ERROR_WHEN_SENDING_SMS,
  ERROR_WHEN_RESENDING_SMS,
  ERROR_WHEN_VALIDATION_CODE,
  VALIDATION_CODE_SUCCESS
}
