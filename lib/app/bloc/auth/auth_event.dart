import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

@immutable
class AuthPhoneEntered extends AuthEvent {
  final String phone;

  const AuthPhoneEntered(this.phone);

  @override
  List<Object> get props => [phone];
}

@immutable
class AuthSmsSent extends AuthEvent {
  final String phone;

  const AuthSmsSent(this.phone);

  @override
  List<Object> get props => [phone];
}

@immutable
class AuthSmsResent extends AuthEvent {
  final String phone;

  const AuthSmsResent(this.phone);
}

@immutable
class AuthPhoneChanged extends AuthEvent {}

@immutable
class AuthPhoneConfirmed extends AuthEvent {}

@immutable
class AuthTimerFinished extends AuthEvent {}

@immutable
class AuthCodeEntered extends AuthEvent {
  final String code;

  const AuthCodeEntered(this.code);
}

@immutable
class AuthRetry extends AuthEvent {}
