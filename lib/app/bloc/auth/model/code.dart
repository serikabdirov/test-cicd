class Code {
  final String? code;
  final String? error;

  const Code(this.code, this.error);

  const Code.pure() : this(null, null);

  const Code.dirty(String code) : this(code, null);

  const Code.error(String code, String error) : this(code, error);
}
