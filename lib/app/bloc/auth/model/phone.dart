class Phone {
  final String? phone;

  const Phone(this.phone);

  const Phone.pure() : this(null);

  const Phone.dirty(String phone) : this(phone);
}
