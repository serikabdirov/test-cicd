import 'package:flutter/foundation.dart';

@immutable
abstract class SplashState {}

@immutable
class SplashInitial extends SplashState {}

@immutable
class SplashShowTour extends SplashState {}

@immutable
class SplashShowAuth extends SplashState {}

@immutable
class SplashShowHome extends SplashState {}

@immutable
class SplashShowCreateAuthCode extends SplashState {}

@immutable
class SplashShowAuthCode extends SplashState {}
