import 'package:flutter/foundation.dart';

@immutable
abstract class SplashEvent {}

class SplashLoaded extends SplashEvent {}
