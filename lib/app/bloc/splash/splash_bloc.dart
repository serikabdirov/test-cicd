import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:medtochka/app/bloc/splash/splash_events.dart';
import 'package:medtochka/app/bloc/splash/splash_states.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/repositories/user_info/user_info.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  final PrefsRepository _prefsRepository;
  final UserInfoRepository _userInfoRepository;
  final TokenManager _tokenManager;

  SplashBloc(this._prefsRepository, this._tokenManager, this._userInfoRepository)
      : super(SplashInitial());

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    if (event is SplashLoaded) {
      await Future.delayed(const Duration(seconds: 1));

      if (!await _prefsRepository.wasTourShown()) {
        yield SplashShowTour();
      } else if (!await _isExistMainToken()) {
        yield SplashShowAuth();
      } else if (!(await _prefsRepository.getAuthCode()).isNotEmpty) {
        yield SplashShowCreateAuthCode();
      } else if ((await _prefsRepository.getAuthCode()).isNotEmpty) {
        try {
          var response = await _userInfoRepository.getInfo();

          await _prefsRepository.setTown(response.townKey);
          // ignore: empty_catches
        } catch (e) {}

        yield SplashShowAuthCode();
      } else {
        yield SplashShowHome();
      }
    }
  }

  Future<bool> _isExistMainToken() async {
    final token = await _tokenManager.getMainToken();
    return token != null && token.isNotEmpty;
  }
}
