import 'package:medtochka/app/resources/chip_type.dart';

class Favourite {
  final String? avatar;
  final String info;
  final String name;
  final int id;
  final ResChipType type;
  final String urlPath;

  Favourite(
      {this.avatar,
      required this.info,
      required this.name,
      required this.id,
      required this.type,
      required this.urlPath});
}
