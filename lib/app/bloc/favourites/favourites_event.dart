part of 'favourites_bloc.dart';

abstract class FavouritesEvent {
  const FavouritesEvent();
}

class FavouritesFetch extends FavouritesEvent {
  final ResChipType? type;

  FavouritesFetch({this.type});
}

class _FavouritesSetNewList extends FavouritesEvent {
  final Iterable<Favourite> favourites;

  _FavouritesSetNewList(this.favourites);
}

class _FavouritesSetError extends FavouritesEvent {}

class FavouritesClearSnackBarText extends FavouritesEvent {}

class FavouritesRemove extends FavouritesEvent {
  final Favourite favourite;

  FavouritesRemove(this.favourite);
}

class FavouritesAdd extends FavouritesEvent {
  final Favourite favourite;
  final ResChipType type;

  FavouritesAdd(this.favourite, this.type);
}
