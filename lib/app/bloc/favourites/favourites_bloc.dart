import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:medtochka/app/bloc/favourites/model/favourite.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/repositories/favourites/favourites_repository.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/favourites/favourites.dart';

part 'favourites_event.dart';
part 'favourites_state.dart';

Iterable<ResChipType> _initChipOptions = [
  ResChipType.favouriteAll,
  ResChipType.favouriteDoctors,
  ResChipType.favouriteLpu,
];

class FavouritesBloc extends Bloc<FavouritesEvent, FavouritesState> {
  final FavouritesRepository _favouritesRepository;

  StreamSubscription? chipFilterSubscription;
  StreamSubscription? searchSubscription;

  FavouritesBloc(this._favouritesRepository)
      : super(
          FavouritesState(
            status: listStatus.LOADING,
            selectedType: _initChipOptions.first,
            chipOptions: _initChipOptions,
          ),
        ) {
    on<FavouritesFetch>(_favouritesFetchEmitter);

    on<FavouritesRemove>(_favouritesRemoveEmitter);

    on<FavouritesAdd>(_favouritesAddEmitter);

    on<_FavouritesSetError>(_setError);

    on<_FavouritesSetNewList>(_setNewList);

    on<FavouritesClearSnackBarText>(_favouritesClearSnackBarText);
  }

  void _favouritesFetchEmitter(FavouritesFetch event, Emitter<FavouritesState> emit) {
    var selectedFilter = event.type ?? state.selectedType;

    emit(state.copyWith(status: listStatus.LOADING, selectedType: selectedFilter));

    searchSubscription?.cancel();

    searchSubscription = _fetchFavourites(selectedFilter).asStream().listen((favourites) {
      add(_FavouritesSetNewList(favourites));
    })
      ..onError((error, _) {
        add(_FavouritesSetError());
      });
  }

  void _setNewList(_FavouritesSetNewList event, Emitter<FavouritesState> emit) {
    emit(state.copyWith(favourites: event.favourites, status: listStatus.SUCCESS));
  }

  void _setError(_FavouritesSetError event, Emitter<FavouritesState> emit) {
    emit(state.copyWith(status: listStatus.ERROR));
  }

  void _favouritesClearSnackBarText(
    FavouritesClearSnackBarText event,
    Emitter<FavouritesState> emit,
  ) {
    emit(state.copyWith(snackBarText: null));
  }

  void _favouritesRemoveEmitter(FavouritesRemove event, Emitter<FavouritesState> emit) async {
    try {
      await _removeFavourite(event.favourite);

      var onlyFavourite = await _removeFavourite(event.favourite);
      emit(
        state.copyWith(
          status: listStatus.SUCCESS,
          favourites: onlyFavourite,
          removedFavourite: event.favourite,
        ),
      );
    } catch (e) {
      emit(state.copyWith(snackBarText: ResFavourites.SNACK_BAR_TEXT_REMOVE_ERROR));
    }
  }

  void _favouritesAddEmitter(FavouritesAdd event, Emitter<FavouritesState> emit) async {
    try {
      await _addFavourite(event.favourite);

      if (state.isCorrectType(event.favourite.type)) {
        emit(
          state.copyWith(
            status: listStatus.SUCCESS,
            favourites: [event.favourite, ...state.favourites],
            snackBarText: ResFavourites.SNACK_BAR_TEXT_CANCELLED,
          ),
        );
      }
    } catch (e) {
      emit(state.copyWith(snackBarText: ResFavourites.SNACK_BAR_TEXT_ADD_ERROR));
    }
  }

  Future<Iterable<Favourite>> _fetchFavourites(ResChipType type) async {
    try {
      var favourites = await _favouritesRepository.fetchFavourites(type.key);

      return favourites.map(
        (favourite) => Favourite(
          avatar: favourite.avatar,
          info: favourite.info,
          name: favourite.name,
          urlPath: favourite.urlPath,
          id: favourite.id,
          type: favourite.type,
        ),
      );
    } catch (ex) {
      throw InternalError();
    }
  }

  Future<Iterable<Favourite>?> _removeFavourite(Favourite favourite) async {
    try {
      await _favouritesRepository.deleteFavourites(favourite.type.key, favourite.id);

      return state.favourites.where((f) => f.id != favourite.id);
    } catch (ex) {
      throw InternalError();
    }
  }

  Future<Iterable<Favourite>?> _addFavourite(Favourite favourite) async {
    try {
      await _favouritesRepository.addFavourites(favourite.type.key, favourite.id);
    } catch (ex) {
      throw InternalError();
    }
  }

  @override
  Future<void> close() {
    searchSubscription?.cancel();

    return super.close();
  }
}
