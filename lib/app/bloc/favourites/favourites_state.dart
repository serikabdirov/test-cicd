part of 'favourites_bloc.dart';

enum listStatus { LOADING, SUCCESS, ERROR }

class FavouritesState extends Equatable {
  final listStatus status;
  final ResChipType selectedType;
  final Iterable<Favourite> favourites;
  final Favourite? removedFavourite;
  final String? snackBarText;
  final Iterable<ResChipType> chipOptions;

  const FavouritesState({
    required this.status,
    this.favourites = const [],
    this.removedFavourite,
    this.snackBarText,
    required this.selectedType,
    required this.chipOptions,
  });

  FavouritesState copyWith({
    listStatus? status,
    Iterable<Favourite>? favourites,
    Favourite? removedFavourite,
    String? snackBarText,
    ResChipType? selectedType,
  }) {
    return FavouritesState(
      status: status ?? this.status,
      favourites: favourites ?? this.favourites,
      removedFavourite: removedFavourite,
      snackBarText: snackBarText,
      chipOptions: chipOptions,
      selectedType: selectedType ?? this.selectedType,
    );
  }

  bool isCorrectType(ResChipType type) {
    return (selectedType == type || selectedType == ResChipType.favouriteAll);
  }

  @override
  List<Object?> get props => [status, selectedType, favourites, snackBarText, removedFavourite];
}
