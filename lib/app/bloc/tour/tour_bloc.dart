import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:meta/meta.dart';

part 'tour_event.dart';
part 'tour_state.dart';

class TourBloc extends Bloc<TourEvent, TourState> {
  final PrefsRepository _prefsRepository;

  TourBloc(this._prefsRepository) : super(TourInitial());

  @override
  Stream<TourState> mapEventToState(
    TourEvent event,
  ) async* {
    if (event is TourCompleted) {
      _prefsRepository.setWasTourShown(true);

      yield TourShowAuth();
    }
  }
}
