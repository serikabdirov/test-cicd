part of 'tour_bloc.dart';

@immutable
abstract class TourState {}

class TourInitial extends TourState {}

class TourShowAuth extends TourState {}
