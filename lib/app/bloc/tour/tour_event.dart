part of 'tour_bloc.dart';

@immutable
abstract class TourEvent {}

class TourLoaded extends TourEvent {}

class TourCompleted extends TourEvent {}
