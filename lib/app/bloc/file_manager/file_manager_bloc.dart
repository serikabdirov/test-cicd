import 'dart:convert';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:medtochka/app/bloc/file_manager/model/file_model.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_document_request.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/resources/file_manager/file_manager.dart';
import 'package:medtochka/app/resources/file_manager/file_manager_error.dart';
import 'package:meta/meta.dart';

part 'file_manager_event.dart';
part 'file_manager_state.dart';

class FileManagerBloc extends Bloc<FileManagerEvent, FileManagerState> {
  final ImagePicker _picker = ImagePicker();
  final int id;
  final String type;
  final ReviewDetailRepository _reviewDetailRepository;

  FileManagerBloc(this._reviewDetailRepository, {required this.id, required this.type})
      : super(FileManagerState()) {
    on<FileManagerDownloadGallery>(uploadFromGallery);

    on<FileManagerResend>(resendFile);

    on<FileManagerDownloadCamera>(uploadFromCamera);

    on<FileManagerDelete>(deleteFile);

    on<FileManagerChangeFile>(changeStatus);

    on<FileManagerSetFile>(setFile);
  }

  Future<void> uploadFromGallery(
      FileManagerDownloadGallery event, Emitter<FileManagerState> emit) async {
    List<XFile>? result = await _picker.pickMultiImage();
    ResFileManagerError? fileError;

    if (result != null) {
      await Future.forEach(result, (XFile element) async {
        if (!validFile(element.path)) {
          fileError = ResFileManagerError.ERROR_WRONG_FORMAT;

          return;
        }

        if (await element.length() > ResFileManager.MAX_SIZE) {
          fileError = ResFileManagerError.ERROR_MAX_SIZE;

          return;
        }

        if (state.isReachedLimit()) {
          fileError = ResFileManagerError.ERROR_LIMIT;

          return;
        }

        var file = StoreFile(
          path: element.path,
        );

        uploadFile(file);

        emit(state.copyWith(files: [...state.files, file]));
      });
    }

    if (fileError != null) {
      emit(state.copyWith(error: fileError));
    }
  }

  Future<void> uploadFromCamera(
      FileManagerDownloadCamera event, Emitter<FileManagerState> emit) async {
    XFile? result = await _picker.pickImage(source: ImageSource.camera);

    if (result != null) {
      var file = StoreFile(
        path: result.path,
      );

      uploadFile(file);

      emit(
        state.copyWith(
          files: [...state.files, file],
        ),
      );
    }
  }

  Future<void> changeStatus(FileManagerChangeFile event, Emitter<FileManagerState> emit) async {
    emit(state.copyWith(files: state.files.map((element) {
      if (event.file.key != element.key) {
        return element;
      }

      if (event.newId != null) {
        (element as StoreFile).changeId = event.newId!;
      }

      element.changeStatus = event.status;

      return element;
    })));
  }

  Future<void> deleteFile(FileManagerDelete event, Emitter<FileManagerState> emit) async {
    if (event.file is StoreFile && event.file.status == messageStatus.FAILED) {
      emit(state
          .copyWith(files: [...state.files.where((element) => element.key != event.file.key)]));

      return;
    }

    add(FileManagerChangeFile(file: event.file, status: messageStatus.UPLOADING));
    try {
      await _reviewDetailRepository.deleteFile(id, type, event.file.id!);

      emit(state
          .copyWith(files: [...state.files.where((element) => element.key != event.file.key)]));
    } catch (e) {
      emit(state.copyWith(error: ResFileManagerError.ERROR_SERVER));
    }
  }

  Future<void> setFile(FileManagerSetFile event, Emitter<FileManagerState> emit) async {
    emit(state.copyWith(files: [...event.file]));
  }

  Future<void> resendFile(FileManagerResend event, Emitter<FileManagerState> emit) async {
    uploadFile(event.file);
  }

  Future<void> uploadFile(StoreFile file) async {
    try {
      add(FileManagerChangeFile(
        status: messageStatus.UPLOADING,
        file: file,
      ));

      var successFile = await _reviewDetailRepository.uploadFile(
          id,
          type,
          ReviewDocumentRequest(
              image: 'data:image/jpg;base64,${base64Encode(File(file.path).readAsBytesSync())}'));

      add(FileManagerChangeFile(
        newId: successFile.id,
        status: messageStatus.SENT,
        file: file,
      ));
    } catch (e) {
      add(FileManagerChangeFile(file: file, newId: file.id, status: messageStatus.FAILED));
    }
  }
}

bool validFile(String path) {
  final re = RegExp(r'\.(jpe?g|png)$');

  return re.hasMatch(path);
}
