part of 'file_manager_bloc.dart';

class FileManagerState {
  final Iterable<FileModel> files;
  final ResFileManagerError? error;
  static const int IMAGE_LIMIT = 5;

  FileManagerState({this.files = const [], this.error});

  FileManagerState copyWith(
      {Iterable<FileModel>? files, ResFileManagerError? error}) {
    return FileManagerState(files: files ?? this.files, error: error);
  }

  bool isReachedLimit() {
    return files.length >= IMAGE_LIMIT;
  }

  bool hasLoadedFile() {
    return files.where((file) => file.status == messageStatus.SENT).isNotEmpty;
  }
}
