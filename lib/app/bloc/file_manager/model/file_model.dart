import 'package:uuid/uuid.dart';

enum messageStatus { UPLOADING, FAILED, SENT }

class FileModel {
  final String key;
  messageStatus status;
  int? id;

  FileModel({
    required this.status,
    required this.key,
    this.id,
  });

  set changeStatus(messageStatus newStatus) => status = newStatus;
}

class NetworkFile extends FileModel {
  final String imgS;
  final String imgL;

  NetworkFile({id, required this.imgS, required this.imgL})
      : super(status: messageStatus.SENT, key: const Uuid().v4(), id: id);
}

class StoreFile extends FileModel {
  final String path;

  StoreFile({id, required this.path})
      : super(status: messageStatus.UPLOADING, key: const Uuid().v4(), id: id);

  set changeId(int newId) => id = newId;
}
