part of 'file_manager_bloc.dart';

@immutable
abstract class FileManagerEvent {}

class FileManagerDownloadGallery extends FileManagerEvent {}

class FileManagerDownloadCamera extends FileManagerEvent {}

class FileManagerChangeFile extends FileManagerEvent {
  final FileModel file;
  final messageStatus status;
  final int? newId;

  FileManagerChangeFile({required this.file, required this.status, this.newId});
}

class FileManagerDelete extends FileManagerEvent {
  final FileModel file;

  FileManagerDelete(this.file);
}

class FileManagerResend extends FileManagerEvent {
  final StoreFile file;

  FileManagerResend(this.file);
}

class FileManagerSetFile extends FileManagerEvent {
  final Iterable<FileModel> file;

  FileManagerSetFile(this.file);
}
