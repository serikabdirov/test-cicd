part of 'create_auth_code_bloc.dart';

@immutable
abstract class CreateAuthCodeState {
  final String code;

  const CreateAuthCodeState({required this.code});
}

class CreateAuthCodeInitial extends CreateAuthCodeState {
  const CreateAuthCodeInitial() : super(code: '');
}

class CreateAuthCodeConfirm extends CreateAuthCodeState {
  final bool isLoading;
  final bool isConfirmed;

  const CreateAuthCodeConfirm(
      {required String code, this.isLoading = false, this.isConfirmed = false})
      : super(code: code);

  CreateAuthCodeConfirm setLoadingState({required bool isLoading}) {
    return CreateAuthCodeConfirm(
        code: code, isLoading: isLoading, isConfirmed: isConfirmed);
  }

  CreateAuthCodeConfirm setConfirmState({required bool isConfirmed}) {
    return CreateAuthCodeConfirm(
        code: code, isLoading: true, isConfirmed: isConfirmed);
  }
}
