import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:medtochka/app/bloc/code_entry/code_entry_bloc.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/resources/create_auth_code/create_auth_code.dart';
import 'package:meta/meta.dart';

part 'create_auth_code_event.dart';
part 'create_auth_code_state.dart';

class CreateAuthCodeBloc extends Bloc<CreateAuthCodeEvent, CreateAuthCodeState> {
  final CreateCodeEntryBloc codeEntry;
  late StreamSubscription codeEntrySubscription;
  final PrefsRepository _prefsRepository;

  CreateAuthCodeBloc(this._prefsRepository, {required this.codeEntry})
      : super(const CreateAuthCodeInitial()) {
    codeEntrySubscription = codeEntry.stream.listen(_onCodeEntryStateChanged);

    codeEntry.add(CodeEntrySetInitial(infoMessage: ResCreateAuthCode.CODE_INFO));
  }

  @override
  Stream<CreateAuthCodeState> mapEventToState(
    CreateAuthCodeEvent event,
  ) async* {
    if (codeEntry.state.status == KeyboardStatus.locked) {
      return;
    }

    if (state is CreateAuthCodeInitial) {
      if (event is CreateAuthCodeEnter) {
        yield* _mapCreateAuthCodeCreate(state as CreateAuthCodeInitial);
      }
    } else if (state is CreateAuthCodeConfirm) {
      if ((state as CreateAuthCodeConfirm).isLoading) {
        return;
      }

      if (event is CreateAuthCodeEnter) {
        yield* _mapCreateAuthCodeConfirm(state as CreateAuthCodeConfirm);
      } else if (event is CreateAuthCodeSetInitial) {
        yield* _mapCreateAuthCodeSetInitial();
      }
    }
  }

  Stream<CreateAuthCodeState> _mapCreateAuthCodeConfirm(CreateAuthCodeConfirm state) async* {
    if (state.code != codeEntry.state.code) {
      codeEntry.add(CodeEntryError(ResCreateAuthCode.DIFFERENT_CODE_ERROR));

      return;
    }

    yield state.setLoadingState(isLoading: true);

    await _prefsRepository.setAuthCode(state.code);

    yield state.setConfirmState(isConfirmed: true);
  }

  Stream<CreateAuthCodeState> _mapCreateAuthCodeCreate(CreateAuthCodeInitial state) async* {
    yield CreateAuthCodeConfirm(code: codeEntry.state.code);

    codeEntry.add(CodeEntrySetInitial(infoMessage: ''));

    return;
  }

  Stream<CreateAuthCodeState> _mapCreateAuthCodeSetInitial() async* {
    codeEntry.add(CodeEntrySetInitial(infoMessage: ResCreateAuthCode.CODE_INFO));

    yield const CreateAuthCodeInitial();

    return;
  }

  Future<void> _onCodeEntryStateChanged(CodeEntryState state) async {
    if (state.isCodeEntered) {
      return;
    }

    if (!state.isCodeUnique) {
      codeEntry.add(CodeEntryError(ResCreateAuthCode.SIMPLE_CODE_ERROR));

      return;
    }

    await Future.delayed(Duration(milliseconds: state.codeEntryDelay));

    add(CreateAuthCodeEnter());
  }

  @override
  Future<void> close() {
    codeEntrySubscription.cancel();

    return super.close();
  }
}
