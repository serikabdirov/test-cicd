part of 'create_auth_code_bloc.dart';

@immutable
abstract class CreateAuthCodeEvent {}

class CreateAuthCodeEnter extends CreateAuthCodeEvent {}

class CreateAuthCodeSetInitial extends CreateAuthCodeEvent {}
