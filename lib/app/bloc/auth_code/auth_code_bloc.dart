import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:medtochka/app/bloc/code_entry/code_entry_bloc.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/resources/auth_code/auth_code.dart';
import 'package:meta/meta.dart';

part 'auth_code_event.dart';
part 'auth_code_state.dart';

class AuthCodeBloc extends Bloc<AuthCodeEvent, AuthCodeState> {
  final ConfirmCodeEntryBloc codeEntry;
  late StreamSubscription codeEntrySubscription;
  final PrefsRepository _prefsRepository;
  final TokenManager _tokenManager;

  AuthCodeBloc(this._prefsRepository, this._tokenManager, {required this.codeEntry})
      : super(AuthCodeState(status: AuthCodeStatus.INPUT_CODE)) {
    codeEntrySubscription = codeEntry.stream.listen(_onCodeEntryStateChanged);

    codeEntry.add(CodeEntrySetInitial());
  }

  @override
  Stream<AuthCodeState> mapEventToState(
    AuthCodeEvent event,
  ) async* {
    if (event is AuthCodeSend) {
      yield* _mapAuthCodeSend(event);
    } else if (event is AuthCodeShowExitDialog) {
      yield* _mapAuthCodeShowExitDialog();
    } else if (event is AuthCodeExit) {
      yield* _mapAuthCodeExit();
    } else if (event is AuthCodeCloseDialog) {
      yield* _mapAuthCodeCloseDialog();
    } else if (event is AuthCodeLogout) {
      yield* _mapAuthCodeLogoutUser();
    }
  }

  Stream<AuthCodeState> _mapAuthCodeSend(AuthCodeSend event) async* {
    yield state.copyWith(status: AuthCodeStatus.SEND_CODE);

    final String authCode = await _prefsRepository.getAuthCode();
    final int authCodeAttempts = await _prefsRepository.getAuthCodeAttempts();

    if (state.isErrorLimit(authCodeAttempts)) {
      yield state.copyWith(status: AuthCodeStatus.ERROR_LIMIT);

      return;
    }

    if (authCode != event.code) {
      await _prefsRepository.setAuthCodeAttempts(authCodeAttempts + 1);

      yield state.copyWith(status: AuthCodeStatus.WRONG_CODE);

      codeEntry.add(
          CodeEntryError(ResAuthCode.attemptsLeftMessage(state.attemptsLeft(authCodeAttempts))));

      return;
    }

    _prefsRepository.setAuthCodeAttempts(0);

    yield state.copyWith(status: AuthCodeStatus.CODE_CONFIRMED);

    codeEntry.add(CodeEntryUnlock());
  }

  Stream<AuthCodeState> _mapAuthCodeShowExitDialog() async* {
    yield state.copyWith(status: AuthCodeStatus.EXIT_MESSAGE);
  }

  Stream<AuthCodeState> _mapAuthCodeExit() async* {
    yield state.copyWith(status: AuthCodeStatus.EXIT);
  }

  Stream<AuthCodeState> _mapAuthCodeCloseDialog() async* {
    yield state.copyWith(status: AuthCodeStatus.INPUT_CODE);
  }

  Future<void> _onCodeEntryStateChanged(CodeEntryState state) async {
    if (state.isCodeEntered) {
      return;
    }

    if (state.isKeyboardLocked) {
      return;
    }

    codeEntry.add(CodeEntryLock());

    await Future.delayed(Duration(milliseconds: state.codeEntryDelay));

    add(AuthCodeSend(code: state.code));
  }

  Stream<AuthCodeState> _mapAuthCodeLogoutUser() async* {
    try {
      await _tokenManager.revokeMainToken();

      await _prefsRepository.setAuthCode('');

      yield state.copyWith(status: AuthCodeStatus.AUTH_PAGE);
      // ignore: empty_catches
    } catch (e) {}
  }

  @override
  Future<void> close() {
    codeEntrySubscription.cancel();

    return super.close();
  }
}
