part of 'auth_code_bloc.dart';

@immutable
abstract class AuthCodeEvent {}

class AuthCodeInitial extends AuthCodeEvent {}

class AuthCodeSend extends AuthCodeEvent {
  final String code;

  AuthCodeSend({required this.code});
}

class AuthCodeShowExitDialog extends AuthCodeEvent {}

class AuthCodeExit extends AuthCodeEvent {}

class AuthCodeCloseDialog extends AuthCodeEvent {}

class AuthCodeLogout extends AuthCodeEvent {}
