part of 'auth_code_bloc.dart';

enum AuthCodeStatus {
  INPUT_CODE,
  SEND_CODE,
  WRONG_CODE,
  ERROR_LIMIT,
  EXIT_MESSAGE,
  EXIT,
  BLOCK_PAGE,
  CODE_CONFIRMED,
  AUTH_PAGE
}

class AuthCodeState {
  final AuthCodeStatus status;
  final int _errorLimit = 10;

  AuthCodeState({required this.status});

  AuthCodeState copyWith({AuthCodeStatus? status}) {
    return AuthCodeState(status: status ?? this.status);
  }

  bool isErrorLimit(int errorCount) {
    return errorCount >= _errorLimit;
  }

  int attemptsLeft(int errorCount) {
    return _errorLimit - errorCount;
  }
}
