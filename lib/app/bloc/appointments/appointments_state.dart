part of 'appointments_bloc.dart';

enum listStatus { LOADING, SUCCESS, ERROR }

class AppointmentsState {
  final listStatus status;
  final ResChipType selectedType;
  final Iterable<AppointmentsResponse> appointments;
  final Iterable<ResChipType> chipOptions;

  AppointmentsState({
    required this.status,
    this.appointments = const [],
    required this.chipOptions,
    required this.selectedType,
  });

  AppointmentsState copyWith({
    listStatus? status,
    Iterable<AppointmentsResponse>? appointments,
    ResChipType? selectedType,
  }) {
    return AppointmentsState(
      status: status ?? this.status,
      chipOptions: chipOptions,
      appointments: appointments ?? this.appointments,
      selectedType: selectedType ?? this.selectedType,
    );
  }
}
