import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:medtochka/app/repositories/appointments/appointments_repository.dart';
import 'package:medtochka/app/repositories/appointments/model/appointments_response.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:meta/meta.dart';

part 'appointments_event.dart';
part 'appointments_state.dart';

List<ResChipType> _initChipOptions = [
  ResChipType.appointmentUpcoming,
  ResChipType.appointmentCompleted,
  ResChipType.appointmentCancelled
];

class AppointmentsBloc extends Bloc<AppointmentsEvent, AppointmentsState> {
  final AppointmentsRepository _appointmentsRepository;
  StreamSubscription? searchSubscription;

  AppointmentsBloc(this._appointmentsRepository)
      : super(
          AppointmentsState(
            status: listStatus.LOADING,
            selectedType: _initChipOptions.first,
            chipOptions: _initChipOptions,
          ),
        ) {
    on<AppointmentsFetch>(_appointmentFetchEmitter);

    on<_AppointmentsSetNewList>(_setNewList);

    on<_AppointmentsSetError>(_setError);
  }

  void _setNewList(_AppointmentsSetNewList event, Emitter<AppointmentsState> emit) {
    emit(state.copyWith(appointments: event.appointments, status: listStatus.SUCCESS));
  }

  void _setError(_AppointmentsSetError event, Emitter<AppointmentsState> emit) {
    emit(state.copyWith(status: listStatus.ERROR));
  }

  void _appointmentFetchEmitter(AppointmentsFetch event, Emitter<AppointmentsState> emit) {
    var selectedFilter = event.type ?? state.selectedType;

    emit(state.copyWith(status: listStatus.LOADING, selectedType: selectedFilter));

    searchSubscription?.cancel();

    searchSubscription = _fetchAppointments(selectedFilter).asStream().listen((appointments) {
      add(_AppointmentsSetNewList(appointments));
    })
      ..onError((error, _) => add(_AppointmentsSetError()));
  }

  Future<Iterable<AppointmentsResponse>> _fetchAppointments(ResChipType type) async {
    try {
      return _appointmentsRepository.fetchAppointments(type.key);
    } catch (ex) {
      throw InternalError();
    }
  }

  @override
  Future<void> close() {
    searchSubscription?.cancel();

    return super.close();
  }
}
