part of 'appointments_bloc.dart';

@immutable
abstract class AppointmentsEvent {}

class AppointmentsFetch extends AppointmentsEvent {
  final ResChipType? type;

  AppointmentsFetch({this.type});
}

class _AppointmentsSetNewList extends AppointmentsEvent {
  final Iterable<AppointmentsResponse> appointments;

  _AppointmentsSetNewList(this.appointments);
}

class _AppointmentsSetError extends AppointmentsEvent {}
