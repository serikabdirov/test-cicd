import 'package:bloc/bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:meta/meta.dart';

part 'review_edit_all_event.dart';
part 'review_edit_all_state.dart';

class ReviewEditAllBloc extends Bloc<ReviewEditAllEvent, ReviewEditAllState> {
  final ReviewDetailBloc _reviewDetailBloc;

  ReviewEditAllBloc(this._reviewDetailBloc) : super(ReviewEditAllState()) {
    on<ReviewEditToDocs>((event, emit) {
      emit(state.copyWith(activePage: currentPage.document));
    });

    on<ReviewEditBackToText>((event, emit) {
      emit(state.copyWith(activePage: currentPage.text));
    });
  }
}
