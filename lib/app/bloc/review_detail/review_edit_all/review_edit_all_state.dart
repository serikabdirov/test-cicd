part of 'review_edit_all_bloc.dart';

enum currentPage { text, document }

class ReviewEditAllState {
  final currentPage activePage;

  ReviewEditAllState({this.activePage = currentPage.text});

  ReviewEditAllState copyWith({currentPage? activePage}) {
    return ReviewEditAllState(activePage: activePage ?? this.activePage);
  }
}
