part of 'review_edit_all_bloc.dart';

@immutable
abstract class ReviewEditAllEvent {}

class ReviewEditToDocs extends ReviewEditAllEvent {}

class ReviewEditBackToText extends ReviewEditAllEvent {}
