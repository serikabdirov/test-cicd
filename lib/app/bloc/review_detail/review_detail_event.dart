part of 'review_detail_bloc.dart';

@immutable
abstract class ReviewDetailEvent {}

@immutable
class ReviewDetailFetch extends ReviewDetailEvent {
  final int id;
  final String type;

  ReviewDetailFetch(this.id, this.type);
}

@immutable
class ReviewDetailToggleReview extends ReviewDetailEvent {}

@immutable
class ReviewDetailCompleteFix extends ReviewDetailEvent {}

@immutable
class ReviewDetailSetSuccessMessage extends ReviewDetailEvent {
  final SuccessMessage successMessage;

  ReviewDetailSetSuccessMessage(this.successMessage);
}

@immutable
class ReviewDetailCompleteScoreFix extends ReviewDetailEvent {
  final ReviewChangeScoreRequest payload;

  ReviewDetailCompleteScoreFix(this.payload);
}

@immutable
class ReviewDetailSetLoading extends ReviewDetailEvent {
  final bool isLoading;

  ReviewDetailSetLoading({this.isLoading = false});
}

@immutable
class ReviewDetailToggleReason extends ReviewDetailEvent {
  final int index;

  ReviewDetailToggleReason(this.index);
}
