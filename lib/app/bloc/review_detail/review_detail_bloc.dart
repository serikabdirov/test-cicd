import 'package:bloc/bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_change_score_request.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_detail_response.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:medtochka/app/resources/review_detail/success_message.dart';
import 'package:medtochka/app/resources/reviews/review_status_key.dart';
import 'package:meta/meta.dart';

part 'review_detail_event.dart';
part 'review_detail_state.dart';

class ReviewDetailBloc extends Bloc<ReviewDetailEvent, ReviewDetailState> {
  final ReviewDetailRepository _reviewDetailRepository;

  ReviewDetailBloc(this._reviewDetailRepository)
      : super(ReviewDetailState(successMessage: SuccessMessage.common)) {
    on<ReviewDetailFetch>(_reviewFetch);

    on<ReviewDetailToggleReview>((event, emit) {
      emit(state.copyWith(isReviewOpen: !state.isReviewOpen));
    });

    on<ReviewDetailToggleReason>(_reviewToggleReason);

    on<ReviewDetailSetSuccessMessage>(_setSuccessMessage);

    on<ReviewDetailCompleteFix>(_reviewComplete);

    on<ReviewDetailCompleteScoreFix>(_scoreComplete);

    on<ReviewDetailSetLoading>((event, emit) {
      emit(state.copyWith(isLoading: event.isLoading));
    });
  }

  void _reviewFetch(ReviewDetailFetch event, Emitter<ReviewDetailState> emit) async {
    emit(state.copyWith(status: pageStatus.LOADING));

    var response = await _reviewDetailRepository.fetchReview(event.id, event.type);
    var reasonsCount = response.declineReason?.reasons.length ?? 1;
    var isReasonsOpen = List.filled(reasonsCount, false);
    if (response.rate != null) {
      emit(state.copyWith(
          reviewDetail: response,
          status: pageStatus.SUCCESS,
          isReasonsOpen: isReasonsOpen,
          isReviewOpen: response.rate!.status == ReviewStatusKey.REJECT));
    }
    emit(state.copyWith(
      reviewDetail: response,
      status: pageStatus.SUCCESS,
      isReasonsOpen: isReasonsOpen,
    ));
  }

  void _reviewToggleReason(ReviewDetailToggleReason event, Emitter<ReviewDetailState> emit) async {
    var newReasonState = state.isReasonsOpen;

    newReasonState[event.index] = !newReasonState[event.index];

    emit(state.copyWith(isReasonsOpen: newReasonState));
  }

  void _setSuccessMessage(
      ReviewDetailSetSuccessMessage event, Emitter<ReviewDetailState> emit) async {
    emit(state.copyWith(successMessage: event.successMessage));
  }

  void _reviewComplete(ReviewDetailCompleteFix event, Emitter<ReviewDetailState> emit) async {
    try {
      add(ReviewDetailSetLoading(isLoading: true));

      await _reviewDetailRepository.completeReview(
          state.reviewDetail!.rate!.id!, state.reviewDetail!.about.rateType);

      emit(state.copyWith(status: pageStatus.FIXED));
    } catch (error) {
      emit(state.copyWith(serverError: true));
    } finally {
      add(ReviewDetailSetLoading(isLoading: false));
    }
  }

  void _scoreComplete(ReviewDetailCompleteScoreFix event, Emitter<ReviewDetailState> emit) async {
    try {
      add(ReviewDetailSetLoading(isLoading: true));

      await _reviewDetailRepository.patchScore(
          state.reviewDetail!.rate!.id!, state.reviewDetail!.about.rateType, event.payload);

      emit(state.copyWith(status: pageStatus.FIXED));
    } catch (error) {
      emit(state.copyWith(serverError: true));
    } finally {
      add(ReviewDetailSetLoading(isLoading: false));
    }
  }
}
