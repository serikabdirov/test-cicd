import 'package:medtochka/app/repositories/review_detail/model/review_about.dart';

class PostponedReview {
  final ReviewAbout from;
  final ReviewAbout to;

  PostponedReview(this.from, this.to);
}
