part of 'review_change_score_bloc.dart';

const List<int> RATING_VALUE = [-2, -1, 0, 1, 2];
final List<ScoreType> ratingScore = [
  ScoreType.empty,
  ScoreType.awful,
  ScoreType.bad,
  ScoreType.normal,
  ScoreType.good,
  ScoreType.excellent
];

enum scoreStatus { SUCCESS, ERROR, FIXED }

class ReviewChangeScoreState {
  final Iterable<ReviewScore> currentScoreType;
  final List<int> selectedRating;
  final String rateType;
  final scoreStatus status;
  final bool isLoading;
  final bool isServerError;

  ReviewChangeScoreState(
      {required this.currentScoreType,
      this.selectedRating = const [0, 0, 0, 0, 0],
      this.isLoading = false,
      this.status = scoreStatus.SUCCESS,
      this.isServerError = false,
      required this.rateType});

  ReviewChangeScoreState copyWith(
      {List<int>? selectedRating,
      bool? isLoading,
      scoreStatus? status,
      bool? isServerError}) {
    return ReviewChangeScoreState(
        currentScoreType: currentScoreType,
        selectedRating: selectedRating ?? this.selectedRating,
        isLoading: isLoading ?? this.isLoading,
        status: status ?? this.status,
        isServerError: isServerError ?? false,
        rateType: rateType);
  }

  bool isAllStarSelected() {
    return selectedRating.every((element) => element != 0);
  }
}
