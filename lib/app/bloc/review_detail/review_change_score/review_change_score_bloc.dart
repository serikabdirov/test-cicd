import 'package:bloc/bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_change_score_request.dart';
import 'package:medtochka/app/resources/review_detail/review_score_type.dart';
import 'package:medtochka/app/resources/review_detail/score_type.dart';
import 'package:medtochka/app/resources/review_detail/success_message.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';

part 'review_change_score_event.dart';
part 'review_change_score_state.dart';

class ReviewChangeScoreBloc
    extends Bloc<ReviewChangeScoreEvent, ReviewChangeScoreState> {
  final ReviewDetailBloc _reviewDetailBloc;
  final String type;
  final int id;

  ReviewChangeScoreBloc(this._reviewDetailBloc,
      {required this.type, required this.id})
      : super(ReviewChangeScoreState(
            currentScoreType: ReviewScoreType.getFromString(type: type),
            rateType: type)) {
    on<ReviewChangeScoreSelect>(changeScoreSelect);

    on<ReviewChangeScoreSend>(changeScoreSend);
  }

  void changeScoreSend(
      ReviewChangeScoreSend event, Emitter<ReviewChangeScoreState> emit) async {
    final ReviewChangeScoreRequest payload;
    final selectedRating = state.selectedRating;

    if (type == ReviewTypeKey.LPU) {
      payload = ReviewChangeScoreLpuRequest(
          building: parseScore(selectedRating[0]),
          equipment: parseScore(selectedRating[1]),
          medstaff: parseScore(selectedRating[2]),
          leisure: parseScore(selectedRating[3]),
          food: parseScore(selectedRating[4]));
    } else {
      payload = ReviewChangeScoreDoctorRequest(
          osmotr: parseScore(selectedRating[0]),
          efficiency: parseScore(selectedRating[1]),
          friendliness: parseScore(selectedRating[2]),
          informativity: parseScore(selectedRating[3]),
          recommend: parseScore(selectedRating[4]));
    }
    _reviewDetailBloc.add(ReviewDetailSetSuccessMessage(SuccessMessage.score));
    _reviewDetailBloc.add(ReviewDetailCompleteScoreFix(payload));
  }

  void changeScoreSelect(
      ReviewChangeScoreSelect event, Emitter<ReviewChangeScoreState> emit) {
    var newArray = state.selectedRating.toList();
    newArray[event.index] = event.value;

    emit(state.copyWith(selectedRating: newArray));
  }
}

int parseScore(int scoreIndex) {
  return RATING_VALUE[scoreIndex - 1];
}
