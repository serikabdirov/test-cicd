part of 'review_change_score_bloc.dart';

abstract class ReviewChangeScoreEvent {}

class ReviewChangeScoreSelect extends ReviewChangeScoreEvent {
  final int index;
  final int value;

  ReviewChangeScoreSelect(this.value, this.index);
}

class ReviewChangeScoreSend extends ReviewChangeScoreEvent {}
