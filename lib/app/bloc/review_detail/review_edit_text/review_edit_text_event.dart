part of 'review_edit_text_bloc.dart';

@immutable
abstract class ReviewEditTextEvent {}

@immutable
class ReviewEditTextFetch extends ReviewEditTextEvent {
  ReviewEditTextFetch();
}

@immutable
class ReviewEditTextSend extends ReviewEditTextEvent {
  final bool ignoreBadWords;

  ReviewEditTextSend({this.ignoreBadWords = false});
}

@immutable
class ReviewEditTextSaveDebounce extends ReviewEditTextEvent {}

@immutable
class ReviewEditTextSet extends ReviewEditTextEvent {}

@immutable
class ReviewEditTextReset extends ReviewEditTextEvent {}

@immutable
class ReviewEditTextPositiveComment extends ReviewEditTextEvent {
  final String text;

  ReviewEditTextPositiveComment(this.text);
}

@immutable
class ReviewEditTextNegativeComment extends ReviewEditTextEvent {
  final String text;

  ReviewEditTextNegativeComment(this.text);
}

@immutable
class ReviewEditTextComment extends ReviewEditTextEvent {
  final String text;

  ReviewEditTextComment(this.text);
}

@immutable
class ReviewEditTextSetShowCountAlert extends ReviewEditTextEvent {
  final bool was;

  ReviewEditTextSetShowCountAlert(this.was);
}
