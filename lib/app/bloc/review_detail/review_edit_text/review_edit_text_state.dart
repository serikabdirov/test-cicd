part of 'review_edit_text_bloc.dart';

class ReviewEditTextState extends Equatable {
  final String negative;
  final String positive;
  final String comment;
  final pageStatus status;
  final String type;
  final int? id;
  final bool firstLoad;
  final bool serverError;
  final bool hasBadWords;
  final bool isLoading;
  final bool isDataLoaded;
  final bool isFixed;
  final bool showLettersCount;

  const ReviewEditTextState(
      {required this.type,
      required this.id,
      this.hasBadWords = false,
      this.isDataLoaded = false,
      this.negative = '',
      this.positive = '',
      this.comment = '',
      this.serverError = false,
      this.isFixed = false,
      this.showLettersCount = false,
      this.isLoading = false,
      this.status = pageStatus.LOADING,
      this.firstLoad = false});

  ReviewEditTextState copyWith(
      {String? negative,
      String? positive,
      String? comment,
      pageStatus? status,
      bool? serverError,
      bool? showLettersCount,
      bool? hasBadWords,
      bool? isDataLoaded,
      bool? isFixed,
      bool? isLoading,
      bool? firstLoad}) {
    return ReviewEditTextState(
        negative: negative ?? this.negative,
        positive: positive ?? this.positive,
        comment: comment ?? this.comment,
        status: status ?? this.status,
        firstLoad: firstLoad ?? this.firstLoad,
        serverError: serverError ?? false,
        isFixed: isFixed ?? false,
        isDataLoaded: isDataLoaded ?? false,
        showLettersCount: showLettersCount ?? false,
        hasBadWords: hasBadWords ?? false,
        isLoading: isLoading ?? this.isLoading,
        id: id,
        type: type);
  }

  String? validateField(String? value) {
    if (value == null) {
      return null;
    }

    if (RegExp(r'[A-ZА-Я]{6}').allMatches(value).isNotEmpty) {
      return 'Пожалуйста, не пишите заглавными буквами';
    }

    if (value.length > 5000) {
      return 'Сократите количество символов на ${value.length - 5001}';
    }

    return null;
  }

  int lettersLeft() {
    return ResReviewEditText.MIN_TEXT_LENGTH -
        (positive.trim().length +
            negative.trim().length +
            comment.trim().length);
  }

  bool isBtnDisabled() {
    return lettersLeft() > 0;
  }

  @override
  List<Object?> get props => [
        negative,
        positive,
        comment,
        status,
        type,
        id,
        firstLoad,
        serverError,
        hasBadWords,
        isFixed,
        isLoading,
        isDataLoaded,
        showLettersCount
      ];
}
