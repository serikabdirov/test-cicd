import 'package:bloc/bloc.dart';
import 'package:easy_debounce/easy_debounce.dart';
import 'package:equatable/equatable.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/repositories/review_detail/exeptions/review_bad_words_exception.dart';
import 'package:medtochka/app/repositories/review_detail/model/review_edit_text_request.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/resources/debounce_tag.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:medtochka/app/resources/review_detail/review_edit_text.dart';
import 'package:meta/meta.dart';

part 'review_edit_text_event.dart';
part 'review_edit_text_state.dart';

class ReviewEditTextBloc
    extends Bloc<ReviewEditTextEvent, ReviewEditTextState> {
  final ReviewDetailRepository _reviewDetailRepository;
  final ReviewDetailBloc _reviewDetailBloc;
  final String type;
  final int? id;

  ReviewEditTextBloc(this._reviewDetailRepository, this._reviewDetailBloc,
      {required this.type, required this.id})
      : super(ReviewEditTextState(type: type, id: id)) {
    on<ReviewEditTextFetch>(_onReviewEditTextFetch);

    on<ReviewEditTextSend>(_onReviewEditTextSend);

    on<ReviewEditTextSet>((event, emit) =>
        emit(state.copyWith(firstLoad: true, status: pageStatus.SUCCESS)));

    on<ReviewEditTextReset>((event, emit) => emit(state.copyWith(
        firstLoad: false, status: pageStatus.LOADING, isDataLoaded: false)));

    on<ReviewEditTextComment>(
        (event, emit) => emit(state.copyWith(comment: event.text)));

    on<ReviewEditTextNegativeComment>(
        (event, emit) => emit(state.copyWith(negative: event.text)));

    on<ReviewEditTextPositiveComment>(
        (event, emit) => emit(state.copyWith(positive: event.text)));

    on<ReviewEditTextSetShowCountAlert>((event, emit) => emit(state.copyWith(
        showLettersCount: event.was && state.lettersLeft() > 0)));

    on<ReviewEditTextSaveDebounce>((event, emit) => EasyDebounce.debounce(
            DebounceTag.SAVE_RATE_TEXT, const Duration(seconds: 3), () {
          _onReviewEditTextSave(event, emit);
        }));
  }

  void _onReviewEditTextFetch(
      ReviewEditTextFetch event, Emitter<ReviewEditTextState> emit) async {
    emit(state.copyWith(firstLoad: false, status: pageStatus.LOADING));

    try {
      var comment =
          await _reviewDetailRepository.fetchComment(state.id!, state.type);

      emit(state.copyWith(
          positive: comment.commentPos,
          negative: comment.commentNeg,
          comment: comment.comment));
    } catch (e) {
      emit(state.copyWith(positive: '', negative: '', comment: ''));
    } finally {
      emit(state.copyWith(isDataLoaded: true));
    }
  }

  void _onReviewEditTextSave(event, Emitter<ReviewEditTextState> emit) async {
    final reviewEditTextRequest = ReviewEditTextRequest(
        comment: state.comment,
        commentNeg: state.negative,
        commentPos: state.positive,
        ignoreBadWords: true);

    try {
      await _reviewDetailRepository.sendComment(
          state.id!, state.type, reviewEditTextRequest);
      // ignore: empty_catches
    } catch (e) {}
  }

  void _onReviewEditTextSend(
      ReviewEditTextSend event, Emitter<ReviewEditTextState> emit) async {
    if (state.lettersLeft() > 0) {
      add(ReviewEditTextSetShowCountAlert(true));

      return;
    }

    final reviewEditTextRequest = ReviewEditTextRequest(
        comment: state.comment,
        commentNeg: state.negative,
        commentPos: state.positive,
        ignoreBadWords: event.ignoreBadWords);
    try {
      _reviewDetailBloc.add(ReviewDetailSetLoading(isLoading: true));
      emit(state.copyWith());

      EasyDebounce.cancel(DebounceTag.SAVE_RATE_TEXT);

      await _reviewDetailRepository.sendComment(
          state.id!, state.type, reviewEditTextRequest);

      emit(state.copyWith(isFixed: true));
    } on ReviewBadWordsException {
      emit(state.copyWith(hasBadWords: true));
    } catch (error) {
      emit(state.copyWith(serverError: true));
    } finally {
      _reviewDetailBloc.add(ReviewDetailSetLoading(isLoading: false));
    }
  }
}
