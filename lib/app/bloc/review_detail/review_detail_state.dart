part of 'review_detail_bloc.dart';

class ReviewDetailState {
  final ReviewDetailResponse? reviewDetail;
  final pageStatus status;
  final bool isReviewOpen;
  final bool isLoading;
  final bool serverError;
  final List<bool> isReasonsOpen;
  final SuccessMessage successMessage;

  ReviewDetailState(
      {this.reviewDetail,
      this.status = pageStatus.LOADING,
      this.isReasonsOpen = const [],
      this.isReviewOpen = false,
      this.isLoading = false,
      this.serverError = false,
      required this.successMessage});

  ReviewDetailState copyWith({
    ReviewDetailResponse? reviewDetail,
    pageStatus? status,
    List<bool>? isReasonsOpen,
    SuccessMessage? successMessage,
    bool? isReviewOpen,
    bool? serverError,
    bool? isLoading,
  }) {
    return ReviewDetailState(
        reviewDetail: reviewDetail ?? this.reviewDetail,
        isReviewOpen: isReviewOpen ?? this.isReviewOpen,
        isReasonsOpen: isReasonsOpen ?? this.isReasonsOpen,
        isLoading: isLoading ?? this.isLoading,
        status: status ?? this.status,
        serverError: serverError ?? false,
        successMessage: successMessage ?? this.successMessage);
  }
}
