part of 'review_edit_document_bloc.dart';

@immutable
abstract class ReviewEditDocumentEvent {}

class ReviewEditDocumentFetch extends ReviewEditDocumentEvent {}
