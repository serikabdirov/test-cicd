import 'package:bloc/bloc.dart';
import 'package:medtochka/app/bloc/file_manager/file_manager_bloc.dart';
import 'package:medtochka/app/bloc/file_manager/model/file_model.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/resources/page_status.dart';
import 'package:meta/meta.dart';

part 'review_edit_document_event.dart';
part 'review_edit_document_state.dart';

class ReviewEditDocumentBloc
    extends Bloc<ReviewEditDocumentEvent, ReviewEditDocumentState> {
  final ReviewDetailRepository _reviewDetailRepository;
  final FileManagerBloc _fileManagerBloc;
  final int id;
  final String type;
  final TokenManager tokenManager;

  ReviewEditDocumentBloc(this._reviewDetailRepository, this._fileManagerBloc,
      {required this.id, required this.type, required this.tokenManager})
      : super(const ReviewEditDocumentState()) {
    on<ReviewEditDocumentFetch>((event, emit) async {
      try {
        var response = await _reviewDetailRepository.getFiles(id, type);
        var token = await tokenManager.getMainToken();

        _fileManagerBloc.add(FileManagerSetFile(response.map((file) =>
            NetworkFile(imgS: file.imgS, imgL: file.imgL, id: file.id))));

        emit(
          state.copyWith(status: pageStatus.SUCCESS, token: token),
        );
      } catch (e) {
        emit(
          state.copyWith(status: pageStatus.ERROR),
        );
      }
    });
  }
}
