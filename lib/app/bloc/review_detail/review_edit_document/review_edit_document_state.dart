part of 'review_edit_document_bloc.dart';

class ReviewEditDocumentState {
  final pageStatus status;
  final String? token;
  final bool isLoading;
  final bool isFileLoaded;

  const ReviewEditDocumentState(
      {this.status = pageStatus.LOADING,
      this.isLoading = false,
      this.isFileLoaded = false,
      this.token});

  ReviewEditDocumentState copyWith(
      {pageStatus? status,
      String? token,
      bool? isLoading,
      bool? isFileLoaded}) {
    return ReviewEditDocumentState(
        status: status ?? this.status,
        isLoading: isLoading ?? this.isLoading,
        isFileLoaded: isFileLoaded ?? this.isFileLoaded,
        token: token ?? this.token);
  }

  Map<String, String> getAuthHeader() {
    return ({"Authorization": "Bearer $token"});
  }
}
