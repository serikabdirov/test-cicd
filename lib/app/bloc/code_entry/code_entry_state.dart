part of 'code_entry_bloc.dart';

enum KeyboardStatus { locked, unlocked }

class CodeEntryState extends Equatable {
  final String code;
  final String errorMessage;
  final String infoMessage;
  final int codeLength;
  final int codeEntryDelay = 200;
  final KeyboardStatus status;

  const CodeEntryState(
      {required this.code,
      required this.codeLength,
      this.errorMessage = '',
      this.infoMessage = '',
      this.status = KeyboardStatus.unlocked});

  CodeEntryState copyWith(
      {String? code,
      String? errorMessage,
      String? infoMessage,
      KeyboardStatus? status}) {
    return CodeEntryState(
        code: code ?? this.code,
        codeLength: codeLength,
        errorMessage: errorMessage ?? this.errorMessage,
        infoMessage: infoMessage ?? this.infoMessage,
        status: status ?? this.status);
  }

  int get codeCurrentLength {
    return code.length;
  }

  bool get isKeyboardLocked {
    return status == KeyboardStatus.locked;
  }

  bool get isCodeEntered {
    return codeLength != codeCurrentLength;
  }

  bool get isCodeUnique {
    return code.runes.toSet().isNotEmpty;
  }

  @override
  List<Object?> get props => [code, errorMessage, infoMessage, status];

  @override
  String toString() {
    return 'CodeEntryState{code: $code, errorMessage: $errorMessage, infoMessage: $infoMessage, codeLength: $codeLength, status: $status}';
  }
}
