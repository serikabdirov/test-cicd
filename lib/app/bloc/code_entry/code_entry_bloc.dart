import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'code_entry_event.dart';
part 'code_entry_state.dart';

mixin ConfirmCodeEntryBloc on Bloc<CodeEntryEvent, CodeEntryState> {}

mixin CreateCodeEntryBloc on Bloc<CodeEntryEvent, CodeEntryState> {}

class CodeEntryBloc extends Bloc<CodeEntryEvent, CodeEntryState>
    with ConfirmCodeEntryBloc, CreateCodeEntryBloc {
  final int codeLength;
  final errorDurationSec = 1;

  CodeEntryBloc({this.codeLength = 4}) : super(CodeEntryState(code: '', codeLength: codeLength));

  @override
  Stream<CodeEntryState> mapEventToState(
    CodeEntryEvent event,
  ) async* {
    if (event is CodeEntryError) {
      yield* _mapCodeEntrySetError(event);
    } else if (event is CodeEntryUnlock) {
      yield* _mapCodeEntryUnlock();
    } else if (event is CodeEntryLock) {
      yield* _mapCodeEntryLock();
    } else if (state.isKeyboardLocked) {
      return;
    } else if (event is CodeEntrySetInitial) {
      yield* _mapCodeEntrySetInitial(event);
    } else if (event is CodeEntryTapNumber) {
      yield* _mapCodeEntryTapNumber(event);
    } else if (event is CodeEntryRemoveNumber) {
      yield* _mapCodeEntryRemoveNumber();
    }
  }

  Stream<CodeEntryState> _mapCodeEntryUnlock() async* {
    yield state.copyWith(code: '', status: KeyboardStatus.unlocked);
  }

  Stream<CodeEntryState> _mapCodeEntrySetInitial(CodeEntrySetInitial data) async* {
    yield state.copyWith(code: '', infoMessage: data.infoMessage, errorMessage: '');
  }

  Stream<CodeEntryState> _mapCodeEntryTapNumber(CodeEntryTapNumber data) async* {
    final _code = state.code + data.number;

    if (_code.length > codeLength) {
      return;
    }

    yield state.copyWith(code: _code, errorMessage: '');
  }

  Stream<CodeEntryState> _mapCodeEntryRemoveNumber() async* {
    if (state.code.isEmpty) {
      return;
    }

    final _code = state.code.substring(0, state.codeCurrentLength - 1);

    yield state.copyWith(code: _code, errorMessage: '');
  }

  Stream<CodeEntryState> _mapCodeEntrySetError(CodeEntryError data) async* {
    yield state.copyWith(errorMessage: data.message, status: KeyboardStatus.locked);

    await Future.delayed(Duration(seconds: errorDurationSec));

    add(CodeEntryUnlock());
  }

  Stream<CodeEntryState> _mapCodeEntryLock() async* {
    yield state.copyWith(status: KeyboardStatus.locked);
  }
}
