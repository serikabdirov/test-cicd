part of 'code_entry_bloc.dart';

@immutable
abstract class CodeEntryEvent {}

class CodeEntryTapNumber extends CodeEntryEvent {
  final String number;

  CodeEntryTapNumber(this.number);
}

class CodeEntryCompleteEntry extends CodeEntryEvent {
  final String number;

  CodeEntryCompleteEntry(this.number);
}

class CodeEntrySetInitial extends CodeEntryEvent {
  final String? infoMessage;

  CodeEntrySetInitial({this.infoMessage});
}

class CodeEntryRemoveNumber extends CodeEntryEvent {}

class CodeEntryError extends CodeEntryEvent {
  final String message;

  CodeEntryError(this.message);
}

class CodeEntryExit extends CodeEntryEvent {}

class CodeEntryUnlock extends CodeEntryEvent {}

class CodeEntryLock extends CodeEntryEvent {}
