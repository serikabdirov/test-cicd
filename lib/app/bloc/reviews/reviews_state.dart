part of 'reviews_bloc.dart';

enum listStatus { LOADING, SUCCESS, ERROR }

class ReviewsState extends Equatable {
  final listStatus status;
  final ResChipType selectedType;
  final Iterable<Reviews> reviews;
  final Iterable<ResChipType> chipOptions;

  const ReviewsState({
    required this.status,
    this.reviews = const [],
    required this.selectedType,
    required this.chipOptions,
  });

  ReviewsState copyWith(
      {listStatus? status, Iterable<Reviews>? reviews, ResChipType? selectedType}) {
    return ReviewsState(
      status: status ?? this.status,
      reviews: reviews ?? this.reviews,
      selectedType: selectedType ?? this.selectedType,
      chipOptions: chipOptions,
    );
  }

  @override
  List<Object> get props => [status, selectedType, reviews];
}
