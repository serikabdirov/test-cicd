part of 'reviews_bloc.dart';

abstract class ReviewsEvent {
  const ReviewsEvent();
}

class ReviewsFetch extends ReviewsEvent {
  final ResChipType? type;

  ReviewsFetch({this.type});
}

class _ReviewsSetNewList extends ReviewsEvent {
  final Iterable<Reviews> reviews;

  _ReviewsSetNewList(this.reviews);
}

class ReviewsGoToPd extends ReviewsEvent {}

class _ReviewsSetError extends ReviewsEvent {}
