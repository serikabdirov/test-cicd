import 'package:medtochka/app/resources/reviews/review_status.dart';

abstract class Reviews {}

class ReviewTitle extends Reviews {
  final ResReviewStatus title;

  ReviewTitle(this.title);
}

class ReviewItem extends Reviews {
  final String name;
  final String? dtCreated;
  final String? dtVisit;
  final String? avatar;
  final String? info;
  final int? id;
  final int? doctorId;
  final int? lpuId;
  final int? messageCount;
  final String rateType;
  final String status;

  ReviewItem(
      {required this.name,
      this.info,
      this.dtCreated,
      this.dtVisit,
      this.avatar,
      this.id,
      this.lpuId,
      this.doctorId,
      required this.rateType,
      required this.status,
      this.messageCount});
}
