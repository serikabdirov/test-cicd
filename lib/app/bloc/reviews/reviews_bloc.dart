import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:medtochka/app/bloc/reviews/model/reviews.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/repositories/exceptions/internal_error.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/reviews/model/reviews_category_response.dart';
import 'package:medtochka/app/repositories/reviews/reviews_repository.dart';
import 'package:medtochka/app/resources/base_url.dart';
import 'package:medtochka/app/resources/chip_type.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';

part 'reviews_event.dart';
part 'reviews_state.dart';

Iterable<ResChipType> _initChipOptions = [
  ResChipType.reviewAll,
  ResChipType.reviewDoctor,
  ResChipType.reviewLPU,
  ResChipType.reviewDrug
];

class ReviewsBloc extends Bloc<ReviewsEvent, ReviewsState> {
  final ReviewsRepository _reviewsRepository;
  final UrlLauncherService _urlLauncherService;
  final PrefsRepository _prefsRepository;

  StreamSubscription? searchSubscription;

  ReviewsBloc(
    this._reviewsRepository,
    this._prefsRepository,
    this._urlLauncherService,
  ) : super(
          ReviewsState(
            status: listStatus.LOADING,
            selectedType: _initChipOptions.first,
            chipOptions: _initChipOptions,
          ),
        ) {
    on<ReviewsFetch>(_reviewFetchEmitter);

    on<ReviewsGoToPd>(_goToPd);

    on<_ReviewsSetNewList>(_setNewList);

    on<_ReviewsSetError>(_setError);
  }

  void _reviewFetchEmitter(ReviewsFetch event, Emitter<ReviewsState> emit) {
    var selectedFilter = event.type ?? state.selectedType;

    emit(state.copyWith(status: listStatus.LOADING, selectedType: selectedFilter));

    searchSubscription?.cancel();

    searchSubscription = _reviewFetch(selectedFilter).asStream().listen((reviews) {
      add(_ReviewsSetNewList(reviews));
    })
      ..onError((error, _) => add(_ReviewsSetError()));
  }

  void _setNewList(_ReviewsSetNewList event, Emitter<ReviewsState> emit) {
    emit(state.copyWith(reviews: event.reviews, status: listStatus.SUCCESS));
  }

  void _setError(_ReviewsSetError event, Emitter<ReviewsState> emit) {
    emit(state.copyWith(status: listStatus.ERROR));
  }

  Future<Iterable<Reviews>> _reviewFetch(ResChipType type) async {
    try {
      var reviews = await _reviewsRepository.fetchReviews(type.key);

      return _formatToFlatList(reviews);
    } catch (ex) {
      throw InternalError();
    }
  }

  void _goToPd(ReviewsGoToPd event, Emitter<ReviewsState> emit) async {
    final String selectedTypeKey = state.selectedType.key;
    final String selectedTown = await _prefsRepository.getTown();

    String townUrl(String town, String category) =>
        town.isNotEmpty ? '$selectedTown/$category/' : '';

    if (selectedTypeKey == ReviewTypeKey.ALL) {
      _urlLauncherService.openWeb('${ResBaseUrl.PD}/$selectedTown');

      return;
    }

    if (selectedTypeKey == ReviewTypeKey.DOCTOR) {
      _urlLauncherService.openWeb('${ResBaseUrl.PD}/${townUrl(selectedTown, ReviewTypeKey.VRACH)}');
      return;
    }

    if (selectedTypeKey == ReviewTypeKey.LPU) {
      _urlLauncherService.openWeb('${ResBaseUrl.PD}/${townUrl(selectedTown, ReviewTypeKey.LPU)}');
      return;
    }

    if (selectedTypeKey == ReviewTypeKey.DRUG) {
      _urlLauncherService.openWeb(ResBaseUrl.TABLETKY);
      return;
    }
  }

  Iterable<Reviews> _formatToFlatList(Iterable<ReviewsCategoryResponse>? reviews) {
    var categoryWithReview = reviews!.where((category) => category.reviews.isNotEmpty);

    var mapCategory = categoryWithReview.map<Iterable<Reviews>>((category) => [
          ReviewTitle(category.status),
          ...category.reviews.map((review) => ReviewItem(
              info: review.info,
              name: review.name,
              dtCreated: review.dtCreated,
              dtVisit: review.dtVisit,
              avatar: review.avatar,
              id: review.id,
              status: review.status,
              lpuId: review.lpuIdProdoctorov,
              doctorId: review.rateObjectId,
              rateType: review.rateType))
        ]);

    return mapCategory.expand<Reviews>((i) => i).toList();
  }

  @override
  Future<void> close() {
    searchSubscription?.cancel();

    return super.close();
  }
}
