class ResAppointments {
  const ResAppointments._();

  static const String APP_BAR_TITLE = 'Записи на приём';
  static const String APPOINTMENT_NEAREST = 'Ближайшая';
  static const String APPOINTMENT_REST = 'Остальные';
}
