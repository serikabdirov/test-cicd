class ResMainPage {
  ResMainPage._();

  static const String LABEL_HOME = 'Главная';
  static const String LABEL_APPOINTMENTS = 'Записи';
  static const String LABEL_REVIEWS = 'Отзывы';
  static const String LABEL_FAVORITES = 'Избранное';
}
