class ReviewTypeKey {
  ReviewTypeKey._();

  static const String ALL = '';
  static const String DOCTOR = 'doctor';
  static const String LPU = 'lpu';
  static const String DRUG = 'drug';
  static const String VRACH = 'vrach';
  static const String HOSPITAL = 'hospital';
}
