class ReviewStatusKey {
  ReviewStatusKey._();

  static const String REJECT = 'reject';
  static const String ON_MOD = 'on_mod';
  static const String ACCEPT = 'accept';
  static const String CORRECT = 'correct';
  static const String WAITING = 'waiting';
}
