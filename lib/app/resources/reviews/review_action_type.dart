class ResReviewsActionType {
  ResReviewsActionType._();

  static const String DISCUSSION = 'support_message';
  static const String PUBLISH = 'publish_agreement';
}
