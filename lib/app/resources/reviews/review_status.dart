import 'package:medtochka/app/resources/reviews/review_status_key.dart';

class ResReviewStatus {
  final String status;
  final String name;

  ResReviewStatus._({required this.status, required this.name});

  static ResReviewStatus correct = ResReviewStatus._(
      status: ReviewStatusKey.CORRECT, name: 'Ждут исправления');

  static ResReviewStatus onMod =
      ResReviewStatus._(status: ReviewStatusKey.ON_MOD, name: 'На модерации');

  static ResReviewStatus accept =
      ResReviewStatus._(status: ReviewStatusKey.ACCEPT, name: 'Опубликованные');

  static ResReviewStatus reject = ResReviewStatus._(
      status: ReviewStatusKey.REJECT, name: 'Сняты с публикации');
  static ResReviewStatus waiting = ResReviewStatus._(
      status: ReviewStatusKey.WAITING, name: 'Ожидают вашего отзыва');
}
