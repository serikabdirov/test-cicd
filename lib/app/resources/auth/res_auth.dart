class ResAuth {
  ResAuth._();

  static const String HINT_PHONE = 'Номер телефона';
  static const String HINT_CODE = 'Код из СМС';
  static const String TEXT_COUNT_DOWN_TIMER = 'Отправить новый код можно через';
  static const String RESEND_SMS = 'Отправить СМС повторно';
  static const String CHANGE_PHONE = 'Изменить номер';
  static const String TEXT_PRIVACY_POLICY =
      'Входя в приложение, вы даёте согласие на';
  static const String SEND_SMS = 'Отправить СМС с кодом';

  static const String ICON_SUPPORT = 'assets/icons/icon_support.svg';
}
