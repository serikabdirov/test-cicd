class ResAlerts {
  final String cancelText;
  final String? confirmText;
  final String title;
  final String description;

  ResAlerts._(
      {required this.cancelText,
      this.confirmText,
      required this.title,
      required this.description});

  static ResAlerts serverError = ResAlerts._(
      cancelText: 'понятно',
      title: 'Что-то пошло не так',
      description: 'Попробуйте немного подождать и обновить страницу.');

  static ResAlerts maxImageSize = ResAlerts._(
      cancelText: 'понятно',
      title: 'Слишком большой файл',
      description: 'Загрузите файл размером до 6 МБ.');

  static ResAlerts minImageSize = ResAlerts._(
      cancelText: 'понятно',
      title: 'Слишком маленькое фото',
      description: 'Загрузите фото размером не менее, чем 300x300 пикселей');

  static ResAlerts invalidFormat = ResAlerts._(
      cancelText: 'понятно',
      title: 'Формат не поддерживается',
      description: 'Файл должен быть в формате JPEG или PNG');

  static ResAlerts reachedLimit = ResAlerts._(
      cancelText: 'понятно',
      title: 'Максимум 15 файлов',
      description:
          'Вы выбрали слишком много файлов, мы загрузим только первые 15');
}
