class ResDateForm {
  final String one;
  final String two;
  final String many;

  ResDateForm._({required this.one, required this.two, required this.many});

  static ResDateForm dayDeclension =
      ResDateForm._(one: 'день', two: 'дня', many: 'дней');
  static ResDateForm weekDeclension =
      ResDateForm._(one: 'неделю', two: 'недели', many: 'недель');
  static ResDateForm monthDeclension =
      ResDateForm._(one: 'месяц', two: 'месяца', many: 'месяцев');
  static ResDateForm yearDeclension =
      ResDateForm._(one: 'год', two: 'года', many: 'лет');
}
