class ResFavourites {
  ResFavourites._();

  static const String APP_BAR_TITLE = 'Избранное';
  static const String SNACK_BAR_TEXT_REMOVE = 'Удалено из избранного';
  static const String SNACK_BAR_TEXT_CANCELLED = 'Восстановлено';
  static const String SNACK_BAR_TEXT_REMOVE_ERROR =
      'Не удалось удалить из избранного. Повторите позже';
  static const String SNACK_BAR_TEXT_ADD_ERROR =
      'Не удалось добавить в избранное. Повторите позже';
}
