class ResTourSlider {
  ResTourSlider._();

  static const APPOINTMENT_TITLE = 'Показывайте врачу электронную медкарту';
  static const APPOINTMENT_DESCRIPTION =
      'На приём не нужно брать папку с бумагами. Врач посмотрит вашу медкарту на своём компьютере';
  static const APPOINTMENT_IMAGE = 'assets/images/mascots/mascot_med_card.svg';

  static const MED_CARD_TITLE = 'Управляйте записями на приём';
  static const MED_CARD_DESCRIPTION =
      'Связывайтесь с клиникой, добавляйте напоминание в календарь, смотрите детали записи';
  static const MED_CARD_IMAGE = 'assets/images/mascots/mascot_appointment.svg';
}
