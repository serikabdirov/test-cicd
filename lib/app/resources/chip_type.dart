import 'package:medtochka/app/resources/favourites/favourite_type_key.dart';
import 'package:medtochka/app/resources/reviews/review_type_key.dart';

import 'info_message.dart';

class ResChipType {
  final String text;
  final String key;
  final ResInfoMessage emptyMessage;

  ResChipType._(
      {required this.text, required this.key, required this.emptyMessage});

  static ResChipType appointmentUpcoming = ResChipType._(
      text: 'Будущие',
      key: 'upcoming',
      emptyMessage: ResInfoMessage.appointmentUpcoming);

  static ResChipType appointmentCompleted = ResChipType._(
      text: 'Прошедшие',
      key: 'completed',
      emptyMessage: ResInfoMessage.appointmentCompleted);

  static ResChipType appointmentCancelled = ResChipType._(
      text: 'Отменённые',
      key: 'cancelled',
      emptyMessage: ResInfoMessage.appointmentCancelled);

  static ResChipType reviewAll = ResChipType._(
      text: 'Все',
      key: ReviewTypeKey.ALL,
      emptyMessage: ResInfoMessage.reviewAll);

  static ResChipType reviewDoctor = ResChipType._(
      text: 'Врачи',
      key: ReviewTypeKey.DOCTOR,
      emptyMessage: ResInfoMessage.reviewDoctor);

  static ResChipType reviewLPU = ResChipType._(
      text: 'Клиники',
      key: ReviewTypeKey.LPU,
      emptyMessage: ResInfoMessage.reviewLpu);

  static ResChipType reviewDrug = ResChipType._(
      text: 'Лекарства',
      key: ReviewTypeKey.DRUG,
      emptyMessage: ResInfoMessage.reviewDrug);

  static ResChipType favouriteAll = ResChipType._(
      text: 'Все',
      key: FavouriteTypeKey.ALL,
      emptyMessage: ResInfoMessage.favouritesAll);

  static ResChipType favouriteDoctors = ResChipType._(
      text: 'Врачи',
      key: FavouriteTypeKey.DOCTORS,
      emptyMessage: ResInfoMessage.favouritesDoctor);

  static ResChipType favouriteLpu = ResChipType._(
      text: 'Клиники',
      key: FavouriteTypeKey.LPU,
      emptyMessage: ResInfoMessage.favouritesLpu);
}
