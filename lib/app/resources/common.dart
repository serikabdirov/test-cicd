class ResCommon {
  ResCommon._();
  static const String TEXT_CONTINUE = 'продолжить';
  static const String TEXT_ENTER = 'войти';
  static const String TEXT_EXIT = 'выход';
  static const String TEXT_CHAT = 'Чат';
  static const String TEXT_CANCEL = 'Отменить';
}
