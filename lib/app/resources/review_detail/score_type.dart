import 'dart:ui';

import 'package:medtochka/app/resources/colors.dart';

class ScoreType {
  final Color primaryColor;
  final Color secondaryColor;
  final Color textColor;
  final String type;
  final String text;
  final String subText;

  ScoreType._(this.primaryColor, this.secondaryColor, this.textColor, this.type,
      this.text, this.subText);

  static ScoreType awful = ScoreType._(ResColors.SECONDARY, ResColors.BG_ERROR,
      ResColors.WHITE, ResReviewTypeKey.AWFUL, 'ужасно', 'никогда');
  static ScoreType bad = ScoreType._(ResColors.SECONDARY, ResColors.BG_ERROR,
      ResColors.WHITE, ResReviewTypeKey.BAD, 'плохо', 'нет');
  static ScoreType normal = ScoreType._(ResColors.WARNING, ResColors.BG_WARNING,
      ResColors.WHITE, ResReviewTypeKey.NORMAL, 'нормально', 'возможно');
  static ScoreType good = ScoreType._(ResColors.SUCCESS, ResColors.BG_SUCCESS,
      ResColors.WHITE, ResReviewTypeKey.GOOD, 'хорошо', 'да');
  static ScoreType excellent = ScoreType._(
      ResColors.SUCCESS,
      ResColors.BG_SUCCESS,
      ResColors.WHITE,
      ResReviewTypeKey.EXCELLENT,
      'отлично',
      'обязательно');
  static ScoreType empty = ScoreType._(ResColors.DISABLED, ResColors.BG_GRAY_80,
      ResColors.WHITE, ResReviewTypeKey.EMPTY, 'нет оценки', 'нет оценки');
  static ScoreType disabled = ScoreType._(ResColors.DISABLED,
      ResColors.BG_GRAY_80, ResColors.WHITE, ResReviewTypeKey.DISABLED, '', '');
}

class ResReviewTypeKey {
  ResReviewTypeKey._();

  static const AWFUL = 'awful';
  static const BAD = 'bad';
  static const NORMAL = 'normal';
  static const GOOD = 'good';
  static const EXCELLENT = 'excellent';
  static const EMPTY = 'empty';
  static const DISABLED = 'disabled';
}
