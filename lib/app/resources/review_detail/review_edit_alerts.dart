class ResReviewEditAlerts {
  final String cancelText;
  final String? confirmText;
  final String title;
  final String description;

  ResReviewEditAlerts._(
      {required this.cancelText,
      this.confirmText,
      required this.title,
      required this.description});

  static ResReviewEditAlerts badWords = ResReviewEditAlerts._(
      cancelText: 'отменить',
      confirmText: 'отправить так',
      title: 'В вашем отзыве нецензурные выражения',
      description:
          'Если вы не замените их на корректные, мы не сможем опубликовать отзыв.');
}
