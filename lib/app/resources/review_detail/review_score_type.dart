import 'package:medtochka/app/resources/reviews/review_type_key.dart';

class ReviewScoreType {
  static final Iterable<ReviewScore> _lpuRating = [
    ReviewScore('building', 'Тщательность обследования',
        'Свежесть ремонта здания, помещений пребывания, легкость ориентации в здании, удобство перемещения, наличие и работоспособность лифтов, место для встречи с родственниками, дополнительные услуги: аптека, кафетерий, почта, прокат оборудования'),
    ReviewScore('equipment', 'Оборудование и медикаменты',
        'Наличие диагностического оборудования, его современность и доступность обследования на нем для пациента, наличие назначенных врачом медикаментов'),
    ReviewScore('medstaff', 'Отношение медперсонала',
        'Вежливость, тактичность, отзывчивость медперсонала, доступность доктора для общения с пациентом и его родственниками, отношение медперсонала к просьбам пациента'),
  ];

  static Iterable<ReviewScore> doctor = [
    ReviewScore('osmotr', 'Тщательность обследования'),
    ReviewScore('efficiency', 'Эффективность лечения'),
    ReviewScore('friendliness', 'Отношение к пациенту'),
    ReviewScore('informativity', 'Информирование пациента'),
    ReviewScore('recommend', 'Посоветуете ли Вы врача?'),
  ];

  static Iterable<ReviewScore> lpu = [
    ..._lpuRating,
    ReviewScore('leisure', 'Комфорт пребывания',
        'Гардероб, туалет, места для сидения, наличие информационных буклетов, проспектов, плакатов, телевизора в зоне ожидания'),
    ReviewScore('food', 'Время ожидания',
        'Электронная очередь, запись по талонам, живая очередь'),
  ];

  static Iterable<ReviewScore> hospital = [
    ..._lpuRating,
    ReviewScore('leisure', 'Комфорт пребывания',
        'Телевизор в палате, в коридоре отделения, в холле больницы, холодильник в палате, в столовой, кнопки вызова медперсонала в палате, содержание прибольничной территории (газоны, деревья, лавочки, урны)'),
    ReviewScore('food', 'Качество питания',
        'Своевременность раздачи пищи,чистота в столовой,разноообразие блюд (обратите внимание, что питание в больнице является диетическим и зависит от заболевания)'),
  ];

  static Iterable<ReviewScore> getFromString(
      {required String type, bool isHospital = false}) {
    if (isHospital) {
      return hospital;
    }

    if (type == ReviewTypeKey.LPU) {
      return lpu;
    }

    return doctor;
  }
}

class ReviewScore {
  final String key;
  final String title;
  final String? detail;

  ReviewScore(this.key, this.title, [this.detail]);
}
