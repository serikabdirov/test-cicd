class ReviewEditType {
  ReviewEditType._();

  static const String TEXT = 'change_text';
  static const String DOCUMENT = 'document';
  static const String RATE = 'change_rate';
  static const String AUTHORIZE = 'authorize';
  static const String CHANGE_SCORE = 'change_scores';
  static const String CONFIRM_DOCUMENTS = 'confirm_documents';
  static const String DEFAULT = 'default';
}
