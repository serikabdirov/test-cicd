class SuccessMessage {
  final String title;
  final String bodyText;

  SuccessMessage._({required this.title, required this.bodyText});

  static SuccessMessage common = SuccessMessage._(
    title: 'Отправлено!',
    bodyText: 'Проверим и опубликуем отзыв в течение дня',
  );
  static SuccessMessage score = SuccessMessage._(
    title: 'Оценки опубликованы',
    bodyText: 'Спасибо',
  );
}
