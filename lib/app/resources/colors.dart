import 'dart:ui';

class ResColors {
  ResColors._();

  @deprecated
  static const Color BLACK = Color(0xFF111111);
  @deprecated
  static const Color WHITE = Color(0xFFFFFFFF);
  @deprecated
  static const Color DEEP_GREY = Color(0xFF757575);
  @deprecated
  static const Color MEDIUM_GREY = Color(0xFFCCCCCC);
  @deprecated
  static const Color LIGHT_GREY = Color(0xfff7f7f7);
  @deprecated
  static const Color PRIMARY_BLUE = Color(0xFF117DC1);
  @deprecated
  static const Color LIGHT_BLUE = Color(0xFFE5F2F9);
  @deprecated
  static const Color LIGHTEST_BLUE = Color(0xFFF2F8FC);
  @deprecated
  static const Color PRIMARY_RED = Color(0xFFEC2327);
  @deprecated
  static const Color DARK_YELLOW = Color(0xFFEF933E);
  @deprecated
  static const Color LIGHT_YELLOW = Color(0xFFFFF2D9);

  static const Color WARNING = Color(0xFFEF933E);
  static const Color BG_WARNING = Color(0xFFFFF2D9);

  static const Color TEXT = Color(0xFF111111);
  static const Color TEXT_SECONDARY = Color(0xFF4E4E4E);
  static const Color TEXT_INFO = Color(0xFF757575);

  static const Color ICON_PRIMARY = Color(0xFF111111);
  static const Color ICON_SECONDARY = Color(0xFF757575);

  static const Color SUCCESS = Color(0xFF33B576);
  static const Color BG_SUCCESS = Color(0xFFE0F6EE);

  static const Color SECONDARY = Color(0xFFEC2327);
  static const Color BG_SECONDARY = Color(0xFFF2F8FC);

  static const Color BG_ERROR = Color(0xFFFCE8E8);

  static const Color BG_GRAY_80 = Color(0xFFF7F7F7);
  static const Color BG_GRAY_60 = Color(0xFFDCDCDC);
  static const Color BG_GRAY_40 = Color(0xFFF7F7F7);
  static const Color BG_GRAY_0 = Color(0xFFFFFFFF);

  static const Color DISABLED = Color(0xFF949494);
}
