class DocumentLink {
  final String link;

  DocumentLink._(this.link);

  static DocumentLink privacyPolicy = DocumentLink._('privacy-policy');
  static DocumentLink termsOfUse = DocumentLink._('terms-of-use');

  String url(String domain) {
    return '$domain$link';
  }
}
