class ResSupport {
  final String email;
  final String phone;
  final String watsApp;

  ResSupport._(this.email, this.phone, this.watsApp);

  static final ResSupport changeText =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
  static final ResSupport document =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
  static final ResSupport changeRate =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
  static final ResSupport authorize =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
  static final ResSupport changeScores =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
  static final ResSupport confirmDocuments =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79181511703');
  static final ResSupport app = ResSupport._('', '78007073129', '');
  static final ResSupport common =
      ResSupport._('proverka@prodoctorov.ru', '78006003028', '79892101704');
}
