class ResErrors {
  ResErrors._();

  static const INTERNAL = 'Ошибка сервера';
  static const NO_INTERNET_CONNECTION = 'Нет соединения с интернетом';
}
