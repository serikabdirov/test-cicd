// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/colors.dart';

class ResTextTheme {
  static const double _FONT_SIZE_XS = 10;
  static const double _FONT_SIZE_SM = 12;
  static const double _FONT_SIZE_MD = 14;
  static const double _FONT_SIZE_LG = 16;
  static const double _FONT_SIZE_XL = 20;
  static const double _FONT_SIZE_2XL = 24;
  static const double _FONT_SIZE_3XL = 34;
  static const double _FONT_SIZE_4XL = 40;
  static const double _FONT_SIZE_5XL = 60;
  static const double _FONT_SIZE_6XL = 96;

  static const double _LINE_HEIGHT_XS = 1.15;
  static const double _LINE_HEIGHT_SM = 1.2;
  static const double _LINE_HEIGHT_MD = 1.25;
  static const double _LINE_HEIGHT_LG = 1.3;
  static const double _LINE_HEIGHT_XL = 1.35;
  static const double _LINE_HEIGHT_2XL = 1.4;
  static const double _LINE_HEIGHT_3XL = 1.5;
  static const double _LINE_HEIGHT_4XL = 1.6;

  static const double _LETTER_SPACING_2XS = -1.5;
  static const double _LETTER_SPACING_XS = -0.5;
  static const double _LETTER_SPACING_BASE = 0;
  static const double _LETTER_SPACING_MD = 0.1;
  static const double _LETTER_SPACING_LG = 0.15;
  static const double _LETTER_SPACING_XL = 0.25;
  static const double _LETTER_SPACING_2XL = 0.4;
  static const double _LETTER_SPACING_3XL = 0.5;
  static const double _LETTER_SPACING_4XL = 1.25;
  static const double _LETTER_SPACING_5XL = 1.5;

  ResTextTheme._();

  static const TextStyle headline1 = TextStyle(
    fontSize: _FONT_SIZE_6XL,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_SM,
    letterSpacing: _LETTER_SPACING_XS,
    color: ResColors.TEXT,
  );

  static const TextStyle headline2 = TextStyle(
    fontSize: _FONT_SIZE_5XL,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_SM,
    letterSpacing: _LETTER_SPACING_2XS,
    color: ResColors.TEXT,
  );

  static const TextStyle headline3 = TextStyle(
    fontSize: _FONT_SIZE_4XL,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_LG,
    letterSpacing: _LETTER_SPACING_BASE,
    color: ResColors.TEXT,
  );

  static const TextStyle headline4 = TextStyle(
    fontSize: _FONT_SIZE_3XL,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_LG,
    letterSpacing: _LETTER_SPACING_XL,
    color: ResColors.TEXT,
  );

  static const TextStyle headline5 = TextStyle(
    fontSize: _FONT_SIZE_2XL,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_XL,
    letterSpacing: _LETTER_SPACING_BASE,
    color: ResColors.TEXT,
  );

  static const TextStyle headline6 = TextStyle(
    fontSize: _FONT_SIZE_XL,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_LG,
    letterSpacing: _LETTER_SPACING_LG,
    color: ResColors.TEXT,
  );

  static const TextStyle subtitle1 = TextStyle(
    fontSize: _FONT_SIZE_LG,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_XL,
    letterSpacing: _LETTER_SPACING_LG,
    color: ResColors.TEXT,
  );

  static const TextStyle body1 = TextStyle(
    fontSize: _FONT_SIZE_LG,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_MD,
    letterSpacing: _LETTER_SPACING_3XL,
    color: ResColors.TEXT,
  );

  static const TextStyle subtitle2 = TextStyle(
    fontSize: _FONT_SIZE_MD,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_2XL,
    letterSpacing: _LETTER_SPACING_MD,
    color: ResColors.TEXT,
  );

  static const TextStyle body2 = TextStyle(
    fontSize: _FONT_SIZE_MD,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_2XL,
    letterSpacing: _LETTER_SPACING_XL,
    color: ResColors.TEXT,
  );

  static const TextStyle caption = TextStyle(
    fontSize: _FONT_SIZE_SM,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_LG,
    letterSpacing: _LETTER_SPACING_2XL,
    color: ResColors.TEXT,
  );

  static const TextStyle overline = TextStyle(
    fontSize: _FONT_SIZE_XS,
    fontWeight: FontWeight.normal,
    fontStyle: FontStyle.normal,
    height: _LINE_HEIGHT_4XL,
    letterSpacing: _LETTER_SPACING_2XL,
    color: ResColors.TEXT,
  );
}
