import 'package:flutter/widgets.dart';
import 'package:medtochka/app/resources/colors.dart';

class ResButtonStyle {
  final Color text;
  final Color background;

  ResButtonStyle._({required this.text, required this.background});

  static ResButtonStyle primary = ResButtonStyle._(
      text: ResColors.WHITE, background: ResColors.PRIMARY_BLUE);
  static ResButtonStyle secondary = ResButtonStyle._(
      text: ResColors.PRIMARY_BLUE, background: ResColors.LIGHTEST_BLUE);
}
