class AppointmentStatus {
  const AppointmentStatus._();

  static const String UPCOMING = 'upcoming';
  static const String COMPLETED = 'completed';
  static const String CANCELLED = 'cancelled';
  static const String ABSENCE = 'absence';
}
