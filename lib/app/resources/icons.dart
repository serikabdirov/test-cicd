class ResIcons {
  ResIcons._();
  // TODO: больше не используем, пользуемся иконочными шрифтами
  static const String ICON_BACKSPACE = 'assets/icons/icon_backspace.svg';
  static const String ICON_ARROW_BACK = 'assets/icons/icon_arrow_back.svg';

// TODO: иконки, которые имеют несколько цветов и их нельзя занести в иконочный шрифт

  static const String ICON_MAIL = 'assets/icons/mail.svg';
  static const String ICON_MOBILE_PHONE = 'assets/icons/mobile_phone.svg';
  static const String ICON_WHATSAPP = 'assets/icons/whatsapp.svg';
}
