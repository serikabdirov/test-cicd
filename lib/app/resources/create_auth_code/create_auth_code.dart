class ResCreateAuthCode {
  ResCreateAuthCode._();

  static const String SIMPLE_CODE_ERROR = 'Код не должен состоять из 4 одинаковых цифр';
  static const String DIFFERENT_CODE_ERROR = 'Коды не совпадают';

  static const String CODE_INFO = 'Нужен для быстрого входа';

  static const String CREATE_CODE_TITLE = 'Придумайте код доступа';
  static const String CONFIRM_CODE_TITLE = 'Повторите код доступа';
}
