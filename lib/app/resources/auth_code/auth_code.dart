class ResAuthCode {
  ResAuthCode._();

  static String attemptsLeftMessage(int count) =>
      'Неправильный код. Осталось $count попыток';

  static const String CODE_TITLE = 'Введите код доступа';
}
