class ResAuthCodeAlerts {
  final String cancelText;
  final String confirmText;
  final String title;
  final String description;

  ResAuthCodeAlerts._(
      {required this.cancelText,
      required this.confirmText,
      required this.title,
      required this.description});

  static ResAuthCodeAlerts errorLimit = ResAuthCodeAlerts._(
      cancelText: 'поддержка',
      confirmText: 'выйти',
      title: 'Вы ввели неправильный код 10 раз',
      description:
          'Выйдите, чтобы сбросить код и установить новый. Или обратитесь в поддержку.');

  static ResAuthCodeAlerts exit = ResAuthCodeAlerts._(
      cancelText: 'отменить',
      confirmText: 'Выйти',
      title: 'Выйти из профиля?',
      description: 'Код сбросится и нужно будет заново авторизоваться');
}
