import 'package:flutter_dotenv/flutter_dotenv.dart';

class ResBaseUrl {
  ResBaseUrl._();

  /// dev flavor
  static final String MAIN = dotenv.get('MT');
  static final String INFO = dotenv.get('MT_INFO');
  static final String PROFILE = dotenv.get('MT_PROFILE');
  static final String LANDING = dotenv.get('MT_LANDING');
  static final String PD = dotenv.get('PD');
  static final String TABLETKY = dotenv.get('TABLETKY');

  /// stage flavor
  // static const String MAIN = 'https://app-medtochka.prodoctorov.com';
  // static const String INFO = 'https://medtochka-info.prodoctorov.com/';
  // static const String PROFILE = 'https://medtochka-profile.prodoctorov.com/';
  // static const String PD = 'https://stage.prodoctorov.com';
  // static const String LANDING = 'https://medtochka.prodoctorov.com/';
  // static const String TABLETKY = 'https://pt-stage.prodoctorov.com/';

  /// prod flavor
  // static const String MAIN = 'https://app.medtochka.ru';
  // static const String INFO = 'https://info.medtochka.ru/';
  // static const String PROFILE = 'https://profile.medtochka.ru/';
  // static const String PD = 'https://prodoctorov.ru';
  // static const String LANDING = 'https://medtochka.ru/';
  // static const String TABLETKY = 'https://protabletky.ru/';
}
