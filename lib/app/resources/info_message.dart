import 'package:medtochka/app/resources/images.dart';

class ResInfoMessage {
  final String avatarSrc;
  final String title;
  final String? subtitle;
  final String? buttonText;

  ResInfoMessage._({
    required this.avatarSrc,
    required this.title,
    this.subtitle,
    this.buttonText,
  });

  static ResInfoMessage appointmentUpcoming = ResInfoMessage._(
    title: 'У вас нет онлайн-записей на предстоящие приёмы',
    subtitle: 'Выберите подходящего врача и запишитесь к нему прямо на сайте',
    avatarSrc: ResImages.MASCOT_APPOINTMENT,
  );

  static ResInfoMessage appointmentCompleted = ResInfoMessage._(
    title: 'Прошедших записей пока нет',
    avatarSrc: ResImages.MASCOT_APPOINTMENT,
  );

  static ResInfoMessage appointmentCancelled = ResInfoMessage._(
    title: 'Вы не отменяли записи',
    avatarSrc: ResImages.MASCOT_APPOINTMENT,
  );

  static ResInfoMessage reviewAll = ResInfoMessage._(
    title: 'Здесь будут ваши отзывы',
    subtitle: 'Делитесь впечатлениями о врачах, клиниках и лекарствах',
    buttonText: 'оставить отзыв',
    avatarSrc: ResImages.MASCOT_REVIEWS_ALL,
  );

  static ResInfoMessage reviewDoctor = ResInfoMessage._(
    title: 'Здесь будут ваши отзывы о врачах',
    subtitle: 'Делитесь впечатлениями, чтобы помочь с выбором другим пациентам',
    buttonText: 'оставить отзыв',
    avatarSrc: ResImages.MASCOT_REVIEWS_DOCTOR,
  );

  static ResInfoMessage reviewLpu = ResInfoMessage._(
    title: 'Здесь будут ваши отзывы о клиниках',
    subtitle: 'Делитесь впечатлениями, чтобы помочь с выбором другим пациентам',
    buttonText: 'оставить отзыв',
    avatarSrc: ResImages.MASCOT_REVIEWS_LPU,
  );

  static ResInfoMessage reviewDrug = ResInfoMessage._(
    title: 'Здесь будут ваши отзывы о лекарствах',
    subtitle: 'Делитесь впечатлениями, чтобы помочь с выбором другим пациентам',
    buttonText: 'оставить отзыв',
    avatarSrc: ResImages.MASCOT_REVIEWS_DRUG,
  );

  static ResInfoMessage errorInternal = ResInfoMessage._(
    title: 'Что-то пошло не так',
    subtitle: 'Попробуйте немного подождать и обновить страницу',
    buttonText: 'Обновить',
    avatarSrc: ResImages.INTERNAL_ERROR,
  );

  static ResInfoMessage favouritesAll = ResInfoMessage._(
    title: 'Здесь будут избранные врачи и клиники',
    subtitle: 'Нажимайте сердечки, чтобы сохранять тех, кто вам понравился',
    avatarSrc: ResImages.MASCOT_FAVOURITES,
  );

  static ResInfoMessage favouritesDoctor = ResInfoMessage._(
    title: 'Здесь будут избранные врачи',
    subtitle: 'Нажимайте сердечки, чтобы сохранять понравившихся врачей',
    avatarSrc: ResImages.MASCOT_FAVOURITES,
  );

  static ResInfoMessage favouritesLpu = ResInfoMessage._(
    title: 'Здесь будут избранные клиники',
    subtitle: 'Нажимайте сердечки, чтобы сохранять понравившиеся клиники',
    avatarSrc: ResImages.MASCOT_FAVOURITES,
  );

  static ResInfoMessage appointmentCancelSuccess = ResInfoMessage._(
    title: 'Запись отменена',
    subtitle:
        'Записаться к этому или другому врачу на удобное время можно в личном кабинете — МедТочке',
    buttonText: 'хорошо',
    avatarSrc: ResImages.CHECK,
  );

  static ResInfoMessage appointmentCancelError = ResInfoMessage._(
    title: 'Что-то пошло не так',
    subtitle:
        'Не получили подтверждение от клиники. Чтобы отменить запись, позвоните в клинику напрямую',
    buttonText: 'понятно',
    avatarSrc: ResImages.X_MARK,
  );
}
