class ResImages {
  ResImages._();

  static const String LOGO_MEDTOCHKA = 'assets/images/logo/medtochka.svg';
  static const String LOGO_MEDLOCK = 'assets/images/logo/medlock.svg';
  static const String LOGO_MEDOTVET = 'assets/images/logo/medotvet.svg';
  static const String LOGO_PROBOLEZNY = 'assets/images/logo/probolezny.svg';
  static const String LOGO_PRODOCTOROV = 'assets/images/logo/prodoctorov.svg';
  static const String LOGO_PROTABLETKY = 'assets/images/logo/protabletky.svg';

  static const String MASCOT_APPOINTMENT =
      'assets/images/mascots/mascot_appointment.svg';
  static const String MASCOT_REVIEWS_ALL =
      'assets/images/mascots/mascot_reviews_all.svg';
  static const String MASCOT_REVIEWS_DOCTOR =
      'assets/images/mascots/mascot_reviews_doctor.svg';
  static const String MASCOT_REVIEWS_LPU =
      'assets/images/mascots/mascot_reviews_lpu.svg';
  static const String MASCOT_REVIEWS_DRUG =
      'assets/images/mascots/mascot_reviews_drug.svg';
  static const String MASCOT_FAVOURITES =
      'assets/images/mascots/mascot_favourites.svg';
  static const String MASCOT_ARMCHAIR =
      'assets/images/mascots/mascot_armchair.svg';

  static const String EMPTY_AVATAR_DOCTOR =
      'assets/images/empty_avatar_doctor.svg';
  static const String EMPTY_AVATAR_LPU = 'assets/images/empty_avatar_lpu.svg';
  static const String EMPTY_AVATAR_DRUG = 'assets/images/empty_avatar_drug.svg';

  static const String INTERNAL_ERROR =
      'assets/images/mascots/error_internal.svg';
  static const String CHECK = 'assets/images/check_img.svg';
  static const String X_MARK = 'assets/images/x_mark_img.svg';
}
