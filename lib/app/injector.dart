import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/data/network/interceptors/interceptor_info_client.dart';
import 'package:medtochka/app/data/network/interceptors/interceptor_main_client.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/repositories/user_info/user_info.dart';
import 'package:medtochka/app/repositories/user_info/user_info_impl.dart';
import 'package:medtochka/app/resources/base_url.dart';

import 'data/network/interceptors/interceptor_pd_client.dart';
import 'data/network/interceptors/interceptor_profile_client.dart';

class DependenciesInitializer {
  static const String MAIN_CLIENT_TAG = 'mainClient';
  static const String PROFILE_CLIENT_TAG = 'profileClient';
  static const String INFO_CLIENT_TAG = 'infoClient';
  static const String PD_CLIENT_TAG = 'pdClient';

  static Future<void> initializeDependencies() async {
    Dio _createMainClient() {
      final client = Dio(BaseOptions(baseUrl: '${ResBaseUrl.MAIN}/'));
      final tokenManager = TokenManager(client);
      client.interceptors.clear();
      client.interceptors.add(InterceptorMainClient(tokenManager, client));
      client.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));
      return client;
    }

    // intl
    initializeDateFormatting();
    Intl.defaultLocale = 'ru';

    // Url launcher service
    UrlLauncherService _urlLauncherService = UrlLauncherService();

    // Prefs
    PrefsRepository _prefsRepository = PrefsRepository();

    // network
    Dio _mainClient = _createMainClient();
    UserInfoRepository _userInfoRepository = UserInfoRepositoryImpl(_mainClient);
    TokenManager _tokenManager = TokenManager(_mainClient);

    Dio _createInfoClient(TokenManager tokenManager) {
      final client = Dio(BaseOptions(baseUrl: ResBaseUrl.INFO));

      client.interceptors.clear();
      client.interceptors.add(InterceptorInfoClient(tokenManager, client));
      client.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));

      return client;
    }

    Dio _createProfileClient(TokenManager tokenManager) {
      final client = Dio(BaseOptions(baseUrl: ResBaseUrl.PROFILE));

      client.interceptors.clear();
      client.interceptors.add(InterceptorProfileClient(tokenManager, client));
      client.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));

      return client;
    }

    Dio _createPdClient(TokenManager tokenManager) {
      final client = Dio(BaseOptions(baseUrl: '${ResBaseUrl.PD}/'));

      client.interceptors.clear();
      client.interceptors.add(InterceptorPdClient(tokenManager, client));
      client.interceptors.add(LogInterceptor(requestBody: true, responseBody: true));

      return client;
    }

    Get.put(_mainClient, tag: MAIN_CLIENT_TAG);
    Get.put(_createProfileClient(_tokenManager), tag: PROFILE_CLIENT_TAG);
    Get.put(_createPdClient(_tokenManager), tag: PD_CLIENT_TAG);
    Get.put(_createInfoClient(_tokenManager), tag: INFO_CLIENT_TAG);
    Get.put(_tokenManager);
    Get.put(_tokenManager);

    Get.put(_urlLauncherService);

    Get.put(_prefsRepository);

    Get.put(_userInfoRepository);
  }
}
