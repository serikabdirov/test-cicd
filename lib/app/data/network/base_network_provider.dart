import 'package:dio/dio.dart';

abstract class BaseNetworkProvider<P, R> {
  static const _BASE_URL = 'https://medtochka-test14.prodoctorov.com/';

  late Dio _dio;

  BaseNetworkProvider() {
    final baseOptions = BaseOptions(baseUrl: _BASE_URL);
    _dio = Dio(baseOptions);
    _dio.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
  }

  Future<R> execute({P? params});

  Options? _getOptions(String? token) {
    final headers = {'Authorization': token};
    Options? options;
    if (token != null) {
      options = Options(headers: headers);
    }
    return options;
  }

  Future<Response?> post(String path, body,
      {String? token, Map<String, dynamic>? queryParams}) async {
    final response = await _dio.post(path,
        data: body, options: _getOptions(token), queryParameters: queryParams);
    return response;
  }

  Future<Response> get(String path,
      {String? token, Map<String, dynamic>? queryParams}) async {
    final response = await _dio.get(path,
        queryParameters: queryParams, options: _getOptions(token));
    return response;
  }

  Future<Response> delete(String path,
      {String? token, body, Map<String, dynamic>? queryParams}) async {
    final response = await _dio.delete(path,
        data: body, options: _getOptions(token), queryParameters: queryParams);
    return response;
  }

  Future<Response> patch(String path,
      {String? token, body, Map<String, dynamic>? queryParams}) async {
    final response = await _dio.patch(path,
        data: body, options: _getOptions(token), queryParameters: queryParams);
    return response;
  }

  // Future<AuthResponse> confirm() async {
  //   final request = ConfirmCodeRequest('79094515463', '1234');
  //   final response = await _dio?.post(
  //       'api/profile/phone_login/',
  //       data: request.toJson()
  //   );
  //   if (response?.statusCode == 200) {
  //     final result = response?.data;
  //     return AuthResponse.fromJson(result);
  //   } else {
  //     throw Exception("Error");
  //   }
  // }

}
