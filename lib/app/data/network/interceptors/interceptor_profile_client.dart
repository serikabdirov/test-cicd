import 'package:dio/dio.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';

class InterceptorProfileClient extends InterceptorsWrapper {
  static const String _AUTHORIZATION = "Authorization";
  static const String _TOKEN = "Token";

  final TokenManager _tokenManager;
  final Dio _dio;

  var _countRetryRefreshToken = 0;

  InterceptorProfileClient(this._tokenManager, this._dio);

  @override
  void onRequest(options, handler) async {
    final token = await _tokenManager.getProfileToken();
    if (token != null && token.isNotEmpty) {
      options.headers[_AUTHORIZATION] = "$_TOKEN $token";
    }
    handler.next(options);
  }

  @override
  // ignore: avoid_renaming_method_parameters
  void onError(error, handler) async {
    try {
      if (!(error.response?.statusCode == 401 &&
          _countRetryRefreshToken <= 3)) {
        throw error;
      }

      // ignore: avoid_print
      print('*** Refresh profile token ***');
      _countRetryRefreshToken++;

      await _tokenManager.refreshProfileToken();

      handler.resolve(await _retry(error.requestOptions));
    } catch (e) {
      _countRetryRefreshToken = 0;

      handler.next(error);
    }
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final token = await _tokenManager.getMainToken();
    final options = Options(
      method: requestOptions.method,
      headers: {_AUTHORIZATION: "$_TOKEN $token"},
    );
    return _dio.request<dynamic>(requestOptions.path,
        data: requestOptions.data,
        queryParameters: requestOptions.queryParameters,
        options: options);
  }
}
