import 'package:dio/dio.dart';
import 'package:medtochka/app/data/network/base_network_provider.dart';
import 'package:medtochka/app/repositories/reviews/model/reviews_category_request.dart';
import 'package:medtochka/app/repositories/reviews/model/reviews_category_response.dart';

class ReviewsProvider extends BaseNetworkProvider<ReviewsCategoryRequest,
    Iterable<ReviewsCategoryResponse>?> {
  static const String _PATH = 'api/rates/';

  @override
  Future<Iterable<ReviewsCategoryResponse>?> execute(
      {ReviewsCategoryRequest? params}) async {
    try {
      final response = await get(_PATH,
          queryParams: params?.toJson(),
          token: 'Bearer mbd9bNSHboJt9VezdPvNMgcchuEMvk');

      if (response.statusCode == 200) {
        final reviewsCategoryResponse = response.data
            .map<ReviewsCategoryResponse>(
                (category) => ReviewsCategoryResponse.fromJson(category));

        return reviewsCategoryResponse as Iterable<ReviewsCategoryResponse>;
      }
    } on DioError catch (ex) {
      throw Exception(ex.message);
    }
  }
}
