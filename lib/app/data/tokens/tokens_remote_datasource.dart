import 'package:dio/dio.dart';
import 'package:medtochka/app/data/tokens/model/token.dart';

class TokensRemoteDatasource {
  static const String _PATH_INFO_TOKEN =
      "api/tokens/medtochka_info/profile_token/";
  static const String _PATH_PROFILE_TOKEN =
      "api/tokens/medtochka_profile/profile_token/";
  // ignore: unused_field
  static const String _PATH_PD_TOKEN = "api/tokens/pd_access_token/";
  static const String _PATH_MAIN_REFRESH_TOKEN = "api/o/token/";

  static const String _HEADER_AUTHORIZATION = "Authorization";
  static const String _PARAM_REFRESH_TOKEN = "refresh_token";
  static const String _PARAM_MOBILE_APP = "mobile_app";
  static const String _PARAM_GRANT_TYPE = "grant_type";
  static const String _PARAM_CLIENT_ID = "client_id";

  final Dio _mainNetworkClient;

  TokensRemoteDatasource(this._mainNetworkClient);

  Future<Token?> mainRefreshToken(String refreshToken) async {
    const grantType = _PARAM_REFRESH_TOKEN;
    const clientId = _PARAM_MOBILE_APP;
    var formData = FormData.fromMap({
      _PARAM_GRANT_TYPE: grantType,
      _PARAM_REFRESH_TOKEN: refreshToken,
      _PARAM_CLIENT_ID: clientId
    });
    try {
      var response = await _mainNetworkClient.post(_PATH_MAIN_REFRESH_TOKEN,
          data: formData);
      if (response.statusCode == 200) {
        return Token.fromJson(response.data);
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  Future<Token?> getInfoToken(String mainAccessToken) async {
    try {
      var response = await _mainNetworkClient.post(_PATH_INFO_TOKEN,
          options: Options(headers: {_HEADER_AUTHORIZATION: mainAccessToken}));
      if (response.statusCode == 201) {
        return Token.fromJson(response.data);
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  Future<Token?> getProfileToken(String mainAccessToken) async {
    try {
      var response = await _mainNetworkClient.post(_PATH_PROFILE_TOKEN,
          options: Options(headers: {_HEADER_AUTHORIZATION: mainAccessToken}));
      if (response.statusCode == 201) {
        return Token.fromJson(response.data);
      }
      // ignore: empty_catches
    } catch (error) {}
  }
}
