import 'package:intl/intl.dart';
import 'package:medtochka/app/data/tokens/model/token.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokensLocalDatasource {
  static const String _TOKEN_TYPE = "tokenType";
  static const String _TYPE_BEARER = "Bearer";
  static const String _FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

  static const String _MAIN_ACCESS_TOKEN = "mainAccessToken";
  static const String _MAIN_REFRESH_TOKEN = "mainRefreshToken";
  static const String _MAIN_EXPIRATION_DATE = "mainExpirationDate";

  static const String _INFO_ACCESS_TOKEN = "infoAccessToken";
  static const String _INFO_EXPIRATION_DATE = "infoExpirationDate";

  static const String _PROFILE_ACCESS_TOKEN = "profileAccessToken";
  static const String _PROFILE_EXPIRATION_DATE = "profileExpirationDate";

  // ignore: unused_field
  static const String _PD_ACCESS_TOKEN = "pdAccessToken";
  // ignore: unused_field
  static const String _PD_DATE_CREATE = "pdDateCreate";
  // ignore: unused_field
  static const String _PD_EXPIRES_IN = "pdExpiresIn";

  Future<void> setMainToken(Token token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_MAIN_ACCESS_TOKEN, token.accessToken ?? "");
    prefs.setString(_MAIN_REFRESH_TOKEN, token.refreshToken ?? "");
    prefs.setString(_TOKEN_TYPE, token.typeToken ?? _TYPE_BEARER);
    prefs.setString(
        _MAIN_EXPIRATION_DATE, _convertDateTimeToString(token.expirationDate));
  }

  Future<Token?> getMainToken() async {
    final prefs = await SharedPreferences.getInstance();
    final accessToken = prefs.getString(_MAIN_ACCESS_TOKEN);
    final refreshToken = prefs.getString(_MAIN_REFRESH_TOKEN);
    if (accessToken != null &&
        accessToken.isNotEmpty &&
        refreshToken != null &&
        refreshToken.isNotEmpty) {
      return Token(
          accessToken: accessToken,
          refreshToken: refreshToken,
          typeToken: prefs.getString(_TOKEN_TYPE) ?? _TYPE_BEARER,
          expirationDate: _convertStringToDateTime(
                  prefs.getString(_MAIN_EXPIRATION_DATE) ?? "") ??
              DateTime.now());
    } else {
      return null;
    }
  }

  Future<void> setInfoAccessToken(Token token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_INFO_ACCESS_TOKEN, token.profileToken ?? "");
    prefs.setString(
        _INFO_EXPIRATION_DATE, _convertDateTimeToString(token.expirationDate));
  }

  Future<Token?> getInfoAccessToken() async {
    final prefs = await SharedPreferences.getInstance();
    final profileToken = prefs.getString(_INFO_ACCESS_TOKEN);
    if (profileToken != null && profileToken.isNotEmpty) {
      return Token(
          accessToken: prefs.getString(_MAIN_ACCESS_TOKEN),
          typeToken: prefs.getString(_TOKEN_TYPE) ?? _TYPE_BEARER,
          profileToken: profileToken,
          expirationDate: _convertStringToDateTime(
                  prefs.getString(_INFO_EXPIRATION_DATE) ?? "") ??
              DateTime.now());
    } else {
      return null;
    }
  }

  Future<void> setProfileAccessToken(Token token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(_PROFILE_ACCESS_TOKEN, token.profileToken ?? "");
    prefs.setString(_PROFILE_EXPIRATION_DATE,
        _convertDateTimeToString(token.expirationDate));
  }

  Future<Token?> getProfileAccessToken() async {
    final prefs = await SharedPreferences.getInstance();
    final profileToken = prefs.getString(_PROFILE_ACCESS_TOKEN);
    if (profileToken != null && profileToken.isNotEmpty) {
      return Token(
          accessToken: prefs.getString(_MAIN_ACCESS_TOKEN),
          typeToken: prefs.getString(_TOKEN_TYPE) ?? _TYPE_BEARER,
          profileToken: profileToken,
          expirationDate: _convertStringToDateTime(
                  prefs.getString(_PROFILE_EXPIRATION_DATE) ?? "") ??
              DateTime.now());
    } else {
      return null;
    }
  }

  String _convertDateTimeToString(DateTime dateTime) {
    final formatter = DateFormat(_FORMAT_DATE_TIME);
    return formatter.format(dateTime);
  }

  DateTime? _convertStringToDateTime(String string) {
    if (string.isEmpty) {
      return null;
    }
    return DateTime.parse(string);
  }
}
