class Token {
  String? accessToken;
  String? refreshToken;
  String? typeToken;
  String? profileToken;
  int? expiresIn;
  DateTime expirationDate;

  Token(
      {this.accessToken,
      this.refreshToken,
      this.typeToken,
      this.profileToken,
      this.expiresIn,
      required this.expirationDate});

  static DateTime getExpirationDate(int value) {
    return DateTime.now().add(Duration(seconds: value));
  }

  static Token fromJson(Map<String, dynamic> json) {
    return Token(
        accessToken: json['access_token'],
        refreshToken: json['refresh_token'],
        typeToken: json['token_type'],
        profileToken: json['profile_token'],
        expiresIn: json['expires_in'],
        expirationDate: getExpirationDate(json['expires_in']));
  }

  bool isValid() {
    final dateTimeNow = DateTime.now();
    return dateTimeNow.isBefore(expirationDate);
  }
}
