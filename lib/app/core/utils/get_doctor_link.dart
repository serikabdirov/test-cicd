import 'package:medtochka/app/resources/base_url.dart';

String getDoctorLink({
  required String doctorLink,
  int? lpuId,
}) {
  var hash = '';

  if (lpuId != null) {
    hash += lpuId.toString();
  }

  return '${ResBaseUrl.PD}$doctorLink#$hash';
}
