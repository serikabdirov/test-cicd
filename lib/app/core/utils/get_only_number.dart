String getOnlyNumber(String val) => val.replaceAll(RegExp(r'[^0-9]+'), '');
