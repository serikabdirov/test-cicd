import 'package:medtochka/app/core/utils/get_only_number.dart';

formatPhoneNumber(String value) {
  final phone = getOnlyNumber(value);
  final re = RegExp(r'^(\d)(\d{3})(\d{3})(\d{2})(\d{2})');

  final phoneGroup = re.allMatches(phone).elementAt(0);

  return '+7 (${phoneGroup.group(2)}) ${phoneGroup.group(3)}-${phoneGroup.group(4)}-${phoneGroup.group(5)}';
}
