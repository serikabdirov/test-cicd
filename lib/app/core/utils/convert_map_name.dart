import 'package:map_launcher/map_launcher.dart';

String convertMapName(AvailableMap map) {
  switch (map.mapType) {
    case MapType.googleGo:
      return 'Google Карты';
    case MapType.yandexMaps:
      return 'Яндекс Карты';
    case MapType.yandexNavi:
      return 'Яндекс Навигатор';
    case MapType.doubleGis:
      return '2GIS';
    case MapType.apple:
      return 'Apple Карты';
    default:
      return map.mapName;
  }
}
