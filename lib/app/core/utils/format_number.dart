String formatNumber(int value) {
  RegExp regExp = RegExp(
    r"\B(?=(\d{3})+(?!\d))",
    caseSensitive: false,
    multiLine: false,
  );

  return value.toString().replaceAll(regExp, ' ');
}
