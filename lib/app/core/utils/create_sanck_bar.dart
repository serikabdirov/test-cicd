import 'package:flutter/material.dart';

SnackBar createSnackBar(BuildContext context, String message,
    {SnackBarAction? snackBarAction}) {
  return SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.floating,
      action: snackBarAction);
}
