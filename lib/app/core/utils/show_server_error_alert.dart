import 'package:flutter/material.dart';
import 'package:medtochka/app/resources/alerts.dart';
import 'package:medtochka/app/resources/colors.dart';

void showServerErrorAlert(BuildContext context, ResAlerts dialogData) {
  showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => AlertDialog(
      title: Text(dialogData.title),
      content: Text(dialogData.description),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text(
            dialogData.cancelText.toUpperCase(),
            style: const TextStyle(color: ResColors.PRIMARY_BLUE),
          ),
        ),
      ],
    ),
  );
}
