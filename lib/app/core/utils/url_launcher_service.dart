import 'package:medtochka/app/core/utils/get_only_number.dart';
import 'package:url_launcher/url_launcher.dart';

class UrlLauncherService {
  Future<void> call(String phone) async {
    final url = 'tel:+${getOnlyNumber(phone)}';
    await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
  }

  Future<void> openWeb(String link) async {
    await canLaunch(link)
        ? await launch(link, forceSafariVC: false, forceWebView: false)
        : throw 'Could not launch $link';
  }

  Future<void> openMailto(String link) async {
    final url = 'mailto:$link';

    await canLaunch(url) ? await launch(url) : throw 'Could not launch $link';
  }

  Future<void> openWhatsAppIOS(String phone) async {
    var url = "https://wa.me/$phone";
    const String storeUrl =
        'https://apps.apple.com/ru/app/whatsapp-messenger/id310633997';

    _openWhatsApp(url, storeUrl);
  }

  Future<void> openWhatsAppAndroid(String phone) async {
    var url = "whatsapp://send?phone=$phone";

    const String storeUrl =
        'https://play.google.com/store/apps/details?id=com.whatsapp&hl=ru&gl=US ';

    _openWhatsApp(url, storeUrl);
  }

  Future<void> _openWhatsApp(String link, String storeLink) async {
    try {
      if (await canLaunch(link)) {
        await launch(link, forceSafariVC: false, forceWebView: false);

        return;
      }

      throw 'Could not launch $link';
    } catch (e) {
      await launch(storeLink, forceSafariVC: false, forceWebView: false);
    }
  }
}
