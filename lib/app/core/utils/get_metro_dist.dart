String getMetroDist(double dist) {
  if (dist >= 1) {
    return '${dist.toStringAsFixed(1)} км';
  }

  final distInMeter = dist * 1000;

  return '${distInMeter.round()} м';
}
