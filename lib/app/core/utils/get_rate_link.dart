import 'package:medtochka/app/resources/base_url.dart';

String getRateLink({
  required String type,
  required int doctorId,
  required int? lpuId,
  required String dateVisit,
}) {
  const String RATE_ANCHOR = 'rate-form';

  var query = '?visitDate=$dateVisit';

  if (lpuId != null) {
    query += '&lpu=$lpuId';
  }

  return '${ResBaseUrl.PD}/new/rate/$type/$doctorId/$query#$RATE_ANCHOR';
}
