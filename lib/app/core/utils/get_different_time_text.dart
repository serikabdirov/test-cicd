import 'package:intl/intl.dart';
import 'package:medtochka/app/resources/date_form.dart';

String? getDifferentTimeText(DateTime date) {
  var nowDay = DateTime.now();
  var differentInDays = nowDay.difference(date).inDays;

  if (differentInDays < 0) {
    return null;
  }

  if (differentInDays == 0) {
    return 'менее дня назад';
  }

  if (differentInDays < 7) {
    return '$differentInDays ${pluralize(differentInDays, ResDateForm.dayDeclension)} назад';
  }

  var weekDifferent = differenceInCalendarWeeks(differentInDays);

  if (weekDifferent < 4) {
    return '$weekDifferent ${pluralize(weekDifferent, ResDateForm.weekDeclension)} назад';
  }

  var monthDifferent = differenceInCalendarMonths(differentInDays);

  if (monthDifferent < 12) {
    return '$monthDifferent ${pluralize(monthDifferent, ResDateForm.monthDeclension)} назад';
  }

  var yearDifferent = differenceInCalendarYears(differentInDays);

  if (yearDifferent > 0) {
    return '$yearDifferent ${pluralize(yearDifferent, ResDateForm.yearDeclension)} назад';
  }

  return '';
}

String pluralize(int count, ResDateForm words) {
  return Intl.plural(
    count,
    one: words.one,
    two: words.two,
    many: words.many,
    other: words.two,
  );
}

int differenceInCalendarWeeks(int days) {
  const int dayInWeek = 7;

  return days ~/ dayInWeek;
}

int differenceInCalendarMonths(int days) {
  const int dayInMonth = 30;

  return days ~/ dayInMonth;
}

int differenceInCalendarYears(int days) {
  const int dayInYears = 365;

  return days ~/ dayInYears;
}
