import 'package:auto_route/auto_route.dart';
import 'package:medtochka/app/core/router/router_path.dart';
import 'package:medtochka/app/ui/appointment_detail/appointment_detail_page.dart';
import 'package:medtochka/app/ui/appointments/appointments_page.dart';
import 'package:medtochka/app/ui/auth/auth_page.dart';
import 'package:medtochka/app/ui/auth_code/auth_code_page.dart';
import 'package:medtochka/app/ui/create_auth_code/create_auth_code_page.dart';
import 'package:medtochka/app/ui/favourites/favourites_page.dart';
import 'package:medtochka/app/ui/home/home_page.dart';
import 'package:medtochka/app/ui/main_page.dart';
import 'package:medtochka/app/ui/review_detail/review_detail_page.dart';
import 'package:medtochka/app/ui/reviews/reviews_page.dart';
import 'package:medtochka/app/ui/splash/splash_page.dart';
import 'package:medtochka/app/ui/tour/tour_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(path: RouterPath.SPLASH, page: SplashPage),
    AutoRoute(path: RouterPath.LOGIN, page: AuthPage),
    AutoRoute(path: RouterPath.TOUR, page: TourPage),
    AutoRoute(path: RouterPath.CREATE_CODE, page: CreateAuthCodePage),
    AutoRoute(path: RouterPath.CONFIRM_CODE, page: AuthCodePage),
    AutoRoute(path: RouterPath.MAIN_PAGE, page: MainPage, children: [
      AutoRoute(path: RouterPath.REVIEWS, page: ReviewsPage),
      AutoRoute(path: RouterPath.HOME, page: HomePage),
      AutoRoute(
          path: RouterPath.APPOINTMENTS,
          name: 'Appointments',
          page: EmptyRouterPage,
          children: [
            AutoRoute(path: '', page: AppointmentsPage),
            RedirectRoute(path: '*', redirectTo: '/'),
          ]),
      AutoRoute(path: RouterPath.FAVOURITES, page: FavouritesPage),
      RedirectRoute(path: '*', redirectTo: '/'),
    ]),
    AutoRoute(path: RouterPath.APPOINTMENT_DETAIL, page: AppointmentDetailPage),
    AutoRoute(path: RouterPath.REVIEWS_DETAIL, page: ReviewDetailPage),
    RedirectRoute(path: '*', redirectTo: '/'),
  ],
)
class $AppRouter {}
