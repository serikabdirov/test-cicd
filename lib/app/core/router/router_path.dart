class RouterPath {
  static const String LOGIN = '/spalsh';

  static const String TOUR = '/tour';

  static const String SPLASH = '/splash';

  static const String MAIN_PAGE = '/';

  static const String HOME = 'home';

  static const String FAVOURITES = 'favourites';

  static const String REVIEWS = 'reviews';
  static const String REVIEWS_DETAIL = '/reviews/:type/:id';

  static const String APPOINTMENTS = 'appointments';
  static const String APPOINTMENT_DETAIL = '/appointments/:id';

  static const String CREATE_CODE = '/createCode';
  static const String CONFIRM_CODE = '/confirmCode';
}
