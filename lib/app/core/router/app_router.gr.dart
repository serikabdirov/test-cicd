// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i11;
import 'package:flutter/material.dart' as _i14;

import '../../ui/appointment_detail/appointment_detail_page.dart' as _i7;
import '../../ui/appointments/appointments_page.dart' as _i13;
import '../../ui/auth/auth_page.dart' as _i2;
import '../../ui/auth_code/auth_code_page.dart' as _i5;
import '../../ui/create_auth_code/create_auth_code_page.dart' as _i4;
import '../../ui/favourites/favourites_page.dart' as _i12;
import '../../ui/home/home_page.dart' as _i10;
import '../../ui/main_page.dart' as _i6;
import '../../ui/review_detail/review_detail_page.dart' as _i8;
import '../../ui/reviews/reviews_page.dart' as _i9;
import '../../ui/splash/splash_page.dart' as _i1;
import '../../ui/tour/tour_page.dart' as _i3;

class AppRouter extends _i11.RootStackRouter {
  AppRouter([_i14.GlobalKey<_i14.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i11.PageFactory> pagesMap = {
    SplashRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i1.SplashPage());
    },
    AuthRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i2.AuthPage());
    },
    TourRoute.name: (routeData) {
      final args =
          routeData.argsAs<TourRouteArgs>(orElse: () => const TourRouteArgs());
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i3.TourPage(key: args.key));
    },
    CreateAuthCodeRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.CreateAuthCodePage());
    },
    AuthCodeRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.AuthCodePage());
    },
    MainRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i6.MainPage());
    },
    AppointmentDetailRoute.name: (routeData) {
      final pathParams = routeData.pathParams;
      final args = routeData.argsAs<AppointmentDetailRouteArgs>(
          orElse: () =>
              AppointmentDetailRouteArgs(id: pathParams.getInt('id')));
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i7.AppointmentDetailPage(key: args.key, id: args.id));
    },
    ReviewDetailRoute.name: (routeData) {
      final pathParams = routeData.pathParams;
      final args = routeData.argsAs<ReviewDetailRouteArgs>(
          orElse: () => ReviewDetailRouteArgs(
              id: pathParams.getInt('id'), type: pathParams.getString('type')));
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i8.ReviewDetailPage(
              key: args.key, id: args.id, type: args.type));
    },
    ReviewsRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i9.ReviewsPage());
    },
    HomeRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i10.HomePage());
    },
    Appointments.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i11.EmptyRouterPage());
    },
    FavouritesRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i12.FavouritesPage());
    },
    AppointmentsRoute.name: (routeData) {
      return _i11.MaterialPageX<dynamic>(
          routeData: routeData, child: _i13.AppointmentsPage());
    }
  };

  @override
  List<_i11.RouteConfig> get routes => [
        _i11.RouteConfig(SplashRoute.name, path: '/splash'),
        _i11.RouteConfig(AuthRoute.name, path: '/spalsh'),
        _i11.RouteConfig(TourRoute.name, path: '/tour'),
        _i11.RouteConfig(CreateAuthCodeRoute.name, path: '/createCode'),
        _i11.RouteConfig(AuthCodeRoute.name, path: '/confirmCode'),
        _i11.RouteConfig(MainRoute.name, path: '/', children: [
          _i11.RouteConfig(ReviewsRoute.name,
              path: 'reviews', parent: MainRoute.name),
          _i11.RouteConfig(HomeRoute.name,
              path: 'home', parent: MainRoute.name),
          _i11.RouteConfig(Appointments.name,
              path: 'appointments',
              parent: MainRoute.name,
              children: [
                _i11.RouteConfig(AppointmentsRoute.name,
                    path: '', parent: Appointments.name),
                _i11.RouteConfig('*#redirect',
                    path: '*',
                    parent: Appointments.name,
                    redirectTo: '/',
                    fullMatch: true)
              ]),
          _i11.RouteConfig(FavouritesRoute.name,
              path: 'favourites', parent: MainRoute.name),
          _i11.RouteConfig('*#redirect',
              path: '*',
              parent: MainRoute.name,
              redirectTo: '/',
              fullMatch: true)
        ]),
        _i11.RouteConfig(AppointmentDetailRoute.name,
            path: '/appointments/:id'),
        _i11.RouteConfig(ReviewDetailRoute.name, path: '/reviews/:type/:id'),
        _i11.RouteConfig('*#redirect',
            path: '*', redirectTo: '/', fullMatch: true)
      ];
}

/// generated route for [_i1.SplashPage]
class SplashRoute extends _i11.PageRouteInfo<void> {
  const SplashRoute() : super(name, path: '/splash');

  static const String name = 'SplashRoute';
}

/// generated route for [_i2.AuthPage]
class AuthRoute extends _i11.PageRouteInfo<void> {
  const AuthRoute() : super(name, path: '/spalsh');

  static const String name = 'AuthRoute';
}

/// generated route for [_i3.TourPage]
class TourRoute extends _i11.PageRouteInfo<TourRouteArgs> {
  TourRoute({_i14.Key? key})
      : super(name, path: '/tour', args: TourRouteArgs(key: key));

  static const String name = 'TourRoute';
}

class TourRouteArgs {
  const TourRouteArgs({this.key});

  final _i14.Key? key;
}

/// generated route for [_i4.CreateAuthCodePage]
class CreateAuthCodeRoute extends _i11.PageRouteInfo<void> {
  const CreateAuthCodeRoute() : super(name, path: '/createCode');

  static const String name = 'CreateAuthCodeRoute';
}

/// generated route for [_i5.AuthCodePage]
class AuthCodeRoute extends _i11.PageRouteInfo<void> {
  const AuthCodeRoute() : super(name, path: '/confirmCode');

  static const String name = 'AuthCodeRoute';
}

/// generated route for [_i6.MainPage]
class MainRoute extends _i11.PageRouteInfo<void> {
  const MainRoute({List<_i11.PageRouteInfo>? children})
      : super(name, path: '/', initialChildren: children);

  static const String name = 'MainRoute';
}

/// generated route for [_i7.AppointmentDetailPage]
class AppointmentDetailRoute
    extends _i11.PageRouteInfo<AppointmentDetailRouteArgs> {
  AppointmentDetailRoute({_i14.Key? key, required int id})
      : super(name,
            path: '/appointments/:id',
            args: AppointmentDetailRouteArgs(key: key, id: id),
            rawPathParams: {'id': id});

  static const String name = 'AppointmentDetailRoute';
}

class AppointmentDetailRouteArgs {
  const AppointmentDetailRouteArgs({this.key, required this.id});

  final _i14.Key? key;

  final int id;
}

/// generated route for [_i8.ReviewDetailPage]
class ReviewDetailRoute extends _i11.PageRouteInfo<ReviewDetailRouteArgs> {
  ReviewDetailRoute({_i14.Key? key, required int id, required String type})
      : super(name,
            path: '/reviews/:type/:id',
            args: ReviewDetailRouteArgs(key: key, id: id, type: type),
            rawPathParams: {'id': id, 'type': type});

  static const String name = 'ReviewDetailRoute';
}

class ReviewDetailRouteArgs {
  const ReviewDetailRouteArgs({this.key, required this.id, required this.type});

  final _i14.Key? key;

  final int id;

  final String type;
}

/// generated route for [_i9.ReviewsPage]
class ReviewsRoute extends _i11.PageRouteInfo<void> {
  const ReviewsRoute() : super(name, path: 'reviews');

  static const String name = 'ReviewsRoute';
}

/// generated route for [_i10.HomePage]
class HomeRoute extends _i11.PageRouteInfo<void> {
  const HomeRoute() : super(name, path: 'home');

  static const String name = 'HomeRoute';
}

/// generated route for [_i11.EmptyRouterPage]
class Appointments extends _i11.PageRouteInfo<void> {
  const Appointments({List<_i11.PageRouteInfo>? children})
      : super(name, path: 'appointments', initialChildren: children);

  static const String name = 'Appointments';
}

/// generated route for [_i12.FavouritesPage]
class FavouritesRoute extends _i11.PageRouteInfo<void> {
  const FavouritesRoute() : super(name, path: 'favourites');

  static const String name = 'FavouritesRoute';
}

/// generated route for [_i13.AppointmentsPage]
class AppointmentsRoute extends _i11.PageRouteInfo<void> {
  const AppointmentsRoute() : super(name, path: '');

  static const String name = 'AppointmentsRoute';
}
