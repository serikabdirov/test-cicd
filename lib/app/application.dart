import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:medtochka/app/bloc/appointment_detail/appointment_detail_bloc.dart';
import 'package:medtochka/app/bloc/appointments/appointments_bloc.dart';
import 'package:medtochka/app/bloc/auth/auth_bloc.dart';
import 'package:medtochka/app/bloc/code_entry/code_entry_bloc.dart';
import 'package:medtochka/app/bloc/favourites/favourites_bloc.dart';
import 'package:medtochka/app/bloc/review_detail/review_detail_bloc.dart';
import 'package:medtochka/app/bloc/reviews/reviews_bloc.dart';
import 'package:medtochka/app/bloc/slider/slider_cubit.dart';
import 'package:medtochka/app/bloc/splash/splash_bloc.dart';
import 'package:medtochka/app/bloc/splash/splash_events.dart';
import 'package:medtochka/app/bloc/tour/tour_bloc.dart';
import 'package:medtochka/app/core/router/app_router.gr.dart';
import 'package:medtochka/app/core/utils/url_launcher_service.dart';
import 'package:medtochka/app/injector.dart';
import 'package:medtochka/app/repositories/appointment_detail/appointment_detail_repository_impl.dart';
import 'package:medtochka/app/repositories/appointments/appointments_repository.dart';
import 'package:medtochka/app/repositories/appointments/appointments_repository_impl.dart';
import 'package:medtochka/app/repositories/auth/auth_repository_impl.dart';
import 'package:medtochka/app/repositories/favourites/favourites_repository.dart';
import 'package:medtochka/app/repositories/favourites/favourites_repository_impl.dart';
import 'package:medtochka/app/repositories/prefs/prefs_repository.dart';
import 'package:medtochka/app/repositories/review_detail/review_detail_repository_impl.dart';
import 'package:medtochka/app/repositories/reviews/reviews_repository.dart';
import 'package:medtochka/app/repositories/reviews/reviews_repository_impl.dart';
import 'package:medtochka/app/repositories/token_manager/token_manager.dart';
import 'package:medtochka/app/repositories/user_info/user_info.dart';
import 'package:medtochka/resources.dart';

import 'bloc/auth_code/auth_code_bloc.dart';
import 'bloc/create_auth_code/create_auth_code_bloc.dart';

class Application extends StatelessWidget {
  final TokenManager _tokenManager = Get.find<TokenManager>();
  final Dio _mainClient = Get.find<Dio>(tag: DependenciesInitializer.MAIN_CLIENT_TAG);
  final Dio _infoClient = Get.find<Dio>(tag: DependenciesInitializer.INFO_CLIENT_TAG);
  final PrefsRepository _prefsRepository = Get.find<PrefsRepository>();
  final UserInfoRepository _userInfoRepository = Get.find<UserInfoRepository>();
  final UrlLauncherService _urlLauncherService = Get.find<UrlLauncherService>();
  late final ReviewsRepository _reviewsRepository = ReviewsRepositoryImpl(_mainClient);
  late final AppointmentsRepository _appointmentsRepository =
      AppointmentsRepositoryImpl(_infoClient);
  late final FavouritesRepository _favouritesRepository = FavouritesRepositoryImpl(
    Get.find<Dio>(tag: DependenciesInitializer.MAIN_CLIENT_TAG),
  );

  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SplashBloc>(
          create: (_) =>
              SplashBloc(_prefsRepository, _tokenManager, _userInfoRepository)..add(SplashLoaded()),
        ),
        BlocProvider<TourBloc>(
          lazy: true,
          create: (_) => TourBloc(_prefsRepository)..add(TourLoaded()),
        ),
        BlocProvider<SliderCubit>(
          lazy: true,
          create: (_) => SliderCubit(),
        ),
        BlocProvider<ConfirmCodeEntryBloc>(
          lazy: true,
          create: (_) => CodeEntryBloc(),
        ),
        BlocProvider<CreateCodeEntryBloc>(
          lazy: true,
          create: (_) => CodeEntryBloc(),
        ),
        BlocProvider<CreateAuthCodeBloc>(
          lazy: true,
          create: (context) => CreateAuthCodeBloc(
            _prefsRepository,
            codeEntry: BlocProvider.of<CreateCodeEntryBloc>(context),
          ),
        ),
        BlocProvider<AuthCodeBloc>(
          lazy: true,
          create: (context) => AuthCodeBloc(
            _prefsRepository,
            _tokenManager,
            codeEntry: BlocProvider.of<ConfirmCodeEntryBloc>(context),
          ),
        ),
        BlocProvider<AuthBloc>(
          lazy: true,
          create: (_) => AuthBloc(
            AuthRepositoryImpl(
              _tokenManager,
              _mainClient,
            ),
            _userInfoRepository,
            _prefsRepository,
          ),
        ),
        BlocProvider<AppointmentDetailBloc>(
          lazy: true,
          create: (_) => AppointmentDetailBloc(AppointmentDetailRepositoryImpl(_infoClient)),
        ),
        BlocProvider<ReviewDetailBloc>(
          lazy: true,
          create: (_) => ReviewDetailBloc(ReviewDetailRepositoryImpl(_mainClient)),
        ),
        BlocProvider<ReviewsBloc>(
          lazy: true,
          create: (context) => ReviewsBloc(
            _reviewsRepository,
            _prefsRepository,
            _urlLauncherService,
          )..add(ReviewsFetch()),
        ),
        BlocProvider<AppointmentsBloc>(
          lazy: true,
          create: (context) => AppointmentsBloc(_appointmentsRepository)..add(AppointmentsFetch()),
        ),
        BlocProvider<FavouritesBloc>(
          lazy: true,
          create: (context) => FavouritesBloc(_favouritesRepository)..add(FavouritesFetch()),
        ),
      ],
      child: MaterialApp.router(
        title: ResStrings.APP_NAME,
        debugShowCheckedModeBanner: true,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        routerDelegate: _appRouter.delegate(initialRoutes: [const SplashRoute()]),
        routeInformationParser: _appRouter.defaultRouteParser(),
      ),
    );
  }
}
