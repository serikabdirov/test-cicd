//строковые ресурсы
class ResStrings {
  ResStrings._();

  //тестовый endPoint
  static const String BASE_URL = "https://medtochka-dev1.prodoctorov.com/";

  static const String APP_NAME = "МедТочка";
  static const String SPLASH_BODY = "Точка входа в цифровую медицину";
}
