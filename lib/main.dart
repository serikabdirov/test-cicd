import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:medtochka/app/application.dart';
import 'package:medtochka/app/injector.dart';

Future main() async {
  await dotenv.load(fileName: ".env");

  DependenciesInitializer.initializeDependencies();

  runApp(Application());
}
